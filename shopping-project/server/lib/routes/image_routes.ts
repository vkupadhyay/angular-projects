import { Application, Request, Response, NextFunction } from 'express';
import { uploadImages, productImages, userImages, getResult, productFullImages } from '../controllers/imageController';

export class UserImageRoutes {

   public route(app: Application) {
      app.post('/api/product-image', uploadImages, productImages, productFullImages, getResult, (req: Request, res: Response) => { });
      app.post('/api/user-image', uploadImages, userImages, getResult, (req: Request, res: Response) => { });
   }
}