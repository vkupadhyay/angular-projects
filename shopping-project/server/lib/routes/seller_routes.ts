import { Application, Request, Response } from 'express';
import { SellerController } from '../controllers/sellerController';
export class SellerRoutes {

    private seller_controller: SellerController = new SellerController();

    public route(app: Application) {

        app.post('/api/add-product', (req: Request, res: Response) => {
            this.seller_controller.addProduct(req, res);
        });
        
        app.get('/api/seller-product-list/:id', (req: Request, res: Response) => {
            this.seller_controller.getSellerProduct(req, res);
        });

        app.get('/api/seller-product-count/:id', (req: Request, res: Response) => {
            this.seller_controller.getSellerProductCount(req, res);
        });



    }
}