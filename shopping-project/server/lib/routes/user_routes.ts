import { Application, Request, Response } from 'express';
import { UserController } from '../controllers/userController';
import { verifyToken } from '../middleware/verifyToken';
export class UserRoutes {

    private user_controller: UserController = new UserController();

    public route(app: Application) {

        app.get('/api/user-data', (req: Request, res: Response) => {
            this.user_controller.userData(req, res);
        });

        app.post('/api/signup', (req: Request, res: Response) => {
            this.user_controller.userSignUp(req, res);
        });

        app.post('/api/signin', (req: Request, res: Response) => {
            this.user_controller.userSignIn(req, res);
        });

        app.get('/api/users-count', (req: Request, res: Response) => {
            this.user_controller.countUsers(req, res);
        });

        app.get('/api/users', (req: Request, res: Response) => {
            this.user_controller.listUsers(req, res);
        });
        
        app.get('/api/admin-seller-info/:id', (req: Request, res: Response) => {
            this.user_controller.getSellerInfo(req, res);
        });

        app.get('/api/admin-user-info/:id', (req: Request, res: Response) => {
            this.user_controller.getUserInfo(req, res);
        });
    }
}