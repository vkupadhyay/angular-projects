import { Application, Request, Response } from 'express';
import { CartController } from '../controllers/cartController';
export class CartRoutes {

    private cart_controller: CartController = new CartController();

    public route(app: Application) {

        app.post('/api/add-to-cart', (req: Request, res: Response) => {
            this.cart_controller.addToCart(req, res);
        });

        app.get('/api/cart', (req: Request, res: Response) => {
            this.cart_controller.cartList(req, res);
        });

        app.get('/api/cart-item-count', (req: Request, res: Response) => {
            this.cart_controller.cartItemCount(req, res);
        });

        app.get('/api/cart-qty', (req: Request, res: Response) => {
            this.cart_controller.cartQty(req, res);
        });

        app.delete('/api/cart-item-remove/:id', (req: Request, res: Response) => {
            this.cart_controller.itemRemoveFromCart(req, res);
        });


    }
}