import { Application, Request, Response } from 'express';
import { MessageController } from '../controllers/messageController';

export class MessageRoutes {

    private message_controller: MessageController = new MessageController();

    public route(app: Application) {

        app.post('/api/message', (req: Request, res: Response) => {
            this.message_controller.postMessage(req, res);
        });

        app.get('/api/admin-message', (req: Request, res: Response) => {
            this.message_controller.allMessage(req, res);
        });

        app.post('/api/order-message', (req: Request, res: Response) => {
            this.message_controller.postOrderMessage(req, res);
        });

    }
}