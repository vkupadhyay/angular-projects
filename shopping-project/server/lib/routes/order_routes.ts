import { Application, Request, Response } from 'express';
import { OrderController } from '../controllers/orderController';
export class OrderRoutes {

    private order_controller: OrderController = new OrderController();

    public route(app: Application) {
        app.post('/api/order', (req: Request, res: Response) => {
            this.order_controller.order(req, res);
        });

        app.get('/api/order-list/:id', (req: Request, res: Response) => {
            this.order_controller.userOrderList(req, res);
        });

        app.put('/api/order-cancel/:id', (req: Request, res: Response) => {
            this.order_controller.orderCancel(req, res);
        });

        app.get('/api/seller-order-list/:id', (req: Request, res: Response) => {
            this.order_controller.sellerOrderList(req, res);
        });

        app.put('/api/seller-order-status', (req: Request, res: Response) => {
            this.order_controller.updateOrderStatusSeller(req, res);
        });

        app.get('/api/admin-order-list', (req: Request, res: Response) => {
            this.order_controller.adminOrderList(req, res);
        })

        app.get('/api/admin-order-count', (req: Request, res: Response) => {
            this.order_controller.adminOrderCount(req, res);
        })

        app.get('/api/users/order-count/:id', (req: Request, res: Response) => {
            this.order_controller.orderCountOfUsers(req, res);
        });

        app.get('/api/users/order-list/:id', (req: Request, res: Response) => {
            this.order_controller.orderListOfUsers(req, res);
        });




    }
}