import { Application, Request, Response } from 'express';
import { AdminController } from '../controllers/adminController';
import { verifyToken } from '../middleware/verifyToken';
export class AdminRoutes {

    private admin_controller: AdminController = new AdminController();

    public route(app: Application) {

        app.post('/api/add-categorie', (req: Request, res: Response) => {
            this.admin_controller.addCategories(req, res);
        });

        app.get('/api/categorie-list', (req: Request, res: Response) => {
            this.admin_controller.categorieList(req, res);
        });

        app.get('/api/admin-products-list', (req: Request, res: Response) => {
            this.admin_controller.adminProductList(req, res);
        })

        app.get('/api/admin-products-count', (req: Request, res: Response) => {
            this.admin_controller.adminProductCount(req, res);
        })

        app.get('/api/product-details/:id', (req: Request, res: Response) => {
            this.admin_controller.productDteils(req, res);
        });

        app.put('/api/product-edit/:id', (req: Request, res: Response) => {
            this.admin_controller.productDteilsEdit(req, res);
        });

        app.get('/api/product-qty', (req: Request, res: Response) => {
            this.admin_controller.productQty(req, res);
        });


      


    }
}