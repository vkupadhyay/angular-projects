import { Application, Request, Response } from 'express';
import { CommonUseController } from '../controllers/commonUseController';
export class CommonUseRoutes {

    private seller_controller: CommonUseController = new CommonUseController();

    public route(app: Application) {

        app.get('/api/product-for-sell', (req: Request, res: Response) => {
            this.seller_controller.showProductList(req, res);
        });

    }
}