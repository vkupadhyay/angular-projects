export interface OrderItem {
    qty: number;
    product_id: string;
    price: number;
    status?: string;
    image: string;
    seller_id: string;
    chart_price: number;
    description: string;
    name: string; 
}

export interface IAddOrder {
    _id?: string;
    status?: string;
    user_id: string;
    order_item: OrderItem[];
}