import * as mongoose from 'mongoose';
import { ModificationNote, CreatedDate } from '../common/model';
import { IAddOrder } from './model';
const Schema = mongoose.Schema;

const schema = new Schema<IAddOrder>({
    user_id: String,
    order_item: [
        {
            qty: { type: Number },
            product_id: { type: String },
            price: { type: Number },
            image: { type: String },
            seller_id: { type: String },
            chart_price: { type: Number },
            description: { type: String },
            name:{ type: String },
            status: {
                type: String,
                default: 'Pending'
            }
        }
    ],
    status: {
        type: String,
        default: 'active'
    }

}, { timestamps: true });

export default mongoose.model('orders', schema);