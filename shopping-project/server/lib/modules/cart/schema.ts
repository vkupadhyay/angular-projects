import * as mongoose from 'mongoose';
import { ModificationNote, CreatedDate  } from '../common/model';
import {IAddToCart} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IAddToCart>({
    qty: Number,
    product_id: String,
    user_id: String,
    status: {
        type: String,
        default: 'active'
    }
    
}, { timestamps: true });

export default mongoose.model('carts', schema);