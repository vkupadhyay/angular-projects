

export interface IAddToCart {
    _id?: string;
    product_id:string;
    user_id:string;
    status?: string;
    qty: number;
}