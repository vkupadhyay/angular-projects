import * as mongoose from 'mongoose';
import {IAddProduct} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IAddProduct>({
    name: { type: String, required: [true, "Enter product name."] },
    description: { type: String, required: [true, "Enter product description."] },
    categorie_id: String,
    price: { type: Number, required: [true, "Enter product price."] },
    seller_id: { type: String, required: [true, "Seller ID is required."] },
    image: { type: String, required: [true, "Enter product image."] },
    status: {type: String, required: [true, "Enter product price."] }
    
}, { timestamps: true });

export default mongoose.model('products', schema);