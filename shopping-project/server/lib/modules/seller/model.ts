export interface IAddProduct {
    _id?: string;
    name: string;
    categorie_id: string;
    price: number
    image: string;
    seller_id: string;
    description: string;
    status?: string;
}