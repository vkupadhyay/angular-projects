

export interface IUser {
    _id?: string;
    name: {
        first_name: string;
        last_name: string;
    };
    email: string;
    phone_number: number;
    gender: string;
    password: string;
    image: string;
    address: string;
    status?: string;
    user_type: string;
}