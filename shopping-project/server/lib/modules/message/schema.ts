import * as mongoose from 'mongoose';
import { ModificationNote, CreatedDate  } from '../common/model';
import {IMessage} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IMessage>({
    user_id: String,
    seller_id: String,
    product_id: String,
    message: String,
    read: {
        type: Boolean,
        default: false
    },
    status: {
        type: String,
        default: 'active'
    }
    
}, { timestamps: true });

export default mongoose.model('messages', schema);