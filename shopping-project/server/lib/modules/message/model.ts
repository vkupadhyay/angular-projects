export interface IMessage {
    _id?: string;
    user_id: string;
    seller_id: string;
    product_id: string;
    message:string;
    read?:boolean;
    status?: string;
}