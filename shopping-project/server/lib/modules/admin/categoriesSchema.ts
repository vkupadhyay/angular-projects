import * as mongoose from 'mongoose';
import { IAddCategories } from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IAddCategories>({
    name: {
        type: String,
        unique: true
    },
    description: String,
    status: {
        type: String,
        default: 'active'
    }

}, { timestamps: true });

export default mongoose.model('categories', schema);