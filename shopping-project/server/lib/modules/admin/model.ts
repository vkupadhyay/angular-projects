export interface IAddCategories {
    _id?: string;
    name: string;
    description: string;
    status?: string;
}