import { Request, Response } from "express";
import {
  insufficientParameters,
  mongoError,
  successResponse,
  failureResponse,
} from "../modules/common/service";
import { IMessage } from "../modules/message/model";
import messages from "../modules/message/schema";
import users from '../modules/users/schema';
import orders from '../modules/order/schema';
import products from '../modules/seller/addProductSchema';
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
export class MessageController {
 
  postMessage = async (req: Request, res: Response) => {
    try {
      if (req.body.user_id && req.body.seller_id && req.body.product_id && req.body.message) {
        const message_params: IMessage = {
          user_id: req.body.user_id,
          seller_id: req.body.seller_id,
          product_id: req.body.product_id,
          message: req.body.message,
        };
        const _session = new messages(message_params);
        await _session.save().then(
          (message_data: IMessage) => {
            successResponse("Message add successfull", null, res);
          },
          (error) => {
            mongoError(error, res);
          }
        );
      } else {
        failureResponse(
          "Something is going wrong, Please check again",
          Date,
          res
        );
      }
    } catch (error) {
      insufficientParameters(error);
    }
  };

  allMessage = async (req: Request, res: Response) => {
    try {
      
      const data = await messages.aggregate([
        {
          $match: { "status": 'active' }
        },
        {
            $project: {  status: 0, __v: 0 },
        },
        {
            $lookup:
            {
                from: "users",
                "let": { "id": { "$toObjectId": "$user_id" } },
                pipeline: [
                    {
                        $match:
                        {
                            $expr:
                            {
                                $and:
                                    [
                                        { $eq: ["$_id", "$$id"] },
                                        { $eq: ["$user_type", 'Buyer'] },
                                    ]
                            }
                        }
                    },
                    { $project: { _id: 0, name: 1, } },
                ],
                as: "Buyer"
            }
        }, {
            $lookup:
            {
                from: "users",
                "let": { "id": { "$toObjectId": "$seller_id" } },
                pipeline: [
                    {
                        $match:
                        {
                            $expr:
                            {
                                $and:
                                    [
                                        { $eq: ["$_id", "$$id"] },
                                        { $eq: ["$user_type", 'Seller'] },
                                    ]
                            }
                        }
                    },
                    { $project: { _id: 0, name: 1, } },
                ],
                as: "Seller"
            }
        },
        {
            $lookup:
            {
                from: "products",
                "let": { "id": { "$toObjectId": "$product_id" } },
                pipeline: [
                    {
                        $match:
                        {
                            $expr:
                            {
                                $and:
                                    [
                                        { $eq: ["$_id", "$$id"] },
    
                                    ]
                            }
                        }
                    },
                    { $project: { _id: 0, name: 1 } },
                ],
                as: "product_details"
            }
        },
        { $unwind: '$product_details' },
        { $unwind: '$Buyer' },
        { $unwind: '$Seller' },
        {
            $addFields: {
                message: "$message",
                message_update: "$updatedAt",
                product_name: "$product_details.name",
                user_name: "$Buyer.name.first_name",
                seller_name: "$Seller.name.first_name"
            },
        },
    
        {
            $project: { user_id: 0, seller_id: 0, updatedAt: 0, product_id: 0, read: 0, createdAt: 0, Buyer: 0, product_details: 0, Seller: 0 },
        },
    ]);
   
      successResponse("All message get successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

  postOrderMessage = async (req: Request, res: Response) => {
    try {
      if (req.body.user_id && req.body.seller_id && req.body.product_id && req.body.message) {
        const message_params: IMessage = {
          user_id: req.body.user_id,
          seller_id: req.body.seller_id,
          product_id: req.body.product_id,
          message: req.body.message,
        };
        const _session = new messages(message_params);
        await _session.save().then(
          (message_data: IMessage) => {
            successResponse("Message add successfull", null, res);
          },
          (error) => {
            mongoError(error, res);
          }
        );
      } else {
        failureResponse(
          "Something is going wrong, Please check again",
          Date,
          res
        );
      }
    } catch (error) {
      insufficientParameters(error);
    }
  }
}
