import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import orders from '../modules/order/schema';
import carts from '../modules/cart/schema';
const mongoose = require("mongoose");
import { IAddOrder } from '../modules/order/model';
const ObjectId = mongoose.Types.ObjectId;

export class OrderController {

  order = async (req: Request, res: Response) => {
    try {
      if (req.body) {

        const user_params: IAddOrder = {
          user_id: req.body.user_id,
          order_item: req.body.order_item
        };
        const _session = new orders(user_params);
        await _session.save().then(
          (user_data: IAddOrder) => {
            successResponse("create user successfull", user_data, res);
          },
          (error) => {
            mongoError(error, res);
          }
        );

        await carts.remove({ "user_id": req.body.user_id });
      } else {
        failureResponse(
          "Something is going wrong, Please check again",
          Date,
          res
        );
      }
    } catch (error) {
      insufficientParameters(error);
    }
  }

  userOrderList = async (req: Request, res: Response) => {

    try {
      const data = await orders.aggregate(
        [
          {
            $match: { user_id: req.params.id }
          },
          { $project: { updatedAt: 0, __v: 0, user_id: 0, status: 0 } },


        ]
      )
      successResponse('User cart list get successfully', data, res);
    } catch (error) {
      insufficientParameters(error)
    }

  }

  orderCancel = async (req: Request, res: Response) => {
    //  console.log(req.body, req.params.id, req.body.status, req.body.product_id);

    try {
      let id = req.body.product_id;
      const orderCancelData = await orders.findOneAndUpdate({
        _id: ObjectId(req.params.id),
        'order_item.product_id': id,
      }, {
        $set: {
          "order_item.$.status": req.body.status,
        },
      }, {

      })
      successResponse('Order successfully cancelled', null, res);
    } catch (err) {
      res.status(500).json(err);
    }
  }

  sellerOrderList = async (req: Request, res: Response) => {

    try {
      const data = await orders.aggregate(
        [
          { $unwind: '$order_item' },
          {
            $match: { "order_item.seller_id": req.params.id, "order_item.status": req.query.status }
          },
          { $project: { _id: 0, __v: 0, updatedAt: 0, status: 0 } },
          {
            $lookup:
            {
              from: "users",
              "let": { "id": { "$toObjectId": "$user_id" } },
              pipeline: [
                {
                  $match:
                  {
                    $expr:
                    {
                      $and:
                        [
                          { $eq: ["$_id", "$$id"] }
                        ]
                    }
                  }
                },
                { $project: { _id: 0, name: 1, email: 1, phone_number: 1, image: 1, address: 1 } },
              ],
              as: "user_details"
            },

          },
          { $unwind: '$user_details' },

        ]
      )

      successResponse('Seller order product list get successfully', data, res);
    } catch (error) {
      insufficientParameters(error)
    }
  }

  updateOrderStatusSeller = async (req: Request, res: Response) => {
    try {
      let id = ObjectId(req.body.params.id);
      const orderStatusData = await orders.findOneAndUpdate({
        "order_item._id": id,
      }, {
        $set: {
          "order_item.$.status": req.body.params.status,
        },
      }, {

      });
      successResponse('Order successfully cancelled', null, res);
    } catch (err) {
      res.status(500).json(err);
    }
  }

  adminOrderList = async (req: Request, res: Response) => {
    try {
      const data = await orders.aggregate([
        { $unwind: '$order_item' },
        {
          $match: {
            "order_item.name": { $regex: req.query.name },
            "order_item.status": req.query.status,
          },
        },
        {
          $sort: { createdAt: 1 },
        },
        { $project: { updatedAt: 0, __v: 0 } },

      ]);
      successResponse("Order list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

  adminOrderCount = async (req: Request, res: Response) => {
    try {
      const date = await orders.aggregate([
        { $unwind: '$order_item' },
        {
          $facet: {
            first: [
              {
                $match: {
                  "order_item.name": { $regex: req.query.name },
                  "order_item.status": "Success",

                },
              },
            ],
            second: [
              {
                $match: {
                  "order_item.name": { $regex: req.query.name },
                  "order_item.status": "Process",

                },
              },
            ],
            third: [
              {
                $match: {
                  "order_item.name": { $regex: req.query.name },
                  "order_item.status": "Pending",

                },
              },
            ],
            fourth: [
              {
                $match: {
                  "order_item.name": { $regex: req.query.name },
                  "order_item.status": "Cancelled",

                },
              },
            ],
          },
        },
        {
          $project: {
            Success_count: {
              $size: "$first",
            },
            Process_count: {
              $size: "$second",
            },
            Pending_count: {
              $size: "$third",
            },
            Cancelled_count: {
              $size: "$fourth",
            },
          },
        },
      ]);
      successResponse("Counts of order got successfully", date, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  orderListOfUsers = async (req: Request, res: Response) => {
    try {
      const data = await orders.aggregate(
        [{ $unwind: '$order_item' },
        {

          $match:
          {
            user_id: req.params.id,
            "order_item.name": { $regex: req.query.name },
            $expr:
            {
              $and:
                [
                  { $eq: ["$order_item.status", req.query.status] }
                ]
            }
          }
        },
        {
          $sort: { createdAt: 1 },
        },
        { $project: { updatedAt: 0, __v: 0, user_id: 0, status: 0 } },
        ]
      );
      successResponse("Order list get successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

  orderCountOfUsers = async (req: Request, res: Response) => {
    try {
      const date = await orders.aggregate([
        { $unwind: '$order_item' },
        {
          $facet: {
            first: [
              {

                $match:
                {
                  user_id: req.params.id,
                  "order_item.name": { $regex: req.query.name },
                  $expr:
                  {
                    $and:
                      [
                        { $eq: ["$order_item.status", "Success"] }
                      ]
                  }
                }

              },
            ],
            second: [
              {


                $match:
                {
                  user_id: req.params.id,
                  "order_item.name": { $regex: req.query.name },
                  $expr:
                  {
                    $and:
                      [
                        { $eq: ["$order_item.status", "Process"] }
                      ]
                  }
                }

              },
            ],
            third: [
              {
                $match:
                {
                  user_id: req.params.id,
                  "order_item.name": { $regex: req.query.name },
                  $expr:
                  {
                    $and:
                      [
                        { $eq: ["$order_item.status", "Pending"] }
                      ]
                  }
                }


              },
            ],
            fourth: [
              {
                $match:
                {
                  user_id: req.params.id,
                  "order_item.name": { $regex: req.query.name },
                  $expr:
                  {
                    $and:
                      [
                        { $eq: ["$order_item.status", "Cancelled"] }
                      ]
                  }
                }
              },
            ],
          },
        },
        {
          $project: {
            Success_count: {
              $size: "$first",
            },
            Process_count: {
              $size: "$second",
            },
            Pending_count: {
              $size: "$third",
            },
            Cancelled_count: {
              $size: "$fourth",
            },
          },
        },
      ]);
      successResponse("Counts of order got successfully", date, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };




}