import { Request, Response } from "express";
import {
  insufficientParameters,
  mongoError,
  successResponse,
  failureResponse,
} from "../modules/common/service";
const mongoose = require("mongoose");
import { IUser } from "../modules/users/model";
const TOKEN_KEY = "@QEGTUI";
import users from "../modules/users/schema";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
const ObjectId = mongoose.Types.ObjectId;
export class UserController {
  userList = async (req: Request, res: Response) => {
    try {
      let query = [{ is_deleted: false }];
      const data = await users.aggregate([
        {
          $match: { $and: query },
        },
        {
          $project: {
            _id: 0,
            user_id: 0,
            is_deleted: 0,
            password: 0,
            user_type: 0,
            createdAt: 0,
            updatedAt: 0,
            __v: 0,
          },
        },
      ]);
      successResponse("User list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  userSignUp = async (req: Request, res: Response) => {
    try {
      if (
        req.body.name &&
        req.body.name.first_name &&
        req.body.name.last_name &&
        req.body.email &&
        req.body.phone_number &&
        req.body.gender &&
        req.body.password &&
        req.body.address
      ) {
        const salt = await bcrypt.genSalt(10);
        const newPassword = await bcrypt.hash(req.body.password, salt);
        const user_params: IUser = {
          name: {
            first_name: req.body.name.first_name,
            last_name: req.body.name.last_name,
          },
          email: req.body.email,
          phone_number: req.body.phone_number,
          image: req.body.image,
          gender: req.body.gender,
          user_type: req.body.user_type,
          password: newPassword,
          address: req.body.address,
        };
        const _session = new users(user_params);
        await _session.save().then(
          (user_data: IUser) => {
            successResponse("create user successfull", user_data, res);
          },
          (error) => {
            mongoError(error, res);
          }
        );
      } else {
        failureResponse(
          "Something is going wrong, Please check again",
          Date,
          res
        );
      }
    } catch (error) {
      insufficientParameters(error);
    }
  };

  userSignIn = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      const user = await users.findOne({ email });
      if (!user) {
        const error = "Email not match";
        mongoError(error, res);
      } else {
        if (user.status === "inactive") {
          const error = "Your account is inactive";
          mongoError(error, res);
        } else {
          bcrypt.compare(password, user.password, function (error, result) {
            if (!result) {
              const error = "Password not match";
              mongoError(error, res);
            } else {
              const auth_token = jwt.sign(
                {
                  user_id: user._id,
                  first_name: user.name.first_name,
                  last_name: user.name.last_name,
                  user_email: user.email,
                  gender: user.gender,
                  image: user.image,
                  phone_number: user.phone_number,
                  address: user.address,
                  user_type: user.user_type,
                },
                TOKEN_KEY,
                {
                  expiresIn: "12h",
                }
              );
              successResponse("Sign In successfully", auth_token, res);
            }
          });
        }
      }
    } catch (error) {
      insufficientParameters(error);
    }
  };

  userData = async (req: Request, res: Response) => {
    try {
      const id = req.params.id;
      let query = [{ _id: ObjectId(id) }];
      const data = await users.aggregate([
        {
          $match: { $and: query },
        },
        {
          $project: {
            _id: 0,
            user_id: 0,
            is_deleted: 0,
            password: 0,
            user_type: 0,
            createdAt: 0,
            updatedAt: 0,
            __v: 0,
          },
        },
      ]);
      successResponse("User Data got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  countUsers = async (req: Request, res: Response) => {
    try {
      const date = await users.aggregate([
        {
          $facet: {
            first: [
              {
                $match: {
                  email: { $regex: req.query.email },
                  status: "active",
                  user_type: req.query.user_type,
                },
              },
            ],
            second: [
              {
                $match: {
                  email: { $regex: req.query.email },
                  status: "inactive",
                  user_type: req.query.user_type,
                },
              },
            ],
          },
        },
        {
          $project: {
            Active_count: {
              $size: "$first",
            },
            Delete_count: {
              $size: "$second",
            },
          },
        },
      ]);
      successResponse("Counts of customer user got successfully", date, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  listUsers = async (req: Request, res: Response) => {
    try {
      const data = await users.aggregate([
        {
          $match: {
            email: { $regex: req.query.email },
            status: req.query.status,
            user_type: req.query.user_type,
          },
        },
        {
          $sort: { createdAt: 1 },
        },
        {
          $project: {
            user_id: 0,
            is_deleted: 0,
            password: 0,
            user_type: 0,
            createdAt: 0,
            updatedAt: 0,
            __v: 0,
          },
        },
      ]);
      successResponse("Customer user list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  getSellerInfo = async (req: Request, res: Response) => {
    let id = ObjectId(req.params.id)
    try {
      const data = await users.aggregate([
        {
          $match: { _id: id },
        },
        {
          $project: {
            _id: 0,
            name: 1,
            email: 1,
            address: 1,
            image: 1,
            phone_number: 1,
          },
        },
      ])
      successResponse('Seller User details get successfully', data, res)
    } catch (error) {
      insufficientParameters(error)
    }
  }

  getUserInfo = async (req: Request, res: Response) => {
    let id = ObjectId(req.params.id)
    try {
      const data = await users.aggregate([
        {
          $match: { _id: id },
        },
        {
          $project: {
            _id: 0,
            name: 1,
            email: 1,
            address: 1,
            image: 1,
            phone_number: 1,
          },
        },
      ])
      successResponse('User details get successfully', data, res)
    } catch (error) {
      insufficientParameters(error)
    }
  }

}
