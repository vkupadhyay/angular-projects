import { Request, Response } from "express";
import {
  insufficientParameters,
  mongoError,
  successResponse,
  failureResponse,
} from "../modules/common/service";
import categories from "../modules/admin/categoriesSchema";
import products from "../modules/seller/addProductSchema";
import carts from "../modules/cart/schema";
import orders from '../modules/order/schema';
import { IAddCategories } from "../modules/admin/model";
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
export class AdminController {
  addCategories = async (req: Request, res: Response) => {
    try {
      if (req.body.name && req.body.description) {
        const categories_params: IAddCategories = {
          name: req.body.name,
          description: req.body.description,
        };
        const _session = new categories(categories_params);
        await _session.save().then(
          (user_data: IAddCategories) => {
            successResponse("Categories add successfull", user_data, res);
          },
          (error) => {
            mongoError(error, res);
          }
        );
      } else {
        failureResponse(
          "Something is going wrong, Please check again",
          Date,
          res
        );
      }
    } catch (error) {
      insufficientParameters(error);
    }
  };

  categorieList = async (req: Request, res: Response) => {
    try {
      let query = [{ status: "active" }];
      const data = await categories.aggregate([
        {
          $match: { $and: query },
        },
        {
          $sort: { name: 1 },
        },
        { $project: { is_deleted: 0, createdAt: 0, updatedAt: 0, __v: 0 } },
      ]);
      successResponse("Categorie list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  adminProductList = async (req: Request, res: Response) => {
    try {
      const data = await products.aggregate([
        {
          $match: {
            name: { $regex: req.query.name },
            status: req.query.status,
          },
        },
        {
          $sort: { createdAt: 1 },
        },
        { $project: { createdAt: 0, updatedAt: 0, __v: 0 } },
      ]);
      successResponse("Product list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  adminProductCount = async (req: Request, res: Response) => {
    try {
      const date = await products.aggregate([
        {
          $facet: {
            first: [
              {
                $match: {
                  name: { $regex: req.query.name },
                  status: "active",
                },
              },
            ],
            second: [
              {
                $match: {
                  name: { $regex: req.query.name },
                  status: "inactive",
                },
              },
            ],
          },
        },
        {
          $project: {
            Active_count: {
              $size: "$first",
            },
            Delete_count: {
              $size: "$second",
            },
          },
        },
      ]);
      successResponse("Counts of customer user got successfully", date, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

  productDteils = async (req: Request, res: Response) => {
    const id = req.params.id;
    let user_id = req.query.user_id;
    let query = [
      {
        product_id: req.params.id,
        user_id: req.query.user_id,
      },
    ];
    const checkProductInCart = await carts.aggregate([
      {
        $match: { $and: query },
      },
    ]);


    if (checkProductInCart.length) {
      const data = await products.aggregate(
        [
          { $match: { "_id": ObjectId(id) } },

          {
            $lookup:
            {
              from: "carts",
              let: { product_id: '$_id' },
              pipeline: [
                {
                  $match:
                  {
                    $expr:
                    {
                      $and:
                        [
                          { $eq: ["$product_id", id] },
                          { $eq: ["$user_id", user_id] }
                        ]
                    }
                  }
                }, { $project: { _id: 0, cart_price: 1, product_price: 1, qty: 1 } }
              ],
              as: "cart"
            }
          },
          { $project: { createdAt: 0, updatedAt: 0, __v: 0, categorie_id: 0, product_status: 0, status: 0, seller_id: 0 } },
          {
            $unwind: "$cart"
          }
        ]);
      successResponse('Product Data got successfully', data, res);
    } else {
      const dataA = await products.aggregate([
        {
          $match: { _id: ObjectId(id) },
        },
        { $project: { name: 1, description: 1, price: 1, image: 1 } },
        {
          $addFields: {
            cart: {
              product_price: "$price",
              cart_price: "$price",
              qty: 1,
            },
          },
        },
      ]);
      successResponse("Productd Data got successfully", dataA, res);
    }

  };

  productDteilsEdit = async (req: Request, res: Response) => {
    try {
      const updatedUser = await products.findByIdAndUpdate(
        req.params.id,
        {
          $set: req.body,
        },
        { new: true }
      );

      successResponse("Product edit successfully", updatedUser, res);
    } catch (err) {
      res.status(500).json(err);
    }
  };

  productQty = async (req: Request, res: Response) => {
    try {
      const { product_id, user_id } = req.query;
      const data = await carts.aggregate([
        {
          $match: {
            $expr: {
              $and: [
                { $eq: ["$product_id", product_id] },
                { $eq: ["$user_id", user_id] },
              ],
            },
          },
        },
        { $project: { _id: 0, qty: 1, product_price: 1, cart_price: 1 } },
      ]);
      successResponse("Qty get successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  };

 
}
