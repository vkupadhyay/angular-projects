import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import carts from '../modules/cart/schema';
const mongoose = require("mongoose");
import { IAddToCart } from '../modules/cart/model';
const ObjectId = mongoose.Types.ObjectId;

export class CartController {
   
  addToCart = async(req:Request, res:Response)=>{
    try {
      const { product_id, user_id } = req.body;
      const user = await carts.findOne({ product_id, user_id });
      if(!user){
        const categories_params: IAddToCart = {
          product_id: req.body.product_id,
          user_id: req.body.user_id,
          qty: req.body.qty,
          
        };
        const _session = new carts(categories_params);
        await _session.save().then((user_data: IAddToCart) => {
          successResponse('Item successfull add in cart', user_data, res);
        }, (error) => {
          mongoError(error, res);
        })
      }else{
        let DATA = "";
        failureResponse("Item allready have in cart", DATA, res)
      }
    
    } catch (error) {
      insufficientParameters(error);
    }
  }

  cartList = async (req: Request, res: Response) => {
    try {
      const data = await carts.aggregate(
        [
          {
            $match: { user_id: req.query.user_id }
          },
          { $project: { _id: 1, product_id: 1, qty: 1 } },
          {
              $lookup:
              {
                  from: "products",
                  "let": { "id": { "$toObjectId": "$product_id" } },
                  "pipeline": [
                      { "$match": { "$expr": { "$eq": ["$_id", "$$id"] } } },
                      { $project: { _id: 1, name: 1, price: 1, description: 1, image: 1, seller_id:1 } },
  
                  ],
  
                  as: "cart"
              }
          },
          { $unwind: '$cart' },
  
          {
              $addFields: {
                 
                  price: {
                      "$sum": {
                          "$multiply": ["$cart.price", "$qty"]
                      }
                  },
                  name: "$cart.name",
                  _id: "$_id",
                  chart_price: "$cart.price",
                  description: "$cart.description",
                  image: "$cart.image",
                  seller_id: "$cart.seller_id"
                  
              },
          },
          { $project: { cart: 0,  } }
  
      ]
    )
      successResponse('User cart list get successfully', data, res);
    } catch (error) {
      insufficientParameters(error)
    }
  }

  cartItemCount = async(req: Request, res: Response)=>{
    try {
      const data = await carts.aggregate(
        [
          {
            $match: { user_id: req.query.user_id }
          },
          { $group: { _id: null, count: { $sum: 1 } } }
        ]
      );
      successResponse('User cart list get successfully', data, res);
    } catch (error) {
      insufficientParameters(error)
    }
  }

  cartQty = async(req: Request, res: Response)=>{
     const { user_id, qty, product_id, cart_price } = req.query;
      let newQty = Number(qty)
      try {
        const data = await carts.updateOne(
          { product_id: product_id, user_id: user_id }, 
          [ { $set: { "qty": newQty, 'cart_price': cart_price} } ] 
          );
        successResponse('User cart list get successfully', data, res);
      } catch (error) {
        insufficientParameters(error)
      }
  }


  itemRemoveFromCart = async(req: Request, res: Response)=>{
      let id = req.params.id;
      try {
        const data = await carts.deleteOne({"_id": id});
        successResponse('Item successfully removed from cart', data, res);
      } catch (error) {
        insufficientParameters(error)
      }
    }

}