import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import products from '../modules/seller/addProductSchema';

export class CommonUseController {

  showProductList = async (req: Request, res: Response) => {
    try {
      var limitNum = Number(req.query.limit)
      let query = [{ status: 'active' }];
      const data = await products.aggregate(
        [{
          $match: { $and: query }
        },
        {
          "$sort": { "name": 1 }
        },
        { "$limit": limitNum },

        { $project: { is_deleted: 0, createdAt: 0, seller_id: 0, product_status: 0, categorie_id: 0, updatedAt: 0, __v: 0 } }
        ]
      );
      successResponse('Product list got successfully', data, res);
    } catch (error) {
      insufficientParameters(error)
    }
  }

}