import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import carts from '../modules/cart/schema';
const mongoose = require("mongoose");
import { IAddToCart } from '../modules/cart/model';
const ObjectId = mongoose.Types.ObjectId;

export class CartController {
   
  addToCart = async(req:Request, res:Response)=>{
    try {
      const { product_id, user_id } = req.body;
      const user = await carts.findOne({ product_id, user_id });
      if(!user){
        const categories_params: IAddToCart = {
          product_id: req.body.product_id,
          user_id: req.body.user_id,
          qty: req.body.qty,
          
        };
        const _session = new carts(categories_params);
        await _session.save().then((user_data: IAddToCart) => {
          successResponse('Item successfull add in cart', user_data, res);
        }, (error) => {
          mongoError(error, res);
        })
      }else{
        let DATA = "";
        failureResponse("Item allready have in cart", DATA, res)
      }
    
    } catch (error) {
      insufficientParameters(error);
    }
  }

  cartList = async (req: Request, res: Response) => {
    try {
      const data = await carts.aggregate(
        [
          {
            $match: { user_id: req.query.user_id }
          },
          {
            "$sort": { "product_name": 1 }
          },
          {
          $addFields: {
            cart: {
              product_price: { 
            "$sum" : { 
                "$multiply" : ["$product_price", "$qty"]
            }
          },
              cart_price: "$product_price",
              qty: "$qty",
            },
          },
        },
          
          { $project: { user_id:0, is_deleted: 0, user_type: 0, createdAt: 0, updatedAt: 0, __v: 0 } }
        ]
      );
      successResponse('User cart list get successfully sdasad', data, res);
    } catch (error) {
      insufficientParameters(error)
    }
  }

  cartItemCount = async(req: Request, res: Response)=>{
    try {
      const data = await carts.aggregate(
        [
          {
            $match: { user_id: req.query.user_id }
          },
          { $group: { _id: null, count: { $sum: 1 } } }
        ]
      );
      successResponse('User cart list get successfully', data, res);
    } catch (error) {
      insufficientParameters(error)
    }
  }

  cartQty = async(req: Request, res: Response)=>{
     const { user_id, qty, product_id, cart_price } = req.query;
      let newQty = Number(qty)
      try {
        const data = await carts.updateOne(
          { product_id: product_id, user_id: user_id }, 
          [ { $set: { "qty": newQty, 'cart_price': cart_price} } ] 
          );
        successResponse('User cart list get successfully', data, res);
      } catch (error) {
        insufficientParameters(error)
      }
    }


    itemRemoveFromCart = async(req: Request, res: Response)=>{
      let id = req.params.id;
      try {
        const data = await carts.remove({"_id": id});
        successResponse('Item successfully removed from cart', data, res);
      } catch (error) {
        insufficientParameters(error)
      }
    }

}