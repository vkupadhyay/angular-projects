import { Request, Response } from 'express'
import {
  insufficientParameters,
  mongoError,
  successResponse,
  failureResponse,
} from '../modules/common/service'
import products from '../modules/seller/addProductSchema'
import users from '../modules/users/schema'
import { IAddProduct } from 'modules/seller/model'
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId

export class SellerController {
  addProduct = async (req: Request, res: Response) => {
    try {
      const add_product_params: IAddProduct = {
        name: req.body.name,
        description: req.body.description,
        categorie_id: req.body.categorie_id,
        seller_id: req.body.seller_id,
        status: req.body.status,
        price: req.body.price,
        image: req.body.image,
      }
      const _session = new products(add_product_params)
      await _session.save().then(
        (user_data: IAddProduct) => {
          successResponse('New Product successfull add', null, res)
        },
        (error) => {
          mongoError(error, res)
        },
      )
    } catch (error) {
      insufficientParameters(error)
    }
  }



 

  getSellerProduct = async (req: Request, res: Response) => {
   
   try {
    const data = await products.aggregate([
      {
        $match: {
          name: { $regex: req.query.name },
          status: req.query.status,
          seller_id: req.params.id,
        },
      },
      {
        $sort: { createdAt: 1 },
      },
      {
        $project: { _id:1, name:1, description:1, price:1, image:1, status:1},
      },
    ]);
   
    successResponse("Seller product list get successfully", data, res);
  } catch (error) {
    insufficientParameters(error);
  }
  }

  getSellerProductCount = async (req: Request, res: Response) => {
    try {
      const date = await products.aggregate([
        {
          $facet: {
            first: [
              {
                $match: {
                  name: { $regex: req.query.name },
                  status: "active",
                  seller_id: req.params.id,
                },
              },
            ],
            second: [
              {
                $match: {
                  name: { $regex: req.query.name },
                  status: "inactive",
                  seller_id: req.params.id,
                },
              },
            ],
          },
        },
        {
          $project: {
            Active_count: {
              $size: "$first",
            },
            Delete_count: {
              $size: "$second",
            },
          },
        },
      ]);
      successResponse("Counts of seller product successfully", date, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }
}
