import { Request, Response, NextFunction } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import * as jwt from 'jsonwebtoken';
const TOKEN_KEY = "@QEGTUI";
interface IToken {
  user_id: string,
  name: string,
  user_email: string,
  weight: number,
  height: number,
  BMR: number,
  gender: string,
  age: number,
  iat: number,
  exp: number
}
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  const token = req.body.token || req.query.token || req.headers.token;
  const tokenSplit = token.replace(/^Bearer\s+/, "");
  if (!token) {
    return failureResponse('A token is required for authentication', Date, res);
  }
  try {
    next();

  } catch (error) {
    return failureResponse('Invalid Token', res, error);
  }

};
