import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from 'mongoose';
import * as cors  from 'cors';
import helmet from "helmet";
import environment from "../environment";
import { UserRoutes } from "../routes/user_routes";
import { AdminRoutes } from "../routes/admin_routes";
import { CommonUseRoutes } from "../routes/common_use_routes";
import { SellerRoutes } from "../routes/seller_routes";
import { CartRoutes } from "../routes/cart_routes";
import { OrderRoutes } from "../routes/order_routes";
import { UserImageRoutes } from "../routes/image_routes";
import { MessageRoutes } from "../routes/message_routes";
import { CommonRoutes } from "../routes/common_routes";


class App {

   public app: express.Application;
   public mongoUrl: string = 'mongodb://localhost/' + environment.getDBName();
   private user_routes: UserRoutes = new UserRoutes();
   private admin_routes: AdminRoutes = new AdminRoutes();
   private seller_routes: SellerRoutes = new SellerRoutes();
   private common_use_routes: CommonUseRoutes = new CommonUseRoutes();
   private cart_routes: CartRoutes = new CartRoutes();
   private order_routes: OrderRoutes = new OrderRoutes();
   private image_routes: UserImageRoutes = new UserImageRoutes();
   private message_routes: MessageRoutes = new MessageRoutes();
   private common_routes: CommonRoutes = new CommonRoutes();

   constructor() {
      this.app = express(); 
      this.app.use(cors());
      this.app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));
      this.app.use('/upload', express.static(process.cwd() + '/upload'))
      this.config();
      this.mongoSetup();
      this.user_routes.route(this.app);
      this.image_routes.route(this.app);
      this.admin_routes.route(this.app);
      this.common_use_routes.route(this.app);
      this.cart_routes.route(this.app);
      this.order_routes.route(this.app);
      this.seller_routes.route(this.app);
      this.message_routes.route(this.app);
      this.common_routes.route(this.app);
   }
   
   private config(): void {
      // support application/json type post data
      this.app.use(bodyParser.json());
      //support application/x-www-form-urlencoded post data
      this.app.use(bodyParser.urlencoded({ extended: true }));
      this.app.use((req, res, next) => {
         res.setHeader('Access-Control-Allow-Origin', '*');
         res.setHeader('Access-Control-Allow-Headers', 'Origin, x-access-token, token, Content-Type, Accept, Authorization');
         res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
         res.setHeader('Content-Type', 'text/plain');
         if (req.method == 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
            return res.status(200).json({});
         }

         next();
      });
   }

   

   private mongoSetup(): void {
      mongoose.set('strictQuery', true);
      mongoose.connect(this.mongoUrl, {});
   }

}
export default new App().app;