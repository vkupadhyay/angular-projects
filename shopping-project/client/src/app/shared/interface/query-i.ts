export interface IUserOrderListQuery {
    status: string;
    name: string;
}