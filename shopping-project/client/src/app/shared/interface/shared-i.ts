export interface IloginData {
   user_id: string;
   first_name: string;
   last_name: string;
   user_email: string;
   gender: string;
   image: string;
   phone_number: number;
   address: string;
   user_type: string;
   iat?: number;
   exp?: number;
}

export interface Name {
   first_name: string;
   last_name: string;
}

export interface ISignUpUser {
   name: Name;
   email: string;
   password: string;
   phone_number: number;
   gender: string;
   address: string;
}

export interface IAddProduct {
   name: Name;
   price: string;
   description: string;
   image: string;
   categorie_id: string;
}


export interface IListUser {
   _id?: string;
   user_id?: string;
   name: Name;
   email: string;
   phone_number: number;
   gender: string;
   image: string;
   address: string;
}

export interface CategoriesList {
   _id: string;
   name: string;
   status: string;
   description: string;
}

export interface IProductLIst {
   _id: string;
   name: string;
   description: string;
   categorie_id: string;
   price: number;
   image: string;
}

export interface ISellerProductLIst {
   _id?: string;
   name: string;
   description: string;
   price: number;
   status: string;
   image: string;
}

export interface ICartList {
   qty: number;
   _id?: string;
   product_id: string;
   product_image: string;
   product_name: string;
   product_price: number;
   cart_price: number;
   status: string;
}

export interface IProductDetailsCart {
   qty: number;
   product_price: number;
   cart_price: number;
}

export interface IProductDetails {
   _id: string;
   name: string;
   description: string;
   price: number;
   image: string;
   cart: IProductDetailsCart;
}


export interface ICartItemsList {
   _id: string;
   qty: number;
   product_id: string;
   price: number;
   name: string;
   chart_price: number;
   description: string;
   image: string;
   seller_id: string;

}


export interface IOrderItem {
   qty: number;
        product_id: string;
        price: number;
        image: string;
        seller_id: string;
        chart_price: number;
        description: string;
        name: string;
        status: string;
        _id: string;
}

export interface IOrderList {
   order_item: IOrderItem[];
   _id: string;
   createdAt: string

}



export interface IAdminOrderItem {
   _id: string;
   user_id: string;
   createdAt: string;
   
   order_item: 
      {
         qty: number;
         product_id: string;
         price: number;
         image: string;
         seller_id: string;
         chart_price: number;
         description: string;
         name: string;
         status: string;
         _id: string;
      };
   status: string;
}

export interface sellerDetails {
   name: Name;
   email: string;
   phone_number: number;
   address: string;
}


export interface UserDetails {
   name: Name;
   email: string;
   phone_number: number;
   address: string;
}

export interface ISellerOrderList {
   user_id: string,
   order_item: {
      qty: number;
      product_id: string;
      price: number;
      image: string;
      seller_id: string;
      chart_price: number;
      description: string;
      name: string;
      status: string;
      _id: string;
   };
   createdAt: Date;
   user_details:
     {
         name: Name;
         email: string;
         phone_number: number;
         image: string;
         address: string;
      }
   ;
}

export interface IGetOrderListOfUser {
   _id: string;
   order_item:  {
      qty: number;
      product_id: string;
      price: number;
      image: string;
      seller_id: string;
      chart_price: number;
      description: string;
      name: string;
      status: string;
      _id: string;
  };
   createdAt: Date;
}