import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JwPaginationModule } from 'jw-angular-pagination';
import { SearchComponent } from './components/search/search.component';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AddToCartButtonComponent } from './components/add-to-cart-button/add-to-cart-button.component';
import { QtyComponent } from './components/qty/qty.component';
import { MessageComponent } from './components/message/message.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { FileUploadModule } from 'ng2-file-upload'



@NgModule({
  declarations: [SearchComponent, AddToCartButtonComponent, QtyComponent, MessageComponent, AddProductComponent],
  imports: [
    CommonModule,
    JwPaginationModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule
    
  ], exports: [
    JwPaginationModule,
    SearchComponent,
    AddToCartButtonComponent,
    QtyComponent,
    AddProductComponent
  ]
})
export class SharedModule { }
