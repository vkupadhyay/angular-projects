import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {AuthService} from 'src/app/shared/services/auth/auth.service';
import { Router } from '@angular/router';
import {CartService} from 'src/app/services/cart/cart.service';

@Component({
  selector: 'app-add-to-cart-button',
  templateUrl: './add-to-cart-button.component.html',
  styleUrls: ['./add-to-cart-button.component.scss']
})
export class AddToCartButtonComponent implements OnInit {
  @Input() sendData: any;
  @Output() cartStatus: EventEmitter<string> = new EventEmitter<string>();
  constructor( private authService:AuthService, private router: Router, private cartService:CartService ) {}

  ngOnInit(): void {}
  
  addToCartButton(){
    if(!this.authService.isLoggedIn){
      this.router.navigate(['/login']);
    }else{
      let userID = this.authService.loginUser.user_id;
      let payload = {
        product_id: this.sendData._id,
        qty: 1,
        user_id: userID,
      };
      this.cartService.addCart(payload).subscribe((result)=>{
        this.cartStatus.emit(result.MESSAGE);
        this.authService.pageReload.next(true);
      })
    }
  }
  
}
