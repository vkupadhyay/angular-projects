import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-qty',
  templateUrl: './qty.component.html',
  styleUrls: ['./qty.component.scss']
})
export class QtyComponent implements OnInit {
  qtyNum: number = 1;
  price: number;
  @Input() itemId: number;
  @Input() itemPrice: number;
  @Input() itemCartPrice: number;
  @Input() itemQty: number;
  plusBtnDisabled: boolean = false;
  minusBtnDisabled: boolean = false;

  constructor(private cartService: CartService, private authService: AuthService,  private router: Router, ) { }

  ngOnInit(): void {
    this.qtyNum = this.itemQty;
    this.plusbuttonDisabled();
    this.minusbuttonDisabled();
  }

  minus() {
    if (this.authService.isLoggedIn) {
      if (this.qtyNum > 1) {
        this.qtyNum = this.qtyNum - 1;
        let minusPrice = this.itemCartPrice - this.itemPrice;
        this.price = minusPrice;
      } else {
        this.qtyNum = 1;
        this.price = this.itemPrice;
      }
      this.qtyCount();
      window.location.reload();
    } else {
      this.router.navigate(['/login']);
    }


  }

  plus() {
    if (this.authService.isLoggedIn) {
      if (this.qtyNum <= 9) {
        this.qtyNum = this.qtyNum + 1;
        let plusPrice = this.itemPrice * this.qtyNum
        this.price = plusPrice;
      } else {
        this.qtyNum = this.qtyNum;
        this.price = this.itemCartPrice;
      }
      this.qtyCount();
      window.location.reload();
    } else {
      this.router.navigate(['/login']);
    }
  }

  qtyCount() {
    if (this.authService.isLoggedIn) {
      let userId = this.authService.loginUser.user_id
      const queryObj: Partial<object> = { user_id: userId, qty: this.qtyNum, product_id: this.itemId, cart_price: this.price };
      this.cartService.updateQty(queryObj).subscribe((result) => {
        console.log(result);
      })
    } else {
      this.router.navigate(['/login']);
    }
  }

  plusbuttonDisabled() {
    if (this.itemQty >= 9) {
      this.plusBtnDisabled = true;
    } else {
      this.plusBtnDisabled = false;
    }
  }

  minusbuttonDisabled() {
    if (this.itemQty === 1) {
      this.minusBtnDisabled = true;
    } else {
      this.minusBtnDisabled = false;
    }
  }


}
