import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service'

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  userId: string;
  
  constructor( private authService:AuthService) { }

  ngOnInit(): void {
   this.userId = this.authService.loginUser.user_id;
   console.log(this.userId);
  }

}
