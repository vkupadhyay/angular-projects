import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { CategoriesList, ISellerProductLIst } from 'src/app/shared/interface/shared-i';
import { SellersService } from 'src/app/services/sellers/sellers.service';
import { ProductsService } from 'src/app/services/products/products.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;
  submitted = false;
  optionList: CategoriesList[];
  URL = environment.productImageApiUrl;
  public uploader: FileUploader = new FileUploader({ url: this.URL, itemAlias: 'image' });

  constructor( private formBuilder: FormBuilder, private sellerService: SellersService, private productsService: ProductsService, private authService: AuthService ) { }

  ngOnInit(): void {
    this.initializeForm();
    this.getCategorieList();
    this.setAttributesForFileUploader();
    this.getLoginUserDate();
  }

  getLoginUserDate() {
    this.addProductForm.patchValue({
      seller_id: this.authService.loginUser.user_id
    });
  }

  get f(): any {
    return this.addProductForm.controls;
  }

  setAttributesForFileUploader() {
    this.uploader.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.selectedImage(response);
    };
  }

  onImageSelect(event) {
    if (event && event.target && event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.uploader.uploadAll();
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  selectedImage(img: string) {
    this.addProductForm.patchValue({
      image: img
    });
  }

  getCategorieList() {
    this.sellerService.getCategoriesListService().subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.optionList = result.DATA;
      }
    }), (error: any) => {
      console.log(error);
    }
  }

  initializeForm() {
    this.addProductForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],
      image: ['', Validators.required],
      categorie_id: ['', Validators.required],
      status: ['', Validators.required],
      seller_id: ['', Validators.required],
    })
  }

  submit() {
    if (this.addProductForm.invalid) {
      this.submitted = true;
      return;
    }
    this.productsService.addProductService(this.addProductForm.value).subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.addProductForm.reset();
      }
    }), (error: any) => {
      console.log(error);
    }
  }


}
