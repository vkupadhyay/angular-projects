import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() panelClass: string;
  @Input() placeholder: string;
  @Input() getValue: string;
  @Output() dateSearchTriggered: EventEmitter<string> = new EventEmitter<string>();
  dateSearchControl: FormControl;
  maxDate = new Date();
  constructor() { 
    
  }

  ngOnInit() {
    this.createForm();
    this.registerSearchInputListeners();
  }

  createForm() {
    this.dateSearchControl = new FormControl('');
  }
  
  registerSearchInputListeners() {
    this.dateSearchControl.valueChanges.subscribe((text) => {
    this.dateSearchTriggered.emit(text);
    });
  }

}
