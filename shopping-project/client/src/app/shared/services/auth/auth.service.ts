import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpErrorResponse,} from '@angular/common/http';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { IloginData } from '../../interface/shared-i';
import {UserType} from '../../../core/utils/enums/user-type'
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isUserLogin =new BehaviorSubject<boolean>(false);
  pageReload = new BehaviorSubject<boolean>(false);
  userType: typeof UserType = UserType;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  dataObj:IloginData;
  localStorage: boolean = localStorage.length > 0;
  decodedToken: { [key: string]: string };
  tokenExpired: boolean;

  constructor(public router: Router) {
    
  }
 
  public getToken(): string {
    return localStorage.getItem('auth_token');
  }

  decodeToken() {
    if (!this.localStorage) {
      return true;
    }else{
      return false;
    }
  }

  getDecodeToken() {
    return jwt_decode(this.getToken());
  }

  getExpiryTime() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.exp : null;
  }

  isTokenExpired(): boolean {
    const expiryTime: any = this.getExpiryTime();
    if(expiryTime === null){
      return true;
    }else if (expiryTime) {
      this.tokenExpired = ((1000 * expiryTime) - (new Date()).getTime()) < 5000;
      return this.tokenExpired;
    } else {
      return false;
    }
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('auth_token');
    return authToken != null ? true : false;
  }

  
  get loginUser(): IloginData {
    let authToken = localStorage.getItem('auth_token');
   
    if(authToken){
      this.dataObj = jwt_decode(authToken);
      const obj= {
        user_id:  this.dataObj.user_id,
        first_name:  this.dataObj.first_name,
        last_name: this.dataObj.last_name,
        user_email: this.dataObj.user_email,
        gender: this.dataObj.gender,
        image: this.dataObj.image,
        phone_number: this.dataObj.phone_number,
        address: this.dataObj.address,
        user_type: this.dataObj.user_type,
        iat: this.dataObj.iat,
        exp: this.dataObj.exp,
      }
      return obj
    }
  }

  doLogout() {
    localStorage.clear(); 
    this.isUserLogin.next(false);
  }

  userAuthReload(){
    if(localStorage.getItem('auth_token')){
      this.router.navigate(['/']);
    }
  }

  adminAuthReload(){
    if(this.loginUser.user_type === this.userType.SELLER){
      this.router.navigate(['/seller']);
    }else if(this.loginUser.user_type === this.userType.CUSTOMER){
      this.router.navigate(['/customer']);
    }
  }

 

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

}
