export default class SystemConstants {
    public static readonly SUCCESS = 'SUCCESS';
    public static readonly CANCELLED_PRODUCT_STATUS = 'Cancelled';
    public static readonly PENDING_PRODUCT_STATUS = 'Pending';
    public static readonly PROCESS_PRODUCT_STATUS  = 'Process ';
    public static readonly SUCCESS_PRODUCT_STATUS  = 'Success ';
    public static readonly FAILURE = 'FAILURE';
    public static readonly EMAIL_NOT_MATCH = 'Email not match';
    public static readonly  ITEM_ALLREADY_HAVE_IN_CART = 'Item allready have in cart';
    public static readonly  YOUR_ACCOUNT_IS_INACTIVE = 'Your account is inactive';
  }
  