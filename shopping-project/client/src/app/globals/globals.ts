import { Injectable } from "@angular/core";
@Injectable()
export class Globals {

  /**
   * This is used for list pagination
   */
  adminPaginationAttrs = {
    statusSelected: 1,
    submitted: false,
    isShow: false,
    rotate: true,
    maxSize: 5,
    limits: 10,
    skips: 0,
    isIndex: -1,
    isActiveIdx: -1,
    childIndex: -1,
    count: 0,
    pageItems: [],
    selectedItemType: "", // Info or Edit Item
    itemArray: [] as any,
  };


}
export const UserType = {
  ADMIN : 'Admin',
  SELLER : 'Seller',
  CUSTOMER : 'Buyer'
}

export const CommonStatus = {
    ACTIVE: "active",
    DELETE: "delete",
    INACTIVE: "inactive",
    
  } as const;