import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from './shared/services/auth/auth.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  authToken = localStorage.getItem('auth_token');
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  getToken() {
    if(localStorage.getItem('auth_token')){
      return true;
    }else{
      return false;
    }
    // return localStorage.getItem('auth_token');
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.getToken){
        return true;
      }
      return false;
    }
  
}
