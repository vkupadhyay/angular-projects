import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from './auth-guard/auth.guard';
import { MessageComponent } from 'src/app/shared/components/message/message.component';
const routes: Routes = [
  { path: '', loadChildren: () => import('./module/home/home.module').then(m => m.HomeModule) },
  { path: 'message', canActivate: [AuthGuardGuard], component: MessageComponent },
  { path: 'login', loadChildren: () => import('./module/login/login.module').then(m => m.LoginModule) }, 
  { path: 'sign-up', loadChildren: () => import('./module/sign-up/sign-up.module').then(m => m.SignUpModule) },
  { path: 'admin', canActivate: [AuthGuardGuard], loadChildren: () => import('./module/admin/admin.module').then(m => m.AdminModule) },
  
  { path: 'users', canActivate: [AuthGuardGuard], loadChildren: () => import('./module/users/users.module').then(m => m.UsersModule) },
  { path: 'profile/:name', canActivate: [AuthGuardGuard], loadChildren: () => import('./module/profile/profile.module').then(m => m.ProfileModule) }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
