export enum UserGenderType{
    MALE = 'male',
    FEMALE = 'female'
};
export enum UserAccountType{
    SELLER = 1,
    BUYER = 2
};