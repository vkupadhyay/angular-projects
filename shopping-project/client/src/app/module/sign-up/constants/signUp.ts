import { UserAccountType, UserGenderType } from "../enums/signUpEnum";
export const UserGenderList = [
    {
        id: UserGenderType.MALE,
        label: "Male"
    },
    {
        id: UserGenderType.FEMALE,
        label: "Female"
    }
] as const

export const UserAccountList = [
    {
        id: UserAccountType.BUYER,
        label: "Buyer"
    },
    {
        id: UserAccountType.SELLER,
        label: "Seller"
    }
] as const