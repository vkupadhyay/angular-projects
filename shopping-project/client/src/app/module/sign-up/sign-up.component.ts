import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/login-signup/login-signup.service';
import { AuthService } from '../../shared/services/auth/auth.service';
import { environment } from '../../../environments/environment';
import SystemConstants from 'src/app/shared/constants/SystemConstants'
import { UserAccountList, UserGenderList } from './constants/signUp'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  sAttributes: any = {};
  signUpForm: FormGroup;
  submitted = false;
  URL = environment.userImageApiUrl;
  userAccountList: typeof UserAccountList = UserAccountList;
  userGenderList: typeof UserGenderList = UserGenderList;

  public uploader: FileUploader = new FileUploader({ url: this.URL, itemAlias: 'image' });

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.userAuthReload();
    this.initializeSignUpForm();
    this.setAttributesForFileUploader();
  }

  setAttributesForFileUploader() {
    this.uploader.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.selectedAvtarAndImage(response);
    };
  }

  onImageSelect(event) {
    if (event && event.target && event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.uploader.uploadAll();
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  selectedAvtarAndImage(img: string) {
    this.signUpForm.patchValue({
      image: img
    });
  }

  initializeSignUpForm() {
    this.signUpForm = this.formBuilder.group({
      name: this.formBuilder.group({
        first_name: ['', [Validators.required],],
        last_name: ['', [Validators.required],],
      }),
      email: ['', Validators.required],
      image: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phone_number: ['', [Validators.required, Validators.minLength(10)]],
      gender: ['', [Validators.required]],
      user_type: ['', [Validators.required]],
      address: ['', [Validators.required]],
    });
  }

  get f(): any {
    return this.signUpForm.controls;
  }

  get userNameForm(): any {
    const nameForm = this.signUpForm.get('name') as FormGroup;
    return nameForm.controls;
  }

  submit() {
    if (this.signUpForm.invalid) {
      this.submitted = true;
      return;
    }
    this.userService.signUp(this.signUpForm.value).subscribe(res => {
      this.router.navigate(['/login']);
    }, error => {
      if (error.STATUS === SystemConstants.FAILURE) {
        if (error.DATA.keyPattern.email === 1) {
          this.sAttributes.emailValidation = true;
        } else {
          this.sAttributes.phoneValidation = true;
        }
      } else {
        console.log(error);
      }
    });
  }

}
