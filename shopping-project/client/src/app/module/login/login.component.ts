import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/login-signup/login-signup.service';
import {AuthService} from 'src/app/shared/services/auth/auth.service';
import {UserType} from '../../core/utils/enums/user-type'
import SystemConstants from 'src/app/shared/constants/SystemConstants'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  sAttributes: any = {};
  signInForm: FormGroup;
  submitted = false;
  userType: typeof UserType = UserType;

  constructor( 
    private formBuilder: FormBuilder, 
    private userService: UserService, 
    private router: Router, 
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.authService.userAuthReload();
    this.initializeSignInForm();
  }

  initializeSignInForm(){
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get f(): any {
    return this.signInForm.controls;
  }

  submit(){
    if (this.signInForm.invalid) {
      this.submitted = true;
      return;
    }else{
      this.userService.signIn(this.signInForm.value).subscribe(result =>{
        localStorage.setItem('auth_token', result.DATA);
        this.authService.isUserLogin.next(true);
         if(this.authService.loginUser.user_type === this.userType.ADMIN){
           this.router.navigate(['/admin']);
         }else if (this.authService.loginUser.user_type === this.userType.SELLER) {
          let userName = this.authService.loginUser.first_name;
          this.router.navigate(['/profile/',userName]);
         } else {
          this.router.navigate(['/']);
         }
        }, error => {
        if(error.STATUS === SystemConstants.FAILURE){
          if(error.DATA === SystemConstants.EMAIL_NOT_MATCH){
            this.sAttributes.emailValidation = true;
            
          }
          else{
            if(error.DATA === SystemConstants.YOUR_ACCOUNT_IS_INACTIVE){
              this.sAttributes.accountValidation = true;
            }else{
              this.sAttributes.passwordValidation = true;
            }
            
          }
        }else{
          console.log(error);
        }
      })
    }
  }

}
