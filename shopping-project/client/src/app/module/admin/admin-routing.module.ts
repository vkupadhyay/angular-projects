import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { UsersComponent } from './components/users/users.component';
import { ProductsComponent } from './components/products/products.component';
import { SellerComponent } from './components/seller/seller.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { OrdersComponent } from './components/orders/orders.component';
const routes: Routes = [
  { path: '', component: AdminComponent },
  {
    component: UsersComponent,
    path: 'users',
  },
  {
    component: ProductsComponent,
    path: 'products',
  },{
    component: SellerComponent,
    path: 'seller',
  },{
    component: CategoriesComponent,
    path: 'categories',
  },
  {
    component: OrdersComponent,
    path: 'orders',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
