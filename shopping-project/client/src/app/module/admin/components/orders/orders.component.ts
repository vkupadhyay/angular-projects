import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import {OrderService} from 'src/app/services/order/order.service'
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { IAdminOrderItem, sellerDetails, UserDetails } from 'src/app/shared/interface/shared-i';
import {UsersService} from 'src/app/services/users/users.service';
import { OrderStatusItems } from 'src/app/core/utils/constants/label';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  sAttributes: any = {};
  btnActive: string = "Success";
  searchText:string;
  orderStatus: string;
  orderItems: IAdminOrderItem[];
  sellerInfo: sellerDetails[];
  userInfo: UserDetails[];
  sellerIdx: number = -1;
  userIdx: number = -1;
  orderStatusType: typeof OrderStatusItems = OrderStatusItems;
  constructor( private orderService:OrderService, private usersService:UsersService) { }

  ngOnInit(): void {
    this.getOrderListAndCount();
  }

  searchByText(text: string){
    this.searchText = text;
    this.getOrderListAndCount();
  }

  sendUserStatus(status: string){
    this.btnActive = status;
    this.orderStatus = status;
    this.getOrderListAndCount();
  }

  setSearchNameQuery(queryObj: any) {
    if (this.searchText) {
      queryObj.name = this.searchText;
    }else{
      queryObj.name = '';
    }
  }

  setOrderStatusQuery(queryObj: any) {
    if (this.orderStatus) {
      queryObj.status = this.orderStatus;
    }else{
      queryObj.status = 'Success';
    }
  }

  listOrder(){
    const queryObj: Partial<object> = { };
    this.setOrderStatusQuery(queryObj);
    this.setSearchNameQuery(queryObj);
    this.orderService.adminOrderList(queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.orderItems = result.DATA;
          if (result.DATA.length) {
            this.sAttributes.notFoundMessage = '';
            this.sAttributes.messageShow = false;
          } else {
            this.sAttributes.notFoundMessage = 'Item not found';
            this.sAttributes.messageShow = true;
          }
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  orderCount(){
    const queryObj: Partial<object> = {};
    this.setSearchNameQuery(queryObj);
    this.setOrderStatusQuery(queryObj);
    this.orderService.adminOrderCount(queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        let getCountArr = [
          result.DATA[0].Success_count, result.DATA[0].Process_count, result.DATA[0].Pending_count,  result.DATA[0].Cancelled_count
        ];
        
        for (let i = 0; i < this.orderStatusType.length; ++i){
          this.orderStatusType[i].count = getCountArr[i] 
        }
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  getOrderListAndCount():void{
    forkJoin([
     this.listOrder(),
     this.orderCount()
    ])
  }

  viewSeller(sellerId: string, orderIndex: number){
    this.sellerIdx = orderIndex;
    this.userIdx = -1;
    this.usersService.adminGetSellerInfo(sellerId).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
          this.sellerInfo = result.DATA;
      }
    })
  }

  viewUser(userId: string, orderIndex: number){
    this.userIdx = orderIndex;
    this.sellerIdx = -1;
    this.usersService.userDetails(userId).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
          this.userInfo = result.DATA;
      }
    })
  }

}
