import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { CategoriesList } from 'src/app/shared/interface/shared-i';
import {CategoriesService} from '../../../../services/categories/categories.service'

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  addCategoriesForm: FormGroup;
  isAddCategoriesFormShow: boolean = false;
  submitted = false;
  categoriesList:CategoriesList[];
  constructor( private formBuilder: FormBuilder, private categoriesService: CategoriesService) { }

  ngOnInit(): void {
    this.initializeForm();
    this.getCategoriesList();
  }

  addCategoriesFormShowBtn(){
    this.isAddCategoriesFormShow = true;
  }

  closeBox(){
    this.isAddCategoriesFormShow = false;
  }

  get f(): any {
    return this.addCategoriesForm.controls;
  }

  initializeForm(){
     this.addCategoriesForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
     })
  }

  submit(){
    if (this.addCategoriesForm.invalid) {
      this.submitted = true;
      return;
    }
    this.categoriesService.addCategorie(this.addCategoriesForm.value).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.isAddCategoriesFormShow = false;
        this.addCategoriesForm.reset();
        this.getCategoriesList();
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  getCategoriesList(){
    this.categoriesService.getCategoriesListService().subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.categoriesList = result.DATA;
      }
    }),(error: any) => {
      console.log(error);
     }
  }

}
