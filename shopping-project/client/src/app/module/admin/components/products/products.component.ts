import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { IProductLIst, CategoriesList } from 'src/app/shared/interface/shared-i';
import { ProductsService } from 'src/app/services/products/products.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import {AdminService} from 'src/app/services/admin/admin.service';
import { SellersService } from 'src/app/services/sellers/sellers.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @Input() panelClass: string;
  sAttributes: any = {};
  searchText:string;
  productStatus: string;
  btnActive: string = "active";
  activeCount: number = 0;
  inactiveCount: number = 0;
  productList: IProductLIst[];
  editProductForm: FormGroup;
  submitted = false;
  optionList:CategoriesList[];
  getIndex: number = -1;
  productDetails: any;
  editProductImg: string;
  editProductId: string;
  constructor(private sellerService:SellersService, private productsService:ProductsService, private formBuilder: FormBuilder, private adminService:AdminService, ) {}

  ngOnInit(): void {
    this.getProductListAndCount();
    this.initializeForm();
    this.getCategorieList();
  }

  get f(): any {
    return this.editProductForm.controls;
  }

  searchByText(text: string){
    this.searchText = text;
    this.getProductListAndCount();
  }

  sendUserStatus(status: string){
    this.getIndex = -1;
    this.productStatus = status;
    this.btnActive = status;
    this.getProductListAndCount();
  }

  setProductStatusQuery(queryObj: any) {
    if (this.productStatus) {
      queryObj.status = this.productStatus;
    }else{
      queryObj.status = 'active';
    }
  }

  setSearchNameQuery(queryObj: any) {
    if (this.searchText) {
      queryObj.name = this.searchText;
    }else{
      queryObj.name = '';
    }
  }

  listProducts(){
    const queryObj: Partial<object> = { };
    this.setProductStatusQuery(queryObj);
    this.setSearchNameQuery(queryObj);
    this.productsService.adminProductList(queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.productList = result.DATA;
          if (result.DATA.length) {
            this.sAttributes.notFoundMessage = '';
            this.sAttributes.messageShow = false;
          } else {
            this.sAttributes.notFoundMessage = 'Item not found';
            this.sAttributes.messageShow = true;
          }
      }
    }),(error: any) => {
      console.log(error);
     }
    
  }

  productCount(){
    const queryObj: Partial<object> = {};
    this.setSearchNameQuery(queryObj);
    this.setProductStatusQuery(queryObj);
    this.productsService.adminProductCount(queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.activeCount = result.DATA[0].Active_count;
        this.inactiveCount = result.DATA[0].Delete_count;
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  getProductListAndCount():void{
    forkJoin([
      this.listProducts(),
      this.productCount()
    ])
  }

  getCategorieList(){
    this.sellerService.getCategoriesListService().subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.optionList = result.DATA;
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  update(index: number, data:any){
    this.sAttributes.data = data;
    this.editProductForm.patchValue(this.sAttributes.data);
    this.getIndex = index;
    this.editProductImg =  this.sAttributes.data.image;
    this.editProductForm.get('status').setValue(this.sAttributes.data.status);
    this.editProductId = this.sAttributes.data._id;
    
  }

  initializeForm(){
    this.editProductForm = this.formBuilder.group({
     name: ['', Validators.required],
     price: ['', Validators.required],
     description: ['', Validators.required],
     categorie_id: ['', Validators.required],
     status: ['', Validators.required],
    })
  }

  editProductFormSubmit(id:string){
    if (this.editProductForm.invalid) {
      this.submitted = true;
      return;
    }
    this.productsService.productEditAdmin(this.editProductId, this.editProductForm.value).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.getIndex = -1;
        this.ngOnInit();
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  closeBox(){
    this.getIndex = -1;
  }

}
