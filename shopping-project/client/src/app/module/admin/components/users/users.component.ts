import { Component, Input, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { IListUser } from 'src/app/shared/interface/shared-i';
import { UsersService } from '../../../../services/users/users.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants'
import { UserType} from 'src/app/globals/globals';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @Input() panelClass: string;
  sAttributes: any = {};
  searchText:string;
  userStatus: string;
  customerList:IListUser[];
  btnActive: string = 'active';
  activeCount: number = 0;
  inactiveCount: number = 0;


  constructor( private userService:UsersService) { }

  ngOnInit(): void {
    this.getUserListAndCounts();
  }
  
  sendUserStatus(status:string){
    this.userStatus = status;
    this.btnActive = status;
    this.getUserListAndCounts();
  }

  setUserStatusQuery(queryObj: any) {
    if (this.userStatus) {
      queryObj.status = this.userStatus;
    }else{
      queryObj.status = 'active';
    }
  }

  searchByText( text: string) {
    this.searchText = text;
    this.getUserListAndCounts();
  }

  setSearchEmailQuery(queryObj: any) {
    if (this.searchText) {
      queryObj.email = this.searchText;
    }else{
      queryObj.email = '';
    }
  }

  listUsers(){
    const queryObj: Partial<object> = {user_type: UserType.CUSTOMER };
    this.setSearchEmailQuery(queryObj);
    this.setUserStatusQuery(queryObj);
    this.userService.userList(queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.customerList = result.DATA;
          if (result.DATA.length) {
            this.sAttributes.notFoundMessage = '';
            this.sAttributes.messageShow = false;
          } else {
            this.sAttributes.notFoundMessage = 'Item not found';
            this.sAttributes.messageShow = true;
          }
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  countUsers(){
    const queryObj: Partial<object> = {user_type: UserType.CUSTOMER };
    this.setSearchEmailQuery(queryObj);
    this.setUserStatusQuery(queryObj);
    this.userService.userCount(queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.activeCount = result.DATA[0].Active_count;
        this.inactiveCount = result.DATA[0].Delete_count;
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  getUserListAndCounts(): void {
    forkJoin([
      this.countUsers(),
      this.listUsers()
    ]);
  }

}
