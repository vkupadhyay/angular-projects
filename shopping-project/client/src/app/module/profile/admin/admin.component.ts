import { Component, Input, OnInit } from '@angular/core';
import { UserStatusLinks } from 'src/app/shared/constants/constants';
import { IloginData } from 'src/app/shared/interface/shared-i';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  @Input() userData: IloginData;
  index: number = -1;
  userStatusLinks: typeof UserStatusLinks = UserStatusLinks;
  constructor() { }

  ngOnInit(): void {
  }
  toggle(index:number){
    this.index = index;
  }

}
