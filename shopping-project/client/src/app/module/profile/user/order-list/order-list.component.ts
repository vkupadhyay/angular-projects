import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { OrderStatusItems } from 'src/app/core/utils/constants/label';
import { OrderService} from 'src/app/services/order/order.service';
import { UsersService} from 'src/app/services/users/users.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { IGetOrderListOfUser } from 'src/app/shared/interface/shared-i';
import {AuthService} from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  userId: string;
  orderStatusType: typeof OrderStatusItems = OrderStatusItems;
  btnActive: string = 'Success';
  orderStatus: string;
  searchedProduct: string;
  orderListOfUser: IGetOrderListOfUser[];

  constructor( private orderService:OrderService, private authService:AuthService, private usersService:UsersService ) { }

  ngOnInit(): void {
    this.userId = this.authService.loginUser.user_id;
    this.getOrderListAndCounts();
  }

  sendUserStatus(status:string){
    this.orderStatus = status;
    this.btnActive = status;
    this.getOrderListAndCounts();
  }
  
  searchByText(text: string) {
    this.searchedProduct = text;
    this.getOrderListAndCounts();
  }

  setOrderStatusQuery(queryObj: any) {
    if (this.orderStatus) {
      queryObj.status = this.orderStatus;
    }else{
      queryObj.status = this.btnActive;
    }
  }

  setSearchNameQuery(queryObj: any) {
    if (this.searchedProduct) {
      queryObj.name = this.searchedProduct;
    } else {
      queryObj.name = '';
    }
  }

  productOrderList(){
    const queryObj: Partial<object> = { };
    this.setOrderStatusQuery(queryObj);
    this.setSearchNameQuery(queryObj);
    this.orderService.getOrderListOfUser(this.userId, queryObj).subscribe((result)=>{
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.orderListOfUser = result.DATA;
      }
    })
  }

  productOrderCount(){
    const queryObj: Partial<object> = { };
    this.setOrderStatusQuery(queryObj);
    this.setSearchNameQuery(queryObj);
    this.orderService.getOrderCountOfUser(this.userId, queryObj).subscribe((result)=>{
      if (result.STATUS === SystemConstants.SUCCESS) {
      
        let getCountArr = [
          result.DATA[0].Success_count, result.DATA[0].Process_count, result.DATA[0].Pending_count,  result.DATA[0].Cancelled_count
        ];
        
        for (let i = 0; i < this.orderStatusType.length; ++i){
          this.orderStatusType[i].count = getCountArr[i] 
        }
         
      }
    })
  }

  getOrderListAndCounts(): void {
    forkJoin([
      this.productOrderList(),
      this.productOrderCount()
    ]);
  }

}
