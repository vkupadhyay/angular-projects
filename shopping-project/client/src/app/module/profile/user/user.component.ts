import { Component, Input, OnInit } from '@angular/core';
import { IloginData } from 'src/app/shared/interface/shared-i';
import {UserStatusLinks} from 'src/app/shared/constants/constants';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() userData: IloginData;
  index: number = -1;
  userStatusLinks: typeof UserStatusLinks = UserStatusLinks;
  constructor() { }

  ngOnInit(): void {
  }
  toggle(index:number){
    this.index = index;
  }

}
