import { Component, OnInit } from '@angular/core';
import { IloginData } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  userData: IloginData;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.userData = this.authService.loginUser;
  }

}
