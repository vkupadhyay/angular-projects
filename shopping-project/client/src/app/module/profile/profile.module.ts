import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { UserComponent } from './user/user.component';
import { SellerComponent } from './seller/seller.component';
import { AdminComponent } from './admin/admin.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { GetOrdersListComponent } from './seller/get-orders-list/get-orders-list.component';
import {SharedModule} from 'src/app/shared/shared.module';

import { SellerProductComponent } from './seller/seller-product/seller-product.component';
import { OrderListComponent } from './user/order-list/order-list.component';


@NgModule({
  declarations: [ProfileComponent, UserComponent, SellerComponent, AdminComponent, UserInfoComponent, GetOrdersListComponent, SellerProductComponent, OrderListComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule

  ]
})
export class ProfileModule { }
