import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { IloginData } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  memberTypeUser: boolean = false;
  memberTypeAdmin: boolean = false;
  memberTypeSeller: boolean = false;
  userData: IloginData;
  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.getUserType();
  }
  getUserType() {
    this.userData = this.authService.loginUser;
    if (this.authService.loginUser.user_type === 'Buyer') {
      this.memberTypeUser = true;
    } else if (this.authService.loginUser.user_type === 'Seller') {
      this.memberTypeSeller = true;
    } else {
      this.memberTypeAdmin = true;
    }
  }
}
