import { Component, OnInit, Type } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { ISellerOrderList } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { UsersService } from 'src/app/services/users/users.service';
import { forkJoin } from 'rxjs';
import { OrderStatusItems } from 'src/app/core/utils/constants/label';


@Component({
  selector: 'app-get-orders-list',
  templateUrl: './get-orders-list.component.html',
  styleUrls: ['./get-orders-list.component.scss']
})
export class GetOrdersListComponent implements OnInit {
  sAttributes: any = {};
  userId: string;
  orderItemList: ISellerOrderList[];
  orderStatus: string;
  btnActive: string = 'Success';
  orderStatusType: typeof OrderStatusItems = OrderStatusItems;
  selectedTeam: string;
  isIndex: number = -1;
  buyerUserName: string = '';
  buyerUserAddress: string;
  constructor(private orderService: OrderService, private authService: AuthService, private usersService:UsersService) { }

  ngOnInit(): void {
    this.userId = this.authService.loginUser.user_id;
    this.getOrderListAndCounts();

  }

  sendUserStatus(status: string) {
    this.orderStatus = status;
    this.btnActive = status;
    this.getOrderListAndCounts();
  }

  setOrderStatusQuery(queryObj: any) {
    if (this.orderStatus) {
      queryObj.status = this.orderStatus;
    } else {
      queryObj.status = this.btnActive;
    }
  }

  productOrderList() {
    const queryObj: Partial<object> = {};
    this.setOrderStatusQuery(queryObj);
    this.orderService.sellerOrderList(this.userId, queryObj).subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.orderItemList = result.DATA;
      }
    })
  }

  sellerProductUpdate(items: any, status: string) {
    let id = items.order_item._id;
    const queryObj: Partial<object> = { id, status };
     this.orderService.orderStatus(queryObj).subscribe((result)=>{
       if(result.STATUS === SystemConstants.SUCCESS){}
     });
    
    let message = '';
    switch (status) {
      case 'Success':
        message = `your item has been delivered successfully`
        break;

      case 'Process':
        message = ` your item has been process`
        break;

      case 'Cancelled':
        message = `your item has been cancelled by `
        break;

      case 'Pending':
        message = `your item has been pending`
        break;
    }

    const messageQueryObj: Partial<object> = {
      product_id: items.order_item.product_id,
      user_id: items.user_id,
      seller_id: items.order_item.seller_id,
      message: message
    };
    
    this.orderService.messageSent(messageQueryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.ngOnInit();
      }
    });
  }

 
  getOrderListAndCounts(): void {
    forkJoin([
      this.productOrderList()
    ]);
  }

 

}
