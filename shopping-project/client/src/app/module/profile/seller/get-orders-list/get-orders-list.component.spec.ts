import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetOrdersListComponent } from './get-orders-list.component';

describe('GetOrdersListComponent', () => {
  let component: GetOrdersListComponent;
  let fixture: ComponentFixture<GetOrdersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetOrdersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetOrdersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
