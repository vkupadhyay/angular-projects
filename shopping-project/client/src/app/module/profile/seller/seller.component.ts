import { Component, Input, OnInit } from '@angular/core';
import { SellerStatusLinks } from 'src/app/shared/constants/constants';
import { IloginData } from 'src/app/shared/interface/shared-i';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.scss']
})
export class SellerComponent implements OnInit {
  @Input() userData: IloginData;
  index: number = -1;
  popHideShow: boolean;
  userStatusLinks: typeof SellerStatusLinks = SellerStatusLinks;
  constructor() { }

  ngOnInit(): void {
  }
  
  toggle(index:number){
    this.index = index;
  }

}
