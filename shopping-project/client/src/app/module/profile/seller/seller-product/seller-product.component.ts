import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products/products.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { ISellerProductLIst } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-seller-product',
  templateUrl: './seller-product.component.html',
  styleUrls: ['./seller-product.component.scss']
})
export class SellerProductComponent implements OnInit {
  sAttributes: any = {};
  editProductForm: FormGroup;
  isTabActive: string = 'active';
  searchedProduct: string;
  userId: string;
  sellerProductList: ISellerProductLIst[];
  activeCount: number = 0;
  inactiveCount: number = 0;
  index: number = -1;
  
  submitted = false;
  constructor( private formBuilder: FormBuilder,  private productsService: ProductsService, private authService: AuthService ) { }

  ngOnInit(): void {
    this.userId = this.authService.loginUser.user_id;
    this.getProductListAndCounts();
    this.initializeForm();
  }

  searchByText(text: string) {
    this.searchedProduct = text;
    this.getProductListAndCounts();
  }

  sendStatus(status: string) {
    this.isTabActive = status;
    this.getProductListAndCounts();
  }

  setProductStatusQuery(queryObj: any) {
    if (this.isTabActive) {
      queryObj.status = this.isTabActive;
    } else {
      queryObj.status = 'active';
    }
  }

  setSearchNameQuery(queryObj: any) {
    if (this.searchedProduct) {
      queryObj.name = this.searchedProduct;
    } else {
      queryObj.name = '';
    }
  }

  getSellerProductList(status?: string) {
    const queryObj: Partial<object> = {};
    this.setSearchNameQuery(queryObj);
    this.setProductStatusQuery(queryObj);
    this.productsService.sellerProductList(this.userId, queryObj).subscribe((data: any) => {
      if (data.STATUS === SystemConstants.SUCCESS) {
        this.sellerProductList = data.DATA;
      }
    }), (error: any) => {
      console.log(error);
    }
  }

  countProduct(){
    const queryObj: Partial<object> = {};
    this.setSearchNameQuery(queryObj);
    this.setProductStatusQuery(queryObj);
    this.productsService.productCount(this.userId, queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.activeCount = result.DATA[0].Active_count;
        this.inactiveCount = result.DATA[0].Delete_count;
      }
    }),(error: any) => {
      console.log(error);
     }
  }

  getProductListAndCounts(): void {
    forkJoin([
      this.countProduct(),
      this.getSellerProductList()
    ]);
  }

  get f(): any {
    return this.editProductForm.controls;
  }

  initializeForm() {
    this.editProductForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],
      status: ['', Validators.required],
    })
  }

  editButton(productId: string, productItemIndex: number, item: any){
    this.index = productItemIndex;
    this.sAttributes.productId = productId;
    this.editProductForm.patchValue(item);
  }

  submit(){
    this.productsService.editProductService(this.sAttributes.productId,  this.editProductForm.value).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        this.getProductListAndCounts();
        this.index = -1;
      }
    })
  }

}
