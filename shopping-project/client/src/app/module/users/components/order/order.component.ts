import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { IOrderList } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  loginUserId: string;
  orderItems: IOrderList[];
  constructor( 
    private orderService: OrderService, 
    private authService: AuthService 
  ) {}

  ngOnInit(): void {
    this.loginUserId = this.authService.loginUser.user_id;
    this.getOrderList();
  }

  getOrderList() {
    this.orderService.orderList(this.loginUserId).subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.orderItems = result.DATA;
      }
    })
  }

  orderCancel(productId:string, orderId:string){
    const obj = {
      status: SystemConstants.CANCELLED_PRODUCT_STATUS,
      product_id: productId
    }
    this.orderService.orderCancel(orderId, obj).subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.orderItems = result.DATA;
        this.getOrderList();
      }
    })
  }

}
