import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import { OrderService } from 'src/app/services/order/order.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { ICartItemsList, IloginData } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  cartList: ICartItemsList[];
  totalAmount: number = 0;
  show: boolean = false;
  loginUserData: IloginData;
  deleteMessage: string;
  finallyPrice: number = 458;
  orderItemList: ICartItemsList[];
  message: string;
  constructor(
    private cartService: CartService,
    private authService: AuthService,
    private router: Router,
    private orderService:OrderService
  ) { }

  ngOnInit(): void {
    this.getCartList();
    this.getUserData();
  }
  
  getUserData() {
    this.loginUserData = this.authService.loginUser;
  }

  getCartList() {
    if (this.authService.isLoggedIn) {
      let userId = this.authService.loginUser.user_id;
      const queryObj: Partial<object> = { user_id: userId };
      this.cartService.cartList(queryObj).subscribe((result) => {
        if (result.STATUS === SystemConstants.SUCCESS) {
          this.cartList = result.DATA;
          if (this.cartList.length) {
          
            for (let i = 0; i < this.cartList.length; i++) {
              let b = this.cartList[i].price;
              let n = Number(b);
              this.totalAmount += n;
              
            }
          } else {
            this.totalAmount = 0;
            this.show = true;
            this.message = "Cart list empty"
          }
        }
      }),
        (error: any) => {
          console.log(error);
        };
    } else {
      console.log('You are not login');
    }
  }

  removeItemFromCart(itemId: string, price: number) {
    this.cartService.deleteItem(itemId).subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.deleteMessage = result.MESSAGE;
        this.show = true;
        this.message = "Product removed successfully";
        window.location.reload();
        
        setTimeout(() => {
          this.show = false;
        }, 3000);
      }
    });
  }

  order(orderItem: any) {
    this.orderItemList = orderItem;
    const result = this.orderItemList.map(({ _id, ...rest }) => ({ ...rest }));
    let queryObj: any = { user_id: this.loginUserData.user_id, order_item: result };
    
    // this.orderService.order(queryObj).subscribe((result) => {
    //   if (result.STATUS === SystemConstants.SUCCESS) {
    //     this.router.navigate(['users/order']);
    //   }
    // });
    
    
    for (var _i = 0; _i < orderItem.length; _i++) {
      var num = orderItem[_i].name;
      console.log(num);
    }

    


  }

}
