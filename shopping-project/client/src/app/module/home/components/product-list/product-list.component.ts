import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth/auth.service'
import { CommonService } from 'src/app/services/common/common.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { IProductLIst } from 'src/app/shared/interface/shared-i';
import { UserType } from 'src/app/core/utils/enums/user-type'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  sAttributes: any = {};
  productList: IProductLIst[];
  limitCount: number = 4;
  showAddToCartButton: boolean = true;
  cartMessage: string;
  show: boolean = false;
  index: number = -1;
  className: string;

  constructor(private commonService: CommonService, private authService: AuthService) { }

  ngOnInit(): void {
    this.checkLogin();
    this.getProductList();
  }

  checkLogin() {
    if (this.authService.isLoggedIn) {
      if (this.authService.loginUser.user_type === UserType.ADMIN || this.authService.loginUser.user_type === UserType.SELLER) {
        this.showAddToCartButton = false
      }
    }
  }

  loadMore() {
    this.limitCount = this.limitCount + 4;
    this.getProductList();
  }

  setLimitQuery(queryObj: any) {
    if (this.limitCount) {
      queryObj.limit = this.limitCount;
    }
  }


  allProductList() {
    const queryObj: Partial<object> = {};
    this.setLimitQuery(queryObj);
    this.commonService.productList(queryObj).subscribe((result) => {
      if (result.STATUS === SystemConstants.SUCCESS) {
        this.productList = result.DATA;
        console.log(this.productList);
      }
      this.productList = result.DATA;
    }), (error: any) => {
      console.log(error);
    }
  }

  getProductList(): void {
    forkJoin([
      this.allProductList()
    ]);
  }

  getStatus(text: string, index: number) {
    if (text === SystemConstants.ITEM_ALLREADY_HAVE_IN_CART) {
      this.className = 'error'
    } else {
      this.className = 'success'
    }
    this.index = index;
    this.show = true;
    this.cartMessage = text;
  }

}
