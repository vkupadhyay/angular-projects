import { Component, Input, OnInit } from '@angular/core';
import {ProductsService} from 'src/app/services/products/products.service';
import { ActivatedRoute } from '@angular/router';
import { IProductDetails } from 'src/app/shared/interface/shared-i';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  @Input() sendData: any;
  productDetails:IProductDetails[];
  qtyNum:number = 1;
  newPrice: number ;
  className: string;
  cartMessage: string;
  show: boolean = false;
  finallyPrice: number = 458;
 
  constructor( private productsService:ProductsService, private route:ActivatedRoute, public authService: AuthService) { }

  ngOnInit(): void {
   this.productData();
   this.getStatus();
   
  }
  
  productData(){
    let id = this.route.snapshot.params.id;
    let newProductId = id.substring(1);
    const queryObj: any = {};
    if (this.authService.isLoggedIn) {
      queryObj.user_id = this.authService.loginUser.user_id
    }else{
      queryObj.user_id = ''
    }
    
    this.productsService.productDetsils(newProductId, queryObj).subscribe((result)=>{
      if(result.STATUS === SystemConstants.SUCCESS){
        if(this.authService.isLoggedIn){
          this.productDetails = result.DATA;
        }else{
          this.productDetails = result.DATA;
        }
        this.finallyPrice = this.productDetails[0].price * this.productDetails[0].cart.qty;
      }
    }),(error: any) => {
      console.log(error);
    }
  }

 

  getStatus(text?: string, index?: number){
    if(text === SystemConstants.ITEM_ALLREADY_HAVE_IN_CART){
       this.className = 'error';
     }else{
       this.className = 'success'
     }
     this.show = true;
     this.cartMessage = text;
   }
}
