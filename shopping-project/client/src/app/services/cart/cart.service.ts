import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { baseUrlApi } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor( private http: HttpClient, public router: Router ) { }

  addCart(payload: any): Observable<any> {
    let api = `${baseUrlApi}/add-to-cart`;
    return this.http.post(api, payload).pipe(catchError(this.handleError));
  }

  cartList(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/cart`;
    return this.http.get(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  countCartItem(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/cart-item-count`;
    return this.http.get(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  updateQty(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/cart-qty`;
    return this.http.get(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  deleteItem(id:any): Observable<any> {
    let api = `${baseUrlApi}/cart-item-remove/${id}`;
    return this.http.delete(api).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.status === 0) {
      errorMessage = error.error

    } else {
      errorMessage = error.error
    }
    return throwError(errorMessage);
  }
}
