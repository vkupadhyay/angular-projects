import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { baseUrlApi } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) { }

  order(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/order`;
    return this.http.post(api, qObj).pipe(catchError(this.handleError));
  }

  orderList(id:string): Observable<any> {
    let api = `${baseUrlApi}/order-list/${id}`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  orderCancel(id:string, obj:any): Observable<any> {
    let api = `${baseUrlApi}/order-cancel/${id}`;
    return this.http.put(api, obj).pipe(catchError(this.handleError));
  }

  sellerOrderList(id:string, qObj:any): Observable<any> {
    let api = `${baseUrlApi}/seller-order-list/${id}`;
    return this.http.get(api,  {params: qObj}).pipe(catchError(this.handleError));
  }

  orderStatus(obj:any): Observable<any> {
    let api = `${baseUrlApi}/seller-order-status`;
    return this.http.put(api,  {params: obj}).pipe(catchError(this.handleError));
  }

  adminOrderList(obj:any): Observable<any> {
    let api = `${baseUrlApi}/admin-order-list`;
    return this.http.get(api,  {params: obj}).pipe(catchError(this.handleError));
  }

  adminOrderCount(obj:any): Observable<any> {
    let api = `${baseUrlApi}/admin-order-count`;
    return this.http.get(api,  {params: obj}).pipe(catchError(this.handleError));
  }

  getOrderListOfUser(id: string, qObj:any): Observable<any> {
    let api = `${baseUrlApi}/users/order-list/${id}`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  getOrderCountOfUser(id: string, qObj:any): Observable<any> {
    let api = `${baseUrlApi}/users/order-count/${id}`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  messageSent(obj:any): Observable<any> {
    let api = `${baseUrlApi}/message`;
    return this.http.post(api,  obj).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.status === 0) {
      errorMessage = error.error

    } else {
      errorMessage = error.error
    }
    return throwError(errorMessage);
  }
}
