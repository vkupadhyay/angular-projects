import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {baseUrlApi  } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) {

  }

  

  productList(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/product-for-sell/`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any ;
        if (error.status === 0) {
          errorMessage = error.error
          
        } else {
          errorMessage = error.error
        }
      return throwError(errorMessage);
  }
}
