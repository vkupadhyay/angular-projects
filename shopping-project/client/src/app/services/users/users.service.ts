import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { baseUrlApi } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IUserOrderListQuery } from 'src/app/shared/interface/query-i';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private manageFoodBaseUrl: string = baseUrlApi;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) {}
  
  userList(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/users/`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  userCount(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/users-count/`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  adminGetSellerInfo(id:string): Observable<any> {
    let api = `${baseUrlApi}/admin-seller-info/${id}`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  userDetails(id:string): Observable<any> {
    let api = `${baseUrlApi}/admin-user-info/${id}`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }
 
  private handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.status === 0) {
      errorMessage = error.error

    } else {
      errorMessage = error.error
    }
    return throwError(errorMessage);
  }
}
