import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { baseUrlApi } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IAddProduct } from 'src/app/shared/interface/shared-i';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor( private http: HttpClient, public router: Router ) { }
  
  adminProductList(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/admin-products-list/`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  adminProductCount(qObj:any): Observable<any> {
    let api = `${baseUrlApi}/admin-products-count/`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  productEditAdmin(id:string, obj:any): Observable<any> {
    let api = `${baseUrlApi}/product-edit/${id}`;
    return this.http.put(api, obj).pipe(catchError(this.handleError));
  }

  addProductService(addProduct: IAddProduct): Observable<any> {
    let api = `${baseUrlApi}/add-product`;
    return this.http.post(api, addProduct).pipe(catchError(this.handleError));
  }

  editProductService(id: string, obj: any): Observable<any> {
    let api = `${baseUrlApi}/product-edit/${id}`;
    return this.http.put(api, obj).pipe(catchError(this.handleError));
  }

  sellerProductList(id:string, qObj:any): Observable<any> {
    let api = `${baseUrlApi}/seller-product-list/${id}`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  productCount(id:string, qObj:any): Observable<any> {
    let api = `${baseUrlApi}/seller-product-count/${id}`;
    return this.http.get<any[]>(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  productDetsils(id: string, qObj:any): Observable<any> {
    let api = `${baseUrlApi}/product-details/${id}`;
    return this.http.get(api, {params: qObj}).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.status === 0) {
      errorMessage = error.error

    } else {
      errorMessage = error.error
    }
    return throwError(errorMessage);
  }
}
