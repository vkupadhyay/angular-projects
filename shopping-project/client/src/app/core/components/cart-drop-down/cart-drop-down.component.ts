import { Component, OnInit } from '@angular/core';
import {CartService} from 'src/app/services/cart/cart.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { ICartItemsList } from 'src/app/shared/interface/shared-i';
import {AuthService} from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-cart-drop-down',
  templateUrl: './cart-drop-down.component.html',
  styleUrls: ['./cart-drop-down.component.scss']
})
export class CartDropDownComponent implements OnInit {
  cartList: ICartItemsList[];
  show:boolean = false;
  constructor( private cartService:CartService, private authService:AuthService) { }

  ngOnInit(): void {
    this.getCartList();
   
  }
  

  getCartList(){
    if(this.authService.isLoggedIn){
        let userId = this.authService.loginUser.user_id;
        const queryObj: Partial<object> = {user_id: userId};
        this.cartService.cartList(queryObj).subscribe((result)=>{
          if(result.STATUS === SystemConstants.SUCCESS && result.DATA.length){
            this.cartList = result.DATA;
          }else{
            this.show = true;
          }
        }),(error: any) => {
          console.log(error);
         }
    }else{
      console.log("b");
    }
  }



}
