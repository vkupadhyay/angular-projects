import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { IloginData } from 'src/app/shared/interface/shared-i';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import {CartService} from 'src/app/services/cart/cart.service';
import SystemConstants from 'src/app/shared/constants/SystemConstants';
import { UserType } from '../../utils/enums/user-type';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  sAttributes: any = {};
  logoImage:string = '../../../../assets/img/logo.png';
  isUserLogin: boolean = false;
  isHomePage: boolean = false;
  showCartButton: boolean = true;
  itemCount: number = 0;
  constructor( public authService: AuthService, private router: Router, private cartService:CartService) { }

  ngOnInit(): void {
    this.checkPageUrl();
    this.getCartItemCount();
    this.authService.isUserLogin.subscribe(res =>{
      this.isUserLogin = res;
    });
    this.reloadApi();
  }

  logout(){
    this.authService.doLogout();
  }

  reloadApi(){
    this.authService.pageReload.subscribe((res)=>{
      if(res){
        this.getCartItemCount();
      }
    })
  }

  checkPageUrl(){
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
           if(this.router.url === '/'){
            this.isHomePage = true;
           }else{
            this.isHomePage = false;
           }
        }
      }
    );
  }

  getCartItemCount(){
    if(this.authService.isLoggedIn){
      if(this.authService.loginUser.user_type === UserType.ADMIN || this.authService.loginUser.user_type === UserType.SELLER){
       this.showCartButton = false;
      }else{
      let userId = this.authService.loginUser.user_id;
      const queryObj: Partial<object> = {user_id: userId};
      this.cartService.countCartItem(queryObj).subscribe((result)=>{
        if(result.STATUS === SystemConstants.SUCCESS && result.DATA.length > 0){
          this.itemCount = result.DATA[0].count;
        }
      }),(error: any) => {
        console.log(error);
       }
      }
      
  }else{
    this.itemCount = 0;
  }
  }

}
