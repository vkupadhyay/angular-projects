import { OrderStatus} from "../enums/user-type";
export const OrderStatusItems = [
    {
        id: OrderStatus.SUCCESS,
        label: "Success",
        count: 0
    },
    {
        id: OrderStatus.PROCESS,
        label: "Process",
        count: 0
    },
    {
        id: OrderStatus.PENDING,
        label: "Pending",
        count: 0
    },
    {
        id: OrderStatus.CANCELLED,
        label: "Cancelled",
        count: 0
    },
]; 

