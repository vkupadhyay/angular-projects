export enum UserType {
    ADMIN = 'Admin',
    SELLER = 'Seller',
    CUSTOMER = 'Buyer'
}

export enum OrderStatus {
    SUCCESS = 'Success',
    PROCESS = 'Process',
    PENDING = 'Pending',
    CANCELLED = 'Cancelled'
}