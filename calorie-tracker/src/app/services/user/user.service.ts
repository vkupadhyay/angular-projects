import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment} from '../../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) {
    
   }
   userList(): Observable<any> {
    let api = `${environment.apiUrl}/user-list`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any ;
        if (error.status === 0) {
          errorMessage = error.error 
          
        } else {
          errorMessage = error.error
        }
      return throwError(errorMessage);
  }
}
