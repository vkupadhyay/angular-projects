import { Component } from '@angular/core';
import { UserService } from '../../services/user/user.service';
@Component({
  selector: 'app-user-list',
  standalone: true,
  imports: [],
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.scss'
})
export class UserListComponent {
  userList: [] = [];
  constructor(private userService: UserService) { }
  
  ngOnInit(): void {
    this.getUserList();
  }

  getUserList(){
    this.userService.userList().subscribe((data:any)=>{
      this.userList = data.DATA;
      console.log(this.userList)
    }),(error: any) => {
      console.log(error);
     }
  }
}
