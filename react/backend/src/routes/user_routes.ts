import { Application, Request, Response } from 'express';
import { UserController } from '../controllers/userController';

export class UserRoutes {

    private user_controller: UserController = new UserController();

    public route(app: Application) {
        
        app.post('/api/user', (req: Request, res: Response) => {
            this.user_controller.create_user(req, res);
        });

        app.get('/api/users', (req: Request, res: Response) => {
            this.user_controller.user_list(req, res);
        });

        app.post('/api/login', (req: Request, res: Response) => {
            this.user_controller.userLogin(req, res);
        });
       

    }
}