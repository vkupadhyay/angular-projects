import { Application, Request, Response } from 'express';
import { AdminController } from '../controllers/adminController';

export class AdminRoutes {

    private admin_controller: AdminController = new AdminController();

    public route(app: Application) {
        
        app.post('/api/admin/addbanner', (req: Request, res: Response) => {
            this.admin_controller.create_user(req, res);
        });
       

    }
}