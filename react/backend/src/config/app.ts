import express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from 'mongoose';
import { UserRoutes } from "../routes/user_routes";
import { CommonRoutes } from "../routes/common_routes";

class App {

   public app: express.Application;
   public MONGODB_URI = 'mongodb://localhost:27017/lmdweb'; // Replace with your MongoDB connection string
   private user_routes: UserRoutes = new UserRoutes();
   private common_routes: CommonRoutes = new CommonRoutes();


   constructor() {
      this.app = express();
      this.config();
      this.mongoSetup();
      this.user_routes.route(this.app);
      this.common_routes.route(this.app);
   }

   private config(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use((req, res, next) => {
       res.setHeader('Access-Control-Allow-Origin', '*');
       res.setHeader('Access-Control-Allow-Headers', 'Origin, x-access-token, token, Content-Type, Accept, Authorization');
       res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
       res.setHeader('Content-Type', 'text/plain');
       if (req.method == 'OPTIONS') {
          res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
          return res.status(200).json({});
       }

       next();
    });
   }

   private mongoSetup(): void {
      mongoose.set('strictQuery', true);
      mongoose.connect(this.MONGODB_URI, {});
   }

}
export default new App().app;