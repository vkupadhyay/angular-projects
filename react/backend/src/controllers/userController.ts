import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import { IUser } from '../modules/users/model';
import users from '../modules/users/schema'
import e = require('express');
import bcrypt from "bcrypt";
const TOKEN_KEY = "@QEGTUI";
import jwt from "jsonwebtoken";
export class UserController {
    
//   User Sign up
    public async create_user(req: Request, res: Response) {
            try{
                if (req.body.name && req.body.email &&  req.body.password &&
                    req.body.phone_number &&
                    req.body.gender) {
                        const salt = await bcrypt.genSalt(10);
                        const newPassword = await bcrypt.hash(req.body.password, salt);
                        const user_params: IUser = {
                            name: req.body.name,
                            email: req.body.email,
                            password: newPassword,
                            phone_number: req.body.phone_number,
                            gender: req.body.gender,
                            modification_notes: [{
                                modified_on: new Date(Date.now()),
                                modified_by: '',
                                modification_note: 'New user created'
                            }],
                        };
                        const _session = new users(user_params);
                        let a = await _session.save();
                        successResponse( "User successfully Create",  a, res)
                } else {
                        insufficientParameters(res);
                }
            }catch(error){
                failureResponse(error, res);
            }
        }

    public async user_list(req: Request, res: Response){
        try {
            const user = await users.find({});
            successResponse( "User successfully Create",  user, res)
          } catch (error) {
            failureResponse(error, res);
          }
    }

    public async userLogin(req: Request, res: Response){
        console.log(req.body)
        const { email, password } = req.body;
        try {
            let user = await users.findOne({ email });
            if (!user) {
                console.log("email")
                return failureResponse('Email Id not match', res);
            }else{
                bcrypt.compare(password, user.password, function (error, result) {
                   if(!result){
                    console.log("password")
                    failureResponse('Password not match', res);
                   }else{
                    const auth_token = jwt.sign(
                        {
                          user_id: user._id,
                          name: user.name,
                          email: user.email,
                          phone_number: user.phone_number,
                          is_admin: user.is_admin,
                        },
                        TOKEN_KEY,
                        {
                          expiresIn: "12h",
                        }
                      );
                      successResponse("Sign In successfully", auth_token, res);
                   }
                });
             }
          } catch (error) {
            console.log("error")
            failureResponse(error, res);
          }
    }

    
 

    
}