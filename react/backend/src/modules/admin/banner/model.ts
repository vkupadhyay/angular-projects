import { ModificationNote } from "../../common/model";

export interface IBanner {
    _id?: String;
    title: string;
    is_deleted?: Boolean;
    modification_notes: ModificationNote[]
}