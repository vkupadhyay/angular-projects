import { ModificationNote } from "../common/model";

export interface IUser {
    _id?: String;
    name: string;
    email: String;
    password: string;
    phone_number: string;
    gender: String;
    is_admin?: Boolean;
    is_deleted?: Boolean;
    modification_notes: ModificationNote[]
}