import { useContext } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Header from './components/header/header';
import GlobalStyle from '../src/assets/Styles/globalStyles';
import Footer from './components/footer/footer';
import Home from './components/home/home';
import About from './components/home/about';
import Career from './components/home/career';
import Client from './components/home/client';
import Services from './components/home/services';
import Industry from './components/home/industry';
import Blog from './components/home/blog';
import Team from './components/home/team';
import SignIn from './components/signin/signin';
import SignUp from './components/signup/signup';
import User from './components/user/user';
import Admin from './components/admin/admin.';
import AuthProvider, { AuthContext } from './context/AuthContext';

function App() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <GlobalStyle />
        <Header />
        <div className="mid-container">
          <Routes>
           
            <Route path="/" element={<PublicRoute><Home /></PublicRoute>} />
            <Route path="/about" element={<PublicRoute><About /></PublicRoute>} />
            <Route path="/career" element={<PublicRoute><Career /></PublicRoute>} />
            <Route path="/client" element={<PublicRoute><Client /></PublicRoute>} />
            <Route path="/services" element={<PublicRoute><Services /></PublicRoute>} />
            <Route path="/industry" element={<PublicRoute><Industry /></PublicRoute>} />
            <Route path="/blog" element={<PublicRoute><Blog /></PublicRoute>} />
            <Route path="/team" element={<PublicRoute><Team /></PublicRoute>} />
            
            <Route path="/user" element={<PrivateRoute><User /></PrivateRoute>} />
            <Route path="/admin" element={<PublicRoute><Admin /></PublicRoute>} />
            <Route path="/sign-in" element={<PublicRoute><SignIn /></PublicRoute>} />
            <Route path="/sign-up" element={<PublicRoute><SignUp /></PublicRoute>} />
          </Routes>
        </div>
        <Footer />
      </BrowserRouter>
    </AuthProvider>
  );
}

const PrivateRoute = ({ children }) => {
  const { auth } = useContext(AuthContext);
  return auth.isAuthenticated ? children : <Navigate to="/sign-in" />;
};

PrivateRoute.propTypes = {
  children: PropTypes.node.isRequired,
};

const PublicRoute = ({ children }) => {
  const { auth } = useContext(AuthContext);

  return !auth.isAuthenticated ? children : <Navigate to="/" />;
};

PublicRoute.propTypes = {
  children: PropTypes.node.isRequired,
};

export default App;
