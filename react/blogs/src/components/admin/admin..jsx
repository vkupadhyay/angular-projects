import { useState } from "react"; 
import AdminBanner from "../../components/admin/parts/banner.jsx";
import AdminBlog from "../../components/admin/parts/blogs.jsx";
import AdminCareer from "../../components/admin/parts/career.jsx";
import AdminClient from "../../components/admin/parts/client.jsx";
import AdminServices from "../../components/admin/parts/services.jsx";
import AdminIndustry from "../../components/admin/parts/industry.jsx";

const menuItems = [ 
  { label: "Banner", url: "#banner" }, 
  { label: "Services", url: "#services" }, 
  { label: "Industry", url: "#industry" }, 
  { label: "Client", url: "#client" }, 
  { label: "Career", url: "#career" }, 
  { label: "Blogs", url: "#blogs" }, 
]; 
const Admin = () => { 

  const [activeSection, setActiveSection] = useState("#banner"); 
  const handleClick = (url) => { setActiveSection(url); };

   return (
  <div className="admin-page">
    <div className="admin-page-left">
      <ul> {menuItems.map((item, index) => (
        <li key={index}>
          <button onClick={()=> handleClick(item.url)} className={activeSection === item.url ? "active" : ""} > {item.label} </button>
        </li> ))} </ul>
    </div>
    <div className="admin-page-right">
      <div id="banner" className={ activeSection==="#banner" ? "sectionshow" : "sectionhide" }> <AdminBanner /> </div>
      <div id="services" className={ activeSection==="#services" ? "sectionshow" : "sectionhide" }> <AdminServices /> </div>
      <div id="industry" className={ activeSection==="#industry" ? "sectionshow" : "sectionhide" }> <AdminIndustry /> </div>
      <div id="client" className={ activeSection==="#client" ? "sectionshow" : "sectionhide" }> <AdminClient /> </div>
      <div id="career" className={ activeSection==="#career" ? "sectionshow" : "sectionhide" }> <AdminCareer /> </div>
      <div id="blogs" className={activeSection==="#blogs" ? "sectionshow" : "sectionhide"}> <AdminBlog /> </div>
    </div>
  </div> 
  ); 
}; 
  export default Admin;