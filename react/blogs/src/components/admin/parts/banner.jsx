import { PrimarySmallButton, PrimaryButton } from "../../../assets/Styles/buttonsStyles";
import { useState } from "react";
const AdminBanner = () => {

  const [formData, setFormData] = useState({title: ""});
  const { title } = formData;
  const addNewSubmit = async (e) => {
    e.preventDefault();
    setFormData({
      title: ""
    });
    console.log(title);
  };
  const addNewChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value || "" });
  };
 
  return (
    
      <section className="admin-banner-section">
          <div className="admin-top">
             <h1>Banner</h1>
             <div className="admin-top-right">
                 <PrimarySmallButton>Add New</PrimarySmallButton>
             </div>
          </div>
          <div className="admin-btm">
              <div className="add-new-section">
                   <h2>Add new</h2>
                   <div className="add-new-form container-50">
                       <form onSubmit={addNewSubmit}>
                        <div className="input-item">
                            <input
                                type="text"
                                name="title"
                                value={title || ''}
                                onChange={addNewChange}
                                placeholder="title"
                            />
                        </div>
                        <div className="input-submit">
                            <PrimaryButton type="submit">Submit</PrimaryButton>
                        </div>
                        
                       </form>
                   </div>
              </div>
          </div>
      </section>
    
     
    
  );
};

export default AdminBanner;