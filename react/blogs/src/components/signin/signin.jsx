import { useState, useContext } from "react";
import axiosInstance from "../../api/axiosInstance";
import { AuthContext } from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";
import { SmContainer, CustomRow } from "../../assets/Styles/globalStyles";
import {
  LoginRoundBox,
  LoginLeftBox,
  LoginRightBox,
} from "../../assets/Styles/loginStyle";
import { InputField } from "../../assets/Styles/inputStyles";
import { PrimaryButton } from "../../assets/Styles/buttonsStyles";
import { jwtDecode } from "jwt-decode";

const SignIn = () => {
  const [formData, setFormData] = useState({ email: "", password: "" });
  const [message, setMessage] = useState("");
  const { login } = useContext(AuthContext);
  let navigate = useNavigate();
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
  
    const { email, password } = formData;
  
    if (!email || !password) {
      setMessage("Please fill in all fields.");
      return;
    }
  
    try {
      const response = await axiosInstance.post("/api/login", formData);
      const { error, MESSAGE, DATA } = response.data;
  
      if (error) {
        setMessage(MESSAGE || "Login failed. Please try again.");
        return;
      }
  
      login(DATA);
      const decoded = jwtDecode(DATA);
      const redirectPath = decoded.is_admin ? "/admin" : "/user";
      navigate(redirectPath);
    } catch (error) {
      setMessage(`Login failed: ${error.message || "An unexpected error occurred."}`);
    }
  };
  

  return (
    <>
      <SmContainer>
        <LoginRoundBox>
          <CustomRow>
            <LoginLeftBox>
              <div className="login__logo">
                <img src="../../login-logo.svg" alt="Login Logo" />
              </div>
              <div className="desc">
                <p className="text-align-center white-text">
                  Welcome to the wellbux administrative login. If you are an
                  admin or a merchant, please sign in to access your dashboard.
                </p>
                <p className="text-align-center white-text">
                  If you are registering for or participating in a challenge,
                  please download the app and log in with your user account.
                </p>
              </div>
            </LoginLeftBox>
            <LoginRightBox>
              <h1>Sign In</h1>
              {message && <div className="error-message">{message}</div>}
              <form onSubmit={handleSubmit}>
                <InputField>
                  <input
                    type="text"
                    name="email"
                    value={formData.email}
                    onChange={handleChange}
                    placeholder="Email"
                  />
                </InputField>
                <InputField>
                  {" "}
                  <input
                    type="password"
                    name="password"
                    value={formData.password}
                    onChange={handleChange}
                    placeholder="Password"
                  />
                </InputField>
                <PrimaryButton type="submit" className="margin-top-2">
                  Login
                </PrimaryButton>
              </form>
            </LoginRightBox>
          </CustomRow>
        </LoginRoundBox>
      </SmContainer>
    </>
  );
};

export default SignIn;
