// eslint-disable-next-line no-unused-vars
import React, { Component } from "react";
import { CustomRow, Container } from "../../assets/Styles/globalStyles";
import { FooterSection,FooterLeft, FooterRight } from "../../assets/Styles/footerStyles";
export default class Footer extends Component {
  render() {
    return (
      <FooterSection>
        <Container>
          <CustomRow>
             <FooterLeft><p>© 2024 Wellbux, Inc. All Rights reserved</p></FooterLeft>
             <FooterRight>
                <ul>
                    <li><a href="#">About Us</a> </li>
                    <li><a href="#">Terms of Use</a> </li>
                    <li><a href="#">About Us</a> </li>
                </ul>
             </FooterRight>
          </CustomRow>
        </Container>
      </FooterSection>
    );
  }
}
