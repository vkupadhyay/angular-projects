import { SmContainer, CustomRow } from "../../assets/Styles/globalStyles";
import {
  LoginRoundBox,
  LoginLeftBox,
  LoginRightBox,
} from "../../assets/Styles/loginStyle";
import { InputField } from "../../assets/Styles/inputStyles";
import { PrimaryButton } from "../../assets/Styles/buttonsStyles";
import { useState } from "react";
import axiosInstance from "../../api/axiosInstance";
import "../../App.scss";
import { useNavigate  } from 'react-router-dom';
const SignUp = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
    phone_number: "",
    gender: "",
  });
  let navigate = useNavigate();
  const { name, email, password, phone_number, gender } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value || "" });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!name || !email || !password || !phone_number || !gender) {
      console.log("Please fill in all fields.");
      return;
    }
    try {
      console.log("Submitting form data:", formData);
      const response = await axiosInstance.post('/api/user', formData);
      if(response.data.error === false){
        console.log("Response:", response.data); 
        navigate('/admin');
      }else{
        console.log("Response:", "not", response.data.MESSAGE.errorResponse.errmsg); 
      }
      
     
      setFormData({
        name: "",
        email: "",
        password: "",
        phone: "",
        gender: "",
      });
    } catch (error) {
      console.error("Error:", error.response ? error.response.data : error.message);
     
    }
  };

  return (
    <>
      <SmContainer>
        <LoginRoundBox>
          <CustomRow>
            <LoginLeftBox>
              <div className="login__logo">
                <img src="../../login-logo.svg" alt="Login Logo" />
              </div>
              <div className="desc">
                <p className="text-align-center white-text">
                  Welcome to the wellbux administrative login. If you are an
                  admin or a merchant, please sign in to access your dashboard.
                </p>
                <p className="text-align-center white-text">
                  If you are registering for or participating in a challenge,
                  please download the app and log in with your user account.
                </p>
              </div>
            </LoginLeftBox>
            <LoginRightBox>
              <h1>Sign Up</h1>
              <form onSubmit={handleSubmit}>
                <InputField>
                  <input
                    type="text"
                    name="name"
                    value={name || ''}
                    onChange={handleChange}
                    placeholder="Name"
                  />
                </InputField>
                <InputField>
                  <input
                    type="text"
                    name="email"
                    value={email || ''}
                    onChange={handleChange}
                    placeholder="Email"
                  />
                </InputField>
                <InputField>
                  <input
                    type="password"
                    name="password"
                    value={password || ''}
                    onChange={handleChange}
                    placeholder="Password"
                  />
                </InputField>
                <InputField>
                  <input
                    type="number"
                    name="phone_number"
                    value={phone_number || ''}
                    onChange={handleChange}
                    placeholder="Phone"
                  />
                </InputField>
                <InputField>
                  <input
                    type="text"
                    name="gender"
                    value={gender || ''}
                    onChange={handleChange}
                    placeholder="Gender"
                  />
                </InputField>
                <PrimaryButton type="submit" className="margin-top-2">
                  Sign Up
                </PrimaryButton>
              </form>
            </LoginRightBox>
          </CustomRow>
        </LoginRoundBox>
      </SmContainer>
    </>
  );
};

export default SignUp;
