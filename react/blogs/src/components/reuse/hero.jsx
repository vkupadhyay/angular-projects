
import Slider from 'react-slick';
import { SliderItem, SliderItemImg, SliderItemContent } from "../../assets/Styles/hero";
const Hero = () => {
  const settings = {
    dots: true,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    
      <section className="hero-section">
        <Slider {...settings}>
          <SliderItem>
            <SliderItemImg>
               <img src=".../../slider1.jpg" />
            </SliderItemImg>
            <SliderItemContent>
                  <div className='container'>
                      <h1>We follow a transparent methodology</h1>
                  </div>
            </SliderItemContent>
          </SliderItem>

          <SliderItem>
            <SliderItemImg>
               <img src=".../../slider1.jpg" />
            </SliderItemImg>
            <SliderItemContent>
                  <div className='container'>
                      <h1>We follow a transparent methodology</h1>
                  </div>
            </SliderItemContent>
          </SliderItem>
        
        </Slider>
      </section>
    
     
    
  );
};

export default Hero;