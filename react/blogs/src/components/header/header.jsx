import { useContext, useState } from "react";
import { useLocation } from 'react-router-dom';
import { CustomRow } from "../../assets/Styles/globalStyles";
import {
  HeaderSection,
  Logo,
  Rightheader,
  Nav,
  MenuIcon,
} from "../../assets/Styles/headerStyles";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";

const Header = () => {
  const { auth, logout } = useContext(AuthContext);
  const [isOpen, setIsOpen] = useState(false);
  const location = useLocation();

  const handleLogout = () => {
    logout();
  };
  const handleMenuClick = () => {
    setIsOpen(!isOpen);
  };

  const handleCloseManu = () => {
    setIsOpen(false);
  };

  return (
    <>
      <HeaderSection>
        <CustomRow className="align-items-center">
          <Logo>
            <Link to="/">
              <img src=".../../logo.png" />
            </Link>
          </Logo>
          <Rightheader>
            <MenuIcon
              className={isOpen ? "open" : ""}
              onClick={handleMenuClick}
            />
          </Rightheader>
        </CustomRow>
      </HeaderSection>

      <Nav className={isOpen ? "open" : ""}>
        <ul>
          
   

          {auth.isAuthenticated ? (
            <>
              <li>{auth ? (auth.user ? auth.user.name : "") : ""}</li>
              <li onClick={handleLogout}>
                <Link to="/">Logout</Link>
              </li>{" "}
            </>
          ) : (
            <>
              <li ><Link className={location.pathname === '/' ? 'active' : ''} to="/" onClick={handleCloseManu}>HOME</Link></li>
              <li><Link className={location.pathname === '/about' ? 'active' : ''} to="/about" onClick={handleCloseManu}>ABOUT US</Link></li>
              <li><Link className={location.pathname === '/services' ? 'active' : ''} to="/services" onClick={handleCloseManu}>SERVICES</Link></li>
              <li><Link className={location.pathname === '/industry' ? 'active' : ''} to="/industry" onClick={handleCloseManu}>INDUSTRIES</Link></li>
              <li><Link className={location.pathname === '/client' ? 'active' : ''} to="/client" onClick={handleCloseManu}>CLIENTS</Link></li>
              <li><Link className={location.pathname === '/career' ? 'active' : ''} to="/career" onClick={handleCloseManu}>CAREERS</Link></li>
              <li><Link className={location.pathname === '/team' ? 'active' : ''} to="/team" onClick={handleCloseManu}>OUR TEAM</Link></li>
              <li><Link className={location.pathname === '/blog' ? 'active' : ''} to="/blog" onClick={handleCloseManu}>BLOGS</Link></li>
              {/* <li>
                  <Link to="/sign-in">Sign In</Link>
                </li>
                <li>
                  <Link to="/sign-up">Sign Up</Link>
                </li> */}
            </>
          )}
        </ul>
      </Nav>
    </>
  );
};

export default Header;
