import styled from "styled-components";

export const LoginRoundBox = styled.div`
  background-color: var(--white-color);
  border-radius: 10px;
`;
export const LoginLeftBox = styled.div`
      width: 50%;
      background: linear-gradient(to right, #00669b 0%, #333bb1 100%);
      border-radius: 10px 0 0 10px;
      padding:3rem 1rem;
      .login__logo{
        margin: auto;
        width: 170px;
        margin-bottom:2rem;
      }
`;
export const LoginRightBox = styled.div`
    width: 50%;
    padding: 2rem;
    h1{
     margin: 0px;
     font-size:1.5rem;
     font-weight:500;
    }
`;
