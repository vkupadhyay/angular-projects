import styled from "styled-components";
export const PrimaryButton = styled.button`
               width: 100%;
               background-color: var(--primary-color);
               border: 0px;
               color: #fff;
               font-size:18px;
               padding: 1rem;
               border-radius:5px;
               &:hover{
                   background-color: #999999;  
               }
        `
;

export const PrimarySmallButton = styled.button`
               width: 100%;
               background-color: var(--primary-color);
               border: 0px;
               color: #fff;
               font-size:16px;
               padding: 10px 15px;
               border-radius:5px;
               cursor: pointer;
               font-weight: 300;
               &:hover{
                   background-color: #999999;  
               }
        `
;

export const SecondaryButton = styled.button`
               width: 100%;
               background-color: var(--secondary-color);
               border: 0px;
               color: #fff;
               font-size:18px;
               padding: 1rem;
               &:hover{
                   background-color: #FE6C0B;  
               }
        `
;


