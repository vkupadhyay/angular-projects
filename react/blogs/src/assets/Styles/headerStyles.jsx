import styled from "styled-components";
import menuIcon from "../images/menu-icon.svg";
import closeMenuIcon from "../images/close-menu-icon.svg";
export const HeaderSection = styled.header`
  width: 100%;
  border: 0px;
  color: #fff;
   background: #fff; 
  padding: 1rem;
  position: fixed;
  top: 0;
  box-shadow: rgba(0, 0, 0, 0.4) 0px 0px 7px;
  z-index: 99;,
    
`;

export const Logo = styled.div`
  width: 150px;

  img {
    width: 100%;
    display: block;
  }
`;
export const Rightheader = styled.div`
  margin-left: auto;
`;
export const MenuIcon = styled.button`
  background-image: url(${menuIcon});
  width: 50px;
  height: 46px;
  background-size: contain;
  background-repeat: no-repeat;
  cursor: pointer;
  background-position: center;
  border: none;
  background-color: transparent;
  padding: 0;
  &.open {
    background-image: url(${closeMenuIcon});
     width: 35px;
  height: 35px;
  }
`;

export const Nav = styled.div`
  position: fixed;
  right: -360px;
  top: 0px;
  z-index: 1;
  width: 300px;
  height: 100%;
  background: var(--primary-color);
  padding: 92px 0 0 0;
  &.open {
    right: 0px;
  }
  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    li {
      margin: 0;
      padding: 0;
      border-bottom: 1px solid var(--white-color);
      a {
        display: block;
        color: var(--white-color);
        font-size: var(--primary-size);
        padding: 1rem;
        font-weight: 300;
        &.active{
          background-color: #b7e3fb;
          color: #000;
      }
    }
  }
`;
