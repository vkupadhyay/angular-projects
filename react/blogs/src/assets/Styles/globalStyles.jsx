import styled, { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  *, *::before, *::after {
    box-sizing: border-box;
  }

  a{
      text-decoration: none;
  }

  body{
      background: var(--body-bg);
      padding: 0px;
      margin: 0px;
      font-family: 'Poppins', sans-serif;
      font-weight: 400;
      
  }
  .mid-container{
        padding:88px 0 0 0;
  }

  .desc{
    font-size:1rem;
  }

  .text-align-center{
   text-align:center;
  }

  .white-text{
   color: var(--white-color);
  }

  .align-items-start{
    align-items:start;
  }
    .align-items-center{
    align-items:center;
  }

  .margin-top-2{
    margin-top:2rem;
  }
     

  

`;

export const Container = styled.div`
  max-width: 1600px;
  margin: auto;
  padding: 0 15px;
`;

export const CustomRow = styled.div`
  display: flex;
`;

export const GirdRow = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: top;
`;

export const MainPadding = styled.div`
  padding: 1rem;
`;
export const ImgSize100 = styled.img`
  width: 100%;
  display: block;
`;

export const WhiteBgBox = styled.div`
   background: var(--white-color);
`;

export const SmContainer = styled.div`
   max-width: 1046px;
  margin: auto;
  padding: 0 15px;
`;

export default GlobalStyle;
