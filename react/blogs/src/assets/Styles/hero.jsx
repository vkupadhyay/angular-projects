import styled from "styled-components";

export const SliderItem = styled.div`
  width: 100%;
  position: relative;
`;

export const SliderItemImg = styled.div`
  width: 100%;
  img {
    width: 100%;
    height: auto;
    display: block;
  }
`;

export const SliderItemContent = styled.div`
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.5);
  }
  .container{
    position: relative;
    z-index: 1;
  }
    h1{
    font-size: 39px;
    color: #fff;
    font-family: "Roboto Slab", serif;
    font-weight: 100;
    }
`;
