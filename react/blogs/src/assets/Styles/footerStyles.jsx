import styled from "styled-components";
export const FooterSection = styled.footer`
  width: 100%;
  background: linear-gradient(to right, #00669b 0%, #333bb1 100%);
  padding: 1rem;
`;

export const FooterLeft = styled.div`
  width: 50%;
  p {
    margin: 0px;
    color: var(--white-color);
    font-size: 13px;
    font-weight: 300;
  }
`;

export const FooterRight = styled.div`
  width: 50%;
  ul {
    display: flex;
    margin: 0;
    padding: 0;
    list-style: none;
    justify-content: flex-end;
    li {
    padding: 0 0 0 1rem;
      a {
        color: var(--white-color);
        font-size: 13px;
    font-weight: 300;
      }
    }
  }
`;
