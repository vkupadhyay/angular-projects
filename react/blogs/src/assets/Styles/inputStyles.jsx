import styled from "styled-components";

export const InputField = styled.div`
    width: 100%;
    margin-top: 1rem;

    input[type="text"],
    input[type="email"],
    input[type="password"],
    input[type="number"],
    input[type="date"],
    textarea,
    select {
        border: 1px var(--border-border-color) solid;
        font-size: 1rem;
        color: var(--input-text-color);
        background: transparent;
        -webkit-appearance: none;
        border-radius: 5px;
        width: 100%;
        padding: 1rem;
       
        &:disabled {
            opacity: 0.5;
        }

        &::-webkit-input-placeholder {
            /* Chrome/Opera/Safari */
            font-style: normal;
            color: $white;
            opacity: 1;
        }

        &::-moz-placeholder {
            /* Firefox 19+ */
            font-style: normal;
            color: $white;
            opacity: 1;
        }

        &:-ms-input-placeholder {
            /* IE 10+ */
            font-style: normal;
            color: $white;
            opacity: 1;
        }

        &:-moz-placeholder {
            /* Firefox 18- */
            font-style: normal;
            color: $white;
            opacity: 1;
        }
    }
`;