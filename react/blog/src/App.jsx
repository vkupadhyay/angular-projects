import {
  BrowserRouter as Router,} from "react-router-dom";
import GlobalStyle from "./globalStyles";
import Header from "./components/header";
import Login from "./components/login";

function App() {
  return (
    <>
      <Router>
        <GlobalStyle />
        <Header />
        <Login />
      </Router>
    </>
  );
}

export default App;
