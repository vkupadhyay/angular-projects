// eslint-disable-next-line no-unused-vars
import React, { Component } from "react";
import {
  Container,
  MainPadding,
  InputField,
  Label,
  Input,
  Button
} from "../globalStyles";

export default class Login extends Component {
    state = {
        email: "",
        password: "",
    };

    add = (e) => {
        e.preventDefault();
        if (this.state.email === "" ||   this.state.password === "" ) {
          console.log("sdcdc")
            return;
        }
        else{
           console.log(this.state)
        }
    };


  render() {

    return (
      <Container>
        <MainPadding>
          <form onSubmit={this.add}>
            <InputField>
              <Label>Email</Label>
              <Input
                type="text"
                name="email"
                placeholder="email"
                value={this.state.email}
                onChange={(e) => this.setState({ email: e.target.value })}
              />
            </InputField>
            <InputField>
              <Label>Password</Label>
              <Input
                type="password"
                name="password"
                placeholder="password"
                value={this.state.password}
                onChange={(e) => this.setState({ password: e.target.value })}
              />
            </InputField>
            <InputField> <Button >Login</Button></InputField>
          </form>
          
        </MainPadding>
      </Container>
    );
  }
}
