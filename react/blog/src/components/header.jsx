// eslint-disable-next-line no-unused-vars
import React, { Component } from 'react'
import { ImgSize100 } from '../globalStyles'
export default class Header extends Component {
  render() {
    return (
      <section className='header'>
         <ImgSize100 src='../../header-img.png' alt='' />
       </section>
    )
  }
}

