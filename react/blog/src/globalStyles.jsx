import styled, { createGlobalStyle} from "styled-components";

const GlobalStyle = createGlobalStyle`

body{
     background: #fff;
     padding: 0px;
     margin: 0px;
     font-family: 'Poppins', sans-serif;
     font-weight: 300;
     
  }
  a{
      text-decoration: none;
  }
  *, *::before, *::after {
    box-sizing: border-box;
}
`;

export const Container = styled.div`
 max-width: 1400px;
 margin: auto;
 padding: 0 15px;
`;

export const CustomRow = styled.div`
 display: flex;
 align-items: center;
`;

export const GirdRow = styled.div`
 display: flex;
 flex-wrap: wrap;
 align-items: top;
`;
export const Input = styled.input`
 border: 1px #000 solid;
 font-size: 14px;
 color: #000;
 padding: 10px;
 width: 100%;
`;
export const Select = styled.select`
  border: 1px #000 solid;
 font-size: 14px;
 color: #000;
 padding: 10px;
 width: 100%;
`;
export const Textarea = styled.textarea`
   border: 1px #000 solid;
 font-size: 14px;
 color: #000;
 padding: 10px;
 width: 100%;
`;
export const InputField = styled.div`
margin-bottom: 10px;
`
export const MainPadding = styled.div`
padding:1rem;
`
export const ImgSize100 = styled.img`
   width:100%;
   display:block;
`
export const Label = styled.label`
display: block;
font-size: 14px;
color: #000;
`
export const Button = styled.button`
               width: 100%;
               background-color: #007fde;
               border: 0px;
               color: #fff;
               font-size:16px;
               padding: 10px;
               &:hover{
                   background-color: #FE6C0B;  
               }
        `

;


export default GlobalStyle
