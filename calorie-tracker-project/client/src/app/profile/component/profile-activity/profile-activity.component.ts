import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActivityService } from 'src/app/services/activity/activity.service';
import { IActivity, IDataApi } from 'src/app/util/interface/user';
import { AuthServiceService } from 'src/app/util/services/auth-service/auth-service.service';
import { GlobalService } from 'src/app/util/services/global/global.service';

@Component({
  selector: 'app-profile-activity',
  templateUrl: './profile-activity.component.html',
  styleUrls: ['./profile-activity.component.scss']
})
export class ProfileActivityComponent implements OnInit {
  sAttributes: any = {};
  userActivityData: IActivity;
  urlId:string = this.route.snapshot.paramMap.get('id');
  searchDate: Date;
  @Input() viewBtnShow: boolean;
  @Input() searchText: Date;

  constructor(
    public authServiceService:AuthServiceService, 
    private activityService:ActivityService,
    public GlobalService:GlobalService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getUserActivityData();
  }

  ngOnChanges() {
    this.storeSearchDate(this.searchText);
  }

  storeSearchDate(searchDate:Date){
    this.searchDate = searchDate;
    this.getUserActivityData();
  }
  getUserActivityData(){
    this.activityService.activityListByUserId(this.authServiceService.loginUser.user_id).subscribe((res:IDataApi)=>{
      let getUserActivityDataArray = res.DATA;
      if(this.searchDate){
        let filterData = getUserActivityDataArray.filter((item:any)=>{if(item.activity_date==this.searchDate){return item}});
        this.userActivityData = filterData;
      }else{
        this.userActivityData = getUserActivityDataArray;
      }
    })
  }

  sendDate(activityDate:string){
    this.GlobalService.getDate = activityDate;
  }
  
  deleteActivityData(id: string){
    let data = '';
    this.GlobalService.deleteActivityData(id, data).subscribe(
      res => console.log("Submited")
      , err => {
        if (err && err.error && err.error.error && err.error.error.message) {
          console.log('error');
      }
    });
  }

}
