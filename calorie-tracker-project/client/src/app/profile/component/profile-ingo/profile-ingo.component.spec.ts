import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileIngoComponent } from './profile-ingo.component';

describe('ProfileIngoComponent', () => {
  let component: ProfileIngoComponent;
  let fixture: ComponentFixture<ProfileIngoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileIngoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileIngoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
