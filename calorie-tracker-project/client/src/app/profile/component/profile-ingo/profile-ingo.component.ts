import { Component, OnInit } from '@angular/core';
import SystemConstants from 'src/app/util/constants/SystemConstants';
import { IProfileInfo } from 'src/app/util/interface/user';
import { AuthServiceService } from 'src/app/util/services/auth-service/auth-service.service';
import {ProfileService} from '../../../services/profile/profile.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'

@Component({
  selector: 'app-profile-ingo',
  templateUrl: './profile-ingo.component.html',
  styleUrls: ['./profile-ingo.component.scss']
})
export class ProfileIngoComponent implements OnInit {
  getProfileInfo: IProfileInfo[];
  getProfileInfoToEdit: IProfileInfo;
  isEditViewActive: boolean = true;
  loginUserId:string = this.authServiceService.loginUser.user_id;
  profileEditForm:FormGroup;

  constructor( private profileService:ProfileService, public authServiceService:AuthServiceService, private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.profileInfo();
    this.initializeForm();
    this.patchInfoInEditForm();
  }

  profileInfo(){
    this.profileService.userProfileInfo(this.loginUserId).subscribe((res:any)=>{
      if(res.STATUS === SystemConstants.SUCCESS){
        this.getProfileInfo = res.DATA;
        this.getProfileInfoToEdit = res.DATA;
      }
    },(error) => {
      console.log(error);
    });
  }

  initializeForm(){
    this.profileEditForm = this.formBuilder.group({
      name: ['' ,[Validators.required]],
      email: ['',[Validators.required]],
      weight: ['',[Validators.required]],
      height: ['',[Validators.required]],
      gender: ['',[Validators.required]],
      age: ['',[Validators.required]]
    })
    
  }
  
  patchInfoInEditForm(){
    console.log(this.authServiceService.loginUser.birth_date);
    this.profileEditForm.patchValue(
      {
        name: this.authServiceService.loginUser.name,
        email: this.authServiceService.loginUser.user_email,
        weight: this.authServiceService.loginUser.weight, 
        height: this.authServiceService.loginUser.height,
        gender: this.authServiceService.loginUser.gender,
        age: this.authServiceService.loginUser.birth_date,  
      }
    );
  }

  update(){
    console.log("clicked");
  }
  

}
