import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FoodService } from 'src/app/services/food/food.service';
import { IDataApi, IFood } from 'src/app/util/interface/user';
import { AuthServiceService } from 'src/app/util/services/auth-service/auth-service.service';
import { GlobalService } from 'src/app/util/services/global/global.service';

@Component({
  selector: 'app-profile-food',
  templateUrl: './profile-food.component.html',
  styleUrls: ['./profile-food.component.scss']
})
export class ProfileFoodComponent implements OnInit {
  sAttributes: any = {};
  userFoodData: IFood;
  searchDate: Date;

  @Input() viewBtnShow: boolean;
  @Input() searchText: Date;

  constructor(
    public authServiceService:AuthServiceService,
    private foodService:FoodService, 
    public GlobalService:GlobalService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
   this.getUserFoodData();
  }
 
  ngOnChanges() {
    this.storeSearchDate(this.searchText);
  }

  storeSearchDate(searchDate:Date){
    this.searchDate = searchDate;
    this.getUserFoodData();
  }

  getUserFoodData(){
    this.foodService.foodListByUserId(this.authServiceService.loginUser.user_id).subscribe((res:IDataApi)=>{
      let getUserFoodDataArray = res.DATA;
      if(this.searchDate){
        let filterData = getUserFoodDataArray.filter((item:any)=>{if(item.add_food_date==this.searchDate){return item}});
        this.userFoodData = filterData;
      }else{
        this.userFoodData = getUserFoodDataArray;
      }
    })
  }

  sendDate(activityDate:string){
    this.GlobalService.getDate = activityDate;
  }

  deleteFoodData(id: string){
    let data = '';
    this.GlobalService.deleteFoodData(id, data).subscribe(
      res => console.log("Submited")
      , err => {
        if (err && err.error && err.error.error && err.error.error.message) {
          console.log('error');
      }
    });
  }


}
