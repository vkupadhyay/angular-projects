import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileIngoComponent } from './component/profile-ingo/profile-ingo.component';
import { ProfileFoodComponent } from './component/profile-food/profile-food.component';
import { ProfileActivityComponent } from './component/profile-activity/profile-activity.component';
import { SharedModule } from '../shared/shared.module'


@NgModule({
  declarations: [ProfileComponent, ProfileIngoComponent, ProfileFoodComponent, ProfileActivityComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    SharedModule
  ],exports:[
    ProfileFoodComponent,
    ProfileActivityComponent
  ]
})
export class ProfileModule { }
