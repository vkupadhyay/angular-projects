import { TabType } from "../enum/tab-type";
export const SectionDetailsList = [
    {
        id: TabType.PROFILE,
        label: "Profile"
    },
    {
        id: TabType.FOOD,
        label: "Food"
    },
    {
        id: TabType.ACTIVITY,
        label: "Activity"
    }
] as const