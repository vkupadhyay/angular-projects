import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/util/services/auth-service/auth-service.service';
import { SectionDetailsList } from './constants/tab-details';
import { TabType } from './enum/tab-type';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  
  tabList: typeof SectionDetailsList = SectionDetailsList;
  sectionIds: typeof TabType = TabType;
  zIndex: number;
  isActive: boolean = true;

  constructor( public authServiceService: AuthServiceService ) { }

  ngOnInit(): void {
  }
  toggleTab(index: number){
    this.zIndex = index;
    this.isActive = false;
  }
}
