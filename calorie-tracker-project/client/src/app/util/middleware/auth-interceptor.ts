import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { AuthServiceService } from '../services/auth-service/auth-service.service';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthServiceService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
   const authToken = this.authService.getToken();
    req = req.clone({
      setHeaders: {
        token: 'Bearer '  + authToken,
      },
    });
    return next.handle(req);
  }
}