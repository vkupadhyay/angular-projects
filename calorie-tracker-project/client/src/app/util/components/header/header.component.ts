import { Component, OnInit } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import {AuthServiceService} from '../../../util/services/auth-service/auth-service.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor( public AuthService: AuthServiceService, private http: HttpClient) { }

  ngOnInit(): void {
  }

  logout(){
    this.AuthService.doLogout();
  }
}
