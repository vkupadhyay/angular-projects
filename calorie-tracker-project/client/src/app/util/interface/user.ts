export interface IUser {
    name: string;
    weight: string;
    height: string;
    email: string;
    password: string;
    gender: string;
    age: string;
    _id: string;
 
}

export interface IListOfUser {
    _id: string;
    name: string;
    weight: number;
    height: number;
    bmr: number;
    email: string;
    gender: string;
    age: string;
}

export interface ILoginUser {
    user_id: string;
    name: string;
    user_email: string;
    weight: string;
    height: string;
    gender: string;
    birth_date: number;
    BMR: number;
    age: string;
    iat: number;
    exp: string;
}
export interface IActivityData {
    user_id: string;
    activity_name: string;
    activity_date: string;
    activity_des: string;
    calorie_out: number;
    activity_duration: number;
    met_value: number;
}

export interface IActivity {
    activity_name: string;
    activity_date: string;
    activity_des: string;
    calorie_out: number;
    activity_duration: number;
    met_value: number;
}

export interface IActivityUser {
    name: string;
    activity: IActivity[];
}

export interface IDataApi {
    STATUS: string;
    MESSAGE: string;
    DATA: any;
}

export interface IFood {
    add_food_date: string;
    food_name: string;
    food_group: string;
    serving: string;
    calorie_in: number;
}

export interface IProfileInfo {
    name: string;
    weight: number;
    height: number;
    bmr: number;
    email: string;
    gender: string;
    age: string;
}