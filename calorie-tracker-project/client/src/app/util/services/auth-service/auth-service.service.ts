import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpErrorResponse,} from '@angular/common/http';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { ILoginUser } from '../../interface/user';
import SystemConstants from '../../constants/SystemConstants';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  dataObj:ILoginUser;
  decodedToken: { [key: string]: string };
  jwtToken: string = localStorage.getItem('auth_token');
  tokenExpired: boolean;
  constructor(public router: Router) { }
  isLoggedInForHeaderSubject = new BehaviorSubject<boolean>(true);
  isLoggedInForHeaderSubject$: Observable<boolean> =  this.isLoggedInForHeaderSubject.asObservable();

  getToken() {
    return localStorage.getItem('auth_token');
  }

  decodeToken() {
    if (this.jwtToken) {
      this.decodedToken = jwt_decode(this.jwtToken);
      return true;
    }
  }

  getDecodeToken() {
    return jwt_decode(this.jwtToken);
  }

  getUser() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.displayname : null;
  }


  getExpiryTime() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken.exp : null;
  }

  isTokenExpired(): boolean {
    const expiryTime: any = this.getExpiryTime();
    if (expiryTime) {
      this.tokenExpired = ((1000 * expiryTime) - (new Date()).getTime()) < 5000;
      return this.tokenExpired;
    } else {
      return false;
    }
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('auth_token');
    return authToken != null ? true : false;
  }

  

  public storeData(){
    let authToken = localStorage.getItem('auth_token');
    const dataObj = jwt_decode(authToken);
    return dataObj || null;
  }

  get loginUser(): ILoginUser {
    let authToken = localStorage.getItem('auth_token');
   
    if(authToken){
      this.dataObj = jwt_decode(authToken);
      const obj= {
        user_id:  this.dataObj.user_id,
        name:  this.dataObj.name,
        user_email: this.dataObj.user_email,
        height: this.dataObj.height,
        BMR: this.dataObj.BMR,
        weight: this.dataObj.weight,
        gender: this.dataObj.gender,
        birth_date: this.dataObj.birth_date,
        age: this.dataObj.age,
        iat: this.dataObj.iat,
        exp: this.dataObj.exp,
      }
      return obj
    }
  }

  doLogout() {
    let removeToken = localStorage.removeItem('auth_token');
    console.log('vimal');
  }


  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

  
}
