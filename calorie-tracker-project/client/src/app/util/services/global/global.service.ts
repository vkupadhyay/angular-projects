import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {baseUrlApi  } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IActivityData } from '../../interface/user';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) { }

  public getDate : string;

  getActivityListForChoose(): Observable<any> {
    let api = `${baseUrlApi}/activity-list`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  getFoodListForChoose(): Observable<any> {
    let api = `${baseUrlApi}/food-list`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  calorieOut(MetVal: number, weight:string, duration:number){
     return Number(MetVal) * Number(weight) * Number(duration);
  }

  addActivityData(data: IActivityData): Observable<object>{
    let api = `${baseUrlApi}/user-activity`;
    return this.http.post(api, data).pipe(catchError(this.handleError));
  }

  deleteActivityData(id:string, data:any): Observable<object>{
    let api = `${baseUrlApi}/user-activity-delete/${id}`;
    return this.http.put(api, data).pipe(catchError(this.handleError));
  }

  addFoodData(data: any): Observable<object>{
    let api = `${baseUrlApi}/user-food`;
    return this.http.post(api, data).pipe(catchError(this.handleError));
  }

  deleteFoodData(id:string, data:any): Observable<object>{
    let api = `${baseUrlApi}/user-food-delete/${id}`;
    return this.http.put(api, data).pipe(catchError(this.handleError));
  }

  

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any ;
        if (error.status === 0) {
          errorMessage = error.error 
          
        } else {
          errorMessage = error.error
        }
      return throwError(errorMessage);
  }
}
