export default class SystemConstants {
    public static readonly EMAIL_NOT_MATCH = "Email not match";
    public static readonly PASSWORD_NOT_MATCH = "Password not match";
    public static readonly SUCCESS = "SUCCESS";
    public static readonly FAILURE = "FAILURE";
  }
  