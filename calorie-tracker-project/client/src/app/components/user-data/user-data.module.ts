import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDataRoutingModule } from './user-data-routing.module';
import { UserDataComponent } from './user-data.component';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {ProfileModule} from '../../profile/profile.module';

@NgModule({
  declarations: [UserDataComponent],
  imports: [
    CommonModule,
    UserDataRoutingModule,
    SharedModule,
    FormsModule,Ng2SearchPipeModule,
    ProfileModule
  ]
})
export class UserDataModule { }
