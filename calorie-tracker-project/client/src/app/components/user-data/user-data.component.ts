import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AuthServiceService } from '../../util/services/auth-service/auth-service.service';
import { DataFormComponent } from 'src/app/shared/data-form/data-form.component';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit {
  sAttributes: any = {};
  bsModalRef?: BsModalRef;
  urlId:string = this.route.snapshot.paramMap.get('id');
  searchDate: Date;
  constructor(
    private modalService: BsModalService, 
    public authServiceService:AuthServiceService, 
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
  }
  
  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(DataFormComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  searchByDate(date: Date) {
    this.searchDate = date;
  }
 
}
