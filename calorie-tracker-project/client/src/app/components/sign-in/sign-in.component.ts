import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {UsersService} from '../../services/users/users.service'
import {AuthServiceService} from '../../util/services/auth-service/auth-service.service';
import { Router } from '@angular/router';
import SystemConstants from 'src/app/util/constants/SystemConstants';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  sAttributes: any = {};
  signInForm: FormGroup;
  submitted:boolean = false;
  serverErrorMessage: string;
  constructor(
    private formBuilder: FormBuilder, 
    private userService:UsersService, 
    private authService: AuthServiceService, 
    public router: Router) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(){
    this.signInForm = this.formBuilder.group({
      email: ['',[Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  submit(){
     const formData = this.signInForm.value;
     this.userService.signIn(formData).subscribe(res =>{
      if(res.MESSAGE === SystemConstants.EMAIL_NOT_MATCH){
        this.sAttributes.emailError = true;
        this.sAttributes.serverEmailErrorMessage = 'Email not match';
      }else if (res.MESSAGE === SystemConstants.PASSWORD_NOT_MATCH) {
        this.sAttributes.passwordError = true;
        this.sAttributes.serverPasswordErrorMessage = 'Password not match';
      }
      else{
        this.authService.isLoggedInForHeaderSubject.next(false);
        localStorage.setItem('auth_token', res.DATA);
        this.router.navigate(['/']);
      }
     }),(error: any) => {
      console.log(error);
     }
  }


}
