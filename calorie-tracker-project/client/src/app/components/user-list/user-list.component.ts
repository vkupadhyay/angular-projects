import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { IListOfUser } from 'src/app/util/interface/user';
import { UsersService } from '../../services/users/users.service'
import {AuthServiceService} from '../../util/services/auth-service/auth-service.service';
import { DataFormComponent } from '../../shared/data-form/data-form.component'
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  bsModalRef?: BsModalRef;
  userList:IListOfUser[];
  showMessage: boolean = false;
  messageTxt: string;
  constructor(
    private modalService: BsModalService, 
    public router: Router,  
    private usersService: UsersService, 
    public authServiceService:AuthServiceService
    ) { }

  ngOnInit(): void {
    this.getUserList();
  }

  getUserList(){
    this.usersService.userList().subscribe((data:any)=>{
      this.userList = data.DATA;
    }),(error: any) => {
      console.log(error);
     }
  }

  pageRouter(){
    this.router.navigate(['/sign-in']);
  }

  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(DataFormComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  viewButton(userId: string){
    if(this.authServiceService.isLoggedIn === true){
      if (this.authServiceService.loginUser.user_id === userId) {
        this.router.navigate(['user-data/'+ this.authServiceService.loginUser.user_id]);
      } else {
        this.showMessage = true;
        this.messageTxt = "You have not access to see data"
      }
    }else{
      this.router.navigate(['/sign-in']);
    }
  }

  openPop(userId: string){
    if(this.authServiceService.isLoggedIn === true){
      if (this.authServiceService.loginUser.user_id === userId) {
        this.openModalWithComponent()
      } else {
        this.showMessage = true;
        this.messageTxt = "You have not access to add data"
      }
    }else{
      this.router.navigate(['/sign-in']);
    }
  }

}
