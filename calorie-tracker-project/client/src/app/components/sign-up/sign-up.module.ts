import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SignUpRoutingModule } from './sign-up-routing.module';
import { SignUpComponent } from './sign-up.component';
import {SharedModule} from '../../shared/shared.module'


@NgModule({
  declarations: [SignUpComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SignUpRoutingModule,
    SharedModule,
  ],
  providers: [
    DatePipe
  ]
})
export class SignUpModule { }
