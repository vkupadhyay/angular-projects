import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {UsersService} from '../../services/users/users.service'
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  submitted:boolean = false;
  maxDate = new Date();
  bsValue = new Date();
  constructor(private formBuilder: FormBuilder, private userService:UsersService, public router: Router, public datepipe: DatePipe) { 
    this.maxDate.setDate(this.maxDate.getDate() + 0);
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  get f(): any {
    return this.signUpForm.controls;
  }

  initializeForm(){
      this.signUpForm = this.formBuilder.group({
        name: ['',[Validators.required]],
        email: ['',[Validators.required]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        weight: ['',[Validators.required]],
        height: ['',[Validators.required]],
        gender: ['',[Validators.required]],
        age: ['',[Validators.required]]
      })
  }
  
  submit(){
    const changeDateFromt = this.datepipe.transform(this.signUpForm.value.age, 'yyyy-MM-dd')
    const signUpObj = {
        name: this.signUpForm.value.name,
        email: this.signUpForm.value.email,
        password: this.signUpForm.value.password,
        weight: this.signUpForm.value.weight, 
        height: this.signUpForm.value.height,
        gender: this.signUpForm.value.gender,
        age: changeDateFromt,  
      }
    
    const formData = this.signUpForm.value.age;
    if (this.signUpForm.invalid) {
      this.submitted = true;
      return;
    }else{
        this.userService.signUp(signUpObj).subscribe(res =>{
          this.router.navigate(['/sign-in']);
        }),(error: any) => {
           console.log(error);
          }
    }
  }

}
