import { Component, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { GlobalService } from '../../util/services/global/global.service';
import { AuthServiceService } from '../../util/services/auth-service/auth-service.service';
import { DataFormComponent } from 'src/app/shared/data-form/data-form.component';
import { IActivity, IDataApi, IFood } from 'src/app/util/interface/user';
import { ActivityService } from '../../services/activity/activity.service';
import { FoodService } from '../../services/food/food.service';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  bsModalRef?: BsModalRef;
  todayDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  userActivityData: IActivity[];
  userFoodData: IFood[];
  activeMessageShow: boolean = true;
  foodMessageShow: boolean = true;
  netCalorie: number = 0;
  totalCalorieOut: number;
  totalCalorieIn: number;
  userBmr: number ;
  searchDate: any;
  user_id: string;
  userName: string;

  constructor(
    private modalService: BsModalService,
    public GlobalService: GlobalService,
    public authServiceService: AuthServiceService,
    private activityService: ActivityService,
    private foodService: FoodService
  ) { }

  ngOnInit(): void {
    this.setDateForSearch();
    this.getUserActivityData();
    this.getUserFoodData();
    this.calculateNetCalorie();
    this.storeLoginDetails();
  }

  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(DataFormComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  calculateNetCalorie() {
    
    if (this.userBmr && this.totalCalorieIn && this.totalCalorieOut) {
      let newBmr = this.userBmr;
      this.netCalorie = this.totalCalorieIn - newBmr - this.totalCalorieOut;
    } else {
      this.netCalorie = 0;
    }
  }

  storeLoginDetails(){
    if(this.authServiceService.isLoggedIn === true){
       this.userBmr = this.authServiceService.loginUser.BMR;
       this.user_id = this.authServiceService.loginUser.user_id;
       this.userName = this.authServiceService.loginUser.name;
    }else{
      this.userBmr = 0;
    }
    this.getUserActivityData();
    this.getUserFoodData();
  }

  searchByDate(date: Date) {
    this.searchDate = date;
    this.getUserActivityData();
    this.getUserFoodData();
  }

  setDateForSearch() {
    this.searchDate = this.GlobalService.getDate;
    if (this.searchDate == undefined) {
      this.searchDate = this.todayDate;
    } else {
      this.searchDate = this.searchDate;
    }
  }

  getUserActivityData() {
    const obj = {
      user_id: this.user_id,
      activity_date: this.searchDate
    }
    this.activityService.activitySearchByDate(obj).subscribe((res: IDataApi) => {
      try {
        if (res.DATA && !res.DATA.length) {
          this.activeMessageShow = false;
          this.userActivityData = [];
          this.totalCalorieOut = null;
        } else {
          this.userActivityData = res.DATA;
          this.activeMessageShow = true;
          let calorieOut = 0;
          this.userActivityData.forEach(function (data: any) { calorieOut += data["calorie_out"]; });
          this.totalCalorieOut = calorieOut;
          
        }
        this.calculateNetCalorie();
      } catch (error) {
        console.log(error);
      }
    });
  }

  getUserFoodData() {
    const obj = {
      user_id: this.user_id,
      add_food_date: this.searchDate
    }
    this.foodService.foodSearchByDate(obj).subscribe((res: IDataApi) => {
      try{
        if (res.DATA && !res.DATA.length) {
          this.foodMessageShow = false;
          this.userFoodData = [];
          this.totalCalorieIn = null;
          
        } else {
          this.userFoodData = res.DATA;
          this.foodMessageShow = true;
          let calorieIn = 0;
          this.userFoodData.forEach(function (data: any) { calorieIn += data["calorie_in"]; });
          this.totalCalorieIn = calorieIn;
        }
        this.calculateNetCalorie();
      }catch(error){
        console.log(error);
      }
    });
  }
}
