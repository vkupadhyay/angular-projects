import { Component, OnInit } from '@angular/core';
import {formatDate} from '@angular/common';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { FormControl, FormGroup } from '@angular/forms';
import { GlobalService } from '../../util/services/global/global.service';
import { AuthServiceService } from '../../util/services/auth-service.service';
import { DataFormComponent } from 'src/app/shared/data-form/data-form.component';
import { IActivity, IDataApi, IFood } from 'src/app/util/interface/user';
import { ActivityService } from '../../services/activity/activity.service';
import { FoodService } from '../../services/food/food.service';
import { TextDateSearchModel } from '../../shared/search-by-date/models/text-date-search-model';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  bsModalRef?: BsModalRef;
  
  todayDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  userActivityData: IActivity[];
  userFoodData: IFood[];
  activeMessageShow: boolean = true;
  foodMessageShow: boolean = true;
  netCalorie: any = 0;
  totalCalorieOut: number;
  totalCalorieIn: number;
  userBmr: number = this.authServiceService.loginUser.BMR;
  playerName: string;
  filterDate: any;
  dateSearchControl: FormControl;
  searchDate: any;
  getDate: any ;
  
  searchDetails: any;


  constructor(
    private modalService: BsModalService, 
    public GlobalService:GlobalService, 
    public authServiceService:AuthServiceService,
    private activityService:ActivityService, 
    private foodService:FoodService 
    ) { }

  ngOnInit(): void {
  }

   createForm() {
    this.dateSearchControl = new FormControl('');
   }

   searchByDate(date: Date) {
    this.searchDetails = date;
   }

  // registerSearchInputListeners() {
  //   this.dateSearchControl.valueChanges.subscribe((text) => {
  //     this.getDate = text;
  //     this.getUserActivityData();
  //     this.getUserFoodData();
  //   });
    
  // }
  getDateFun(){
    this.getDate =  localStorage.getItem('date_date')
  }

  openModalWithComponent() {
    this.bsModalRef = this.modalService.show(DataFormComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  calculateNetCalorie() {
    if (this.userBmr && this.totalCalorieIn && this.totalCalorieOut) {
      let newBmr = this.authServiceService.loginUser.BMR;
      this.netCalorie = this.totalCalorieIn - newBmr - this.totalCalorieOut;
    } else {
      this.netCalorie = "No value for calculate";
    }
   
  }

  getUserActivityData(){
    console.log(this.getDate, 'This is get data');
    this.activityService.activityListByUserId(this.authServiceService.loginUser.user_id).subscribe((res:IDataApi)=>{
      const filterData = res.DATA.filter((acti: { activity_date: string; }) => acti.activity_date === this.searchDetails);
      if(filterData.length <= 0){
        this.activeMessageShow = false;
        this.totalCalorieOut = 0;
      }else{
        this.userActivityData = filterData;
        let calorieOut = 0;
        this.userActivityData.forEach(function (data: any) { calorieOut += data["calorie_out"]; });
        console.log(this.userActivityData, 'Activity');
        this.totalCalorieOut = calorieOut;
        this.calculateNetCalorie();
      }
    });
  }

  getUserFoodData(){
    console.log(this.getDate, 'This is get data Food');
    this.foodService.foodListByUserId(this.authServiceService.loginUser.user_id).subscribe((res:IDataApi)=>{
      const filterData  = res.DATA.filter((food: { add_food_date: string; }) => food.add_food_date === this.getDate);
      if(filterData.length <= 0){
        this.foodMessageShow = false;
        this.totalCalorieIn = 0;
      }else{
        this.userFoodData = filterData;
        let calorieIn = 0;
        this.userFoodData.forEach(function (data: any) { calorieIn += data["calorie_in"]; });
        console.log(this.userFoodData, 'Food');
        this.totalCalorieIn = calorieIn;
        this.calculateNetCalorie()
      }
    })
  }

 
 

}
