import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-search-by-date',
  templateUrl: './search-by-date.component.html',
  styleUrls: ['./search-by-date.component.scss']
})
export class SearchByDateComponent implements OnInit {
  
  @Input() panelClass: string;
  @Input() getValue: string;
  @Output() dateSearchTriggered: EventEmitter<string | Date[]> = new EventEmitter<string | Date[]>();
  dateSearchControl: FormControl;
  maxDate = new Date();
  constructor(public datepipe: DatePipe) { 
    this.maxDate.setDate(this.maxDate.getDate() + 0);
  }

  ngOnInit() {
    this.createForm();
    this.registerSearchInputListeners();
  }

  createForm() {
    this.dateSearchControl = new FormControl('');
  }
  
  registerSearchInputListeners() {
    
    this.dateSearchControl.valueChanges.subscribe((text) => {
      const latest_date =this.datepipe.transform(text, 'yyyy-MM-dd');
      console.log(latest_date, 'vimal');
      this.dateSearchTriggered.emit(latest_date);
    });
  }

 
}
