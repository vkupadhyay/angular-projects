import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalService } from '../../util/services/global/global.service'
import {AuthServiceService} from '../../util/services/auth-service/auth-service.service';
import { ILoginUser } from 'src/app/util/interface/user';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.scss']
})
export class DataFormComponent implements OnInit {
  sAttributes: any = {};
  activityForm: FormGroup;
  foodForm:FormGroup;
  activityListfromApiToChoose: any [];
  foodListfromApiToChoose: any [];
  activityTime: number;
  activityMets: any;
  activityName:any;
  loginUserData: ILoginUser;
  activityCaloriesOut: any;
  calMetVal: number;
  listActivityDescription: any;
  isShow:boolean = true;
  submitted:boolean = false;
  foodTypeList:any [];
  getFoodNameList: any [];
  getFoodGroupName:any;
  getActivityDescriptionObj: any;
  closeBtnName?: string;

  constructor(
    private formBuilder: FormBuilder, 
    private globalService: GlobalService, 
    private authServiceService:AuthServiceService,
    public bsModalRef: BsModalRef,
    public datepipe: DatePipe
    ) {}

  ngOnInit(): void {
    this.getActivityListFromApi();
    this.getFoodListFromApi();
    this.initializeActivityForm();
    this.initializeFoodForm();
    this.getLoginUserData();
  }

  getLoginUserData(){
   this.loginUserData = this.authServiceService.loginUser;
  }

  get activityFormError(): any {
    return this.activityForm.controls;
  }

  get activityFoodError(): any {
    return this.activityForm.controls;
  }

  getActivityListFromApi(){
    this.globalService.getActivityListForChoose().subscribe((data:any)=>{
      this.activityListfromApiToChoose = data.DATA;
      const ids = this.activityListfromApiToChoose.map(o => o.activity_name);
      this.activityName = this.activityListfromApiToChoose.filter(({activity_name}, index) => !ids.includes(activity_name, index + 1));
      
    });
  };

  initializeActivityForm(){
    this.activityForm = this.formBuilder.group({
      activity_date: ['',[Validators.required]],
      activity_name: ['',[Validators.required]],
      activity_des: ['',[Validators.required]],
      met_value: ['',[Validators.required]],
      activity_duration: ['',[Validators.required]]
    });
  }

  getActivityDescription(){
    const activityName = this.activityForm.controls['activity_name'].value;
    this.getActivityDescriptionObj = this.activityListfromApiToChoose.filter((a:any)=> a.activity_name === activityName);
    
  }

  getActivityMetsFun(){
    const activityName = this.activityForm.controls['activity_des'].value;
    this.activityMets = this.activityListfromApiToChoose.find((a:any)=> a._id === activityName);
    console.log(this.activityMets)
    this.activityTime = this.activityForm.controls['activity_duration'].value;
    this.calMetVal = Number(this.activityTime) * Number(this.activityMets.METs);
    this.activityForm.controls['met_value'].setValue(this.calMetVal);
  }

  activityFormSubmit(){
    this.activityCaloriesOut = this.globalService.calorieOut(this.calMetVal, this.loginUserData.weight, this.activityTime);
    const changeDateFromt = this.datepipe.transform(this.activityForm.controls['activity_date'].value, 'yyyy-MM-dd')
    const newActivityObj = {
      "activity_date": changeDateFromt,
      "user_id": this.loginUserData.user_id,
      "activity_name": this.activityForm.controls['activity_name'].value,
      "activity_des": this.activityMets.activity_description,
      "calorie_out": this.activityCaloriesOut,
      "activity_duration": this.activityForm.controls['activity_duration'].value,
      "met_value": this.activityForm.controls['met_value'].value,
    }
    if (this.activityForm.invalid) {
      this.submitted = true;
      return;
    }else{
      this.globalService.addActivityData(newActivityObj).subscribe((res:any)=>{
        this.activityForm.reset();
        window.location.reload();
      }),(error: any) => {
        console.log(error);
       };
    }
    
  }

  showActivityForm(){ 
    this.isShow = true;
  }

  showFoodForm(){ 
    this.isShow = false;
  }

  getFoodListFromApi(){
    this.globalService.getFoodListForChoose().subscribe((data:any)=>{
      this.foodListfromApiToChoose = data.DATA;
      const ids = this.foodListfromApiToChoose.map(o => o.type)
      this.foodTypeList = this.foodListfromApiToChoose.filter(({type}, index) => !ids.includes(type, index + 1));
    });
  }

  initializeFoodForm(){
    this.foodForm = this.formBuilder.group({
      add_food_date: ['',[Validators.required]],
      food_type: ['',[Validators.required]],
      food_group: ['',[Validators.required]],
      food_name : ['',[Validators.required]],
      serving: ['',[Validators.required]],
      calorie_in: ['',[Validators.required]]
    });
  }

  getFoodGroup(){
    const findFoodGroupName = this.foodForm.controls['food_type'].value;
    const getFoodGroupName = this.foodListfromApiToChoose.filter((obj:any)=> obj.type === findFoodGroupName);
    const ids = getFoodGroupName.map(o => o.type)
    this.getFoodGroupName = getFoodGroupName.filter(({type}, index) => !ids.includes(type, index + 1));
  }

  getFoodName(){
    const findFoodName = this.foodForm.controls['food_group'].value;
    const getFoodGroupName = this.foodListfromApiToChoose.filter((obj:any)=> obj.group === findFoodName);
    this.getFoodNameList = getFoodGroupName;
  }
  
  getFoodCalories(){
    const getFoodServing = this.foodForm.controls['serving'].value;
    const foodNameForGetCalorie = this.foodForm.controls['food_name'].value;
    const findFoodData = this.getFoodNameList.find((obj:any)=> obj.name === foodNameForGetCalorie);
    const foodGetCalorie = findFoodData.Calories;
    let calorieIN = Number(getFoodServing) * Number( foodGetCalorie);
    this.foodForm.controls['calorie_in'].setValue(calorieIN);
  }

  foodFormSubmit(){
    const changeDateFromt = this.datepipe.transform(this.foodForm.controls.add_food_date.value, 'yyyy-MM-dd')
    const newFoodObj = {
      "add_food_date": changeDateFromt,
      "user_id": this.loginUserData.user_id,
      "food_type": this.foodForm.controls.food_type.value,
      "food_group": this.foodForm.controls.food_group.value,
      "food_name": this.foodForm.controls.food_name.value,
      "serving": this.foodForm.controls.serving.value,
      "calorie_in": this.foodForm.controls.calorie_in.value,
    }
    this.globalService.addFoodData(newFoodObj).subscribe((res:any)=>{
      this.foodForm.reset();
      window.location.reload();
    }),(error: any) => {
      console.log(error);
     };
  }

}
