
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { DataFormComponent } from './data-form/data-form.component';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SearchByDateComponent } from './search-by-date/search-by-date.component';
import { FoodListComponent } from './food-list/food-list.component';
import { ActivityListComponent } from './activity-list/activity-list.component';



@NgModule({
  declarations: [DataFormComponent, SearchByDateComponent, FoodListComponent, ActivityListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
     ModalModule.forRoot(),
     BsDatepickerModule.forRoot(),
  ],
  exports: [
    DataFormComponent,
    ModalModule,
    SearchByDateComponent,
    BsDatepickerModule
    
  ],
  providers: [
    DatePipe
  ]
})
export class SharedModule { }
