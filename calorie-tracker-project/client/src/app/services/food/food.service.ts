import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {baseUrlApi  } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FoodService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor( private http: HttpClient, public router: Router ) { }
  
  foodListByUserId(id:string): Observable<any> {
    let api = `${baseUrlApi}/user-food-data/${id}`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  foodSearchByDate(obj: object): Observable<any> {
    let api = `${baseUrlApi}/food-by-date`;
    return this.http.post(api, obj).pipe(catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    let errorMessage: any ;
        if (error.status === 0) {
          errorMessage = error.error 
          
        } else {
          errorMessage = error.error
        }
      return throwError(errorMessage);
  }
}
