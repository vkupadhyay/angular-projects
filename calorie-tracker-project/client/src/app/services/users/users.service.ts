import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {baseUrlApi  } from 'src/environments/environment';
import { Router } from '@angular/router';
import { IUser } from 'src/app/util/interface/user';

@Injectable({
  
  providedIn: 'root'
})
export class UsersService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, public router: Router) {

  }

  signUp(user: any): Observable<any> {
    let api = `${baseUrlApi}/sign-up`;
    return this.http.post(api, user).pipe(catchError(this.handleError));
  }

  signIn(user: IUser): Observable<any> {
    let api = `${baseUrlApi}/sign-in`;
    return this.http.post(api, user).pipe(catchError(this.handleError));
  }

  userList(): Observable<any> {
    let api = `${baseUrlApi}/user-list`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any ;
        if (error.status === 0) {
          errorMessage = error.error 
          
        } else {
          errorMessage = error.error
        }
      return throwError(errorMessage);
  }
}
