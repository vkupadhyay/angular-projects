import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from "./auth-guard/auth-guard.guard";

const routes: Routes = [
  { path: 'sign-up', loadChildren: () => import('./components/sign-up/sign-up.module').then(m => m.SignUpModule) },
  { path: 'sign-in', loadChildren: () => import('./components/sign-in/sign-in.module').then(m => m.SignInModule) },
  { path: '', loadChildren: () => import('./components/user-list/user-list.module').then(m => m.UserListModule) },
  { path: 'user-data/:id', canActivate: [AuthGuardGuard] , loadChildren: () => import('./components/user-data/user-data.module').then(m => m.UserDataModule) },
  { path: 'user-data/:id/user-details', canActivate: [AuthGuardGuard], loadChildren: () => import('./components/user-details/user-details.module').then(m => m.UserDetailsModule) },
  { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule) },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
