

export interface IUser {
    _id?: string;
    name: string;
    email: string;
    password?: string;
    weight: number;
    height: number;
    bmr:string | number;
    gender: string;
    birth_date: string;
    age?: number;
    is_deleted: boolean;
    modification_notes: number;
}