import * as mongoose from 'mongoose';
import { ModificationNote } from '../common/model';
import {IUser} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IUser>({
    name: {
        type: String,
        require: "Name is require"
    },
    weight: {
        type: Number,
        require: "Weight is require"
    },
    height: {
        type: Number,
        require: "Height is require"
    },
    bmr: {
        type: Number
    },
    email: {
        type: String,
        require: [true, 'Enter an email address.'],
        index: { unique: true, sparse: true },
        lowercase: true,
    },
    password:{
        type: String,
        required: [true, 'Name is required'],
        minlength:6,
    },
    gender: {
        type: String,
        require: "Gender is require"
    },
    birth_date: {
        type: String
    },
    age: {
        type: Number,
        require: "Age is require"
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    modification_notes: [ModificationNote]
});

export default mongoose.model('users', schema);