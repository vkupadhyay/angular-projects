
export function ageCalculation(dob:number) {
    let birthdaytoDate = new Date(dob);
    var ageDifMs = Date.now() - birthdaytoDate.getTime();
    // console.warn(ageDifMs);
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
};


export function bmrCalculation(age:number, Weight:number, Height:number, gender:string) {
    if(gender == 'male'){
      return  66.4730 + (13.7516 * Weight) + (5.0033 * Height)-(6.7550 * age) 
    }else if(gender == 'female'){
      return   655.0955 + (9.5634 * Weight) + (1.8496 * Height) - (4.6756 * age)  
   }else{
      return "cannot be Calculate"
    }   
}