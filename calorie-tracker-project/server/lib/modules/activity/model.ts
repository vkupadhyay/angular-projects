export interface IActivityData {
    _id?: string;
    user_id: string;
    activity_name: string;
    activity_date?: string;
    activity_des?: string;
    calorie_out: number;
    activity_duration?: number;
    activity_time: number;
    met_value:number;
    is_deleted: boolean;
    createdDate: Date;
    modification_notes: number;
}