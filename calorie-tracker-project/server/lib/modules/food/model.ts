export interface IFoodData {
    _id?: string;
    add_food_date?: string;
    user_id: string;
    food_group: string;
    food_name: string;
    serving: string;
    calorie_in: number;
    is_deleted: boolean;
    createdDate: Date;
}