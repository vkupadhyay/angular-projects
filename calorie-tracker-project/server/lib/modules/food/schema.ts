import * as mongoose from 'mongoose';
import { ModificationNote } from '../common/model';
import {IFoodData} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IFoodData>({
    add_food_date: {
        type: String,
        require: "Date is require"
    },
    user_id: {
        type: String,
        require: "Weight is require"
    },
    food_name: {
        type: String,
        require: "Name is require"
    },
    food_group: {
        type: String,
        require: "Height is require"
    },
    serving: {
        type: String
    },
    calorie_in: {
        type: Number,
        require: "Height is require"
    },
    
    is_deleted: {
        type: Boolean,
        default: false
    },
    createdDate: {
        type: Date,
        default: new Date()
    }
});

export default mongoose.model('foodData', schema);