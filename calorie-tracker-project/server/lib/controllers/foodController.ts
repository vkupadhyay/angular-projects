import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse, blank } from '../modules/common/service';
import foodDB from '../modules/food/schema';
import foods from '../modules/common/food_model';
import foodDatas from '../modules/food/schema'
export class FoodController {
    
    food = async (req:Request, res:Response)=>{
        try {
            const activityData = await foods.aggregate([
                { $project: { _id: 0, __v: 0, modification_notes: 0, create_date: 0, createdAt: 0, updatedAt: 0, is_deleted: 0 } }
            ]);
            successResponse('Activity list got successfully', activityData, res);
        } catch (error) {
            failureResponse('error', res, error);
        }
    };

    addFood = async (req:Request, res:Response)=>{
      try {
            if(req.body.add_food_date && req.body.user_id  && req.body.food_name  && req.body.food_group  && req.body.serving  && req.body.calorie_in){
               
                const foodData = new foodDB({
                    "add_food_date": req.body.add_food_date,
                    "user_id": req.body.user_id,
                    "food_group": req.body.food_group,
                    "food_name": req.body.food_name,
                    "serving": req.body.serving,
                    "calorie_in": req.body.calorie_in
                });
                await foodData.save().then((Date: any) => { 
                    successResponse('Food successfull Added', Date, res)
                });
            }else{
                failureResponse('Something is going wrong, Please check again', Date, res);
            }
        } catch (error) {
        failureResponse('Error', error, res);
      }
    }

    userFoodDelete = async (req:Request, res:Response)=>{
        const filter =  { _id: req.params.id };
        const update = { is_deleted: true };
        foodDatas.findOneAndUpdate(filter, update, (error: any, data: any) => {
           if(data == undefined){
               failureResponse('Something is going wrong, Please check again', blank, res);
           }else{
               successResponse('Activity staus successfull changed', data, res);
           }
          
        });
    }

    foodSearch = async (req:Request, res:Response)=>{
        try {
            const id = req.body.user_id;
            const date = req.body.add_food_date;
            let query=[{ user_id: id}, { add_food_date: date }, { is_deleted: false }];
            const data = await foodDatas.aggregate(
                [ {
                    $match: { $and: query }
                  },
                  { 
                    $project: {  _id:0, user_id:0, is_deleted:0, createdDate:0, __v:0   } 
                  }
                ]
            );
            successResponse('Successfully Get User Food by Search with Date', data, res);
        } catch (error) {
            insufficientParameters(error)
        }
    };

}