import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import activityas from '../modules/activity/schema';
import activitys from '../modules/common/activity_model';
import activityData from '../modules/activity/schema'
export class ActivityController {
    
    activity = async (req:Request, res:Response)=>{
        try {
            const activityData = await activitys.aggregate([
                { $project: { __v: 0, modification_notes: 0, create_date: 0, createdAt: 0, updatedAt: 0, is_deleted: 0 } }
            ]);
            successResponse('Activity list got successfully', activityData, res);
        } catch (error) {
            failureResponse('error', res, error);
        }
    };

    userActivity = async (req:Request, res:Response)=>{
        try {
            if(req.body.user_id && req.body.activity_name && req.body.activity_date && req.body.activity_des && req.body.calorie_out && req.body.activity_duration && req.body.met_value){
               
                const activityData = new activityas({
                    "user_id" : req.body.user_id,
                    "activity_name" : req.body.activity_name,
                    "activity_date" : req.body.activity_date,
                    "activity_des" : req.body.activity_des,
                    "calorie_out" : req.body.calorie_out,
                    "activity_duration" : req.body.activity_duration,
                    "met_value" : req.body.met_value
                });
                await activityData.save().then((Date: any) => { 
                    successResponse('Activity successfull Added', Date, res)
                });
            }else{
                failureResponse('Something is going wrong, Please check again', Date, res);
            } 
        }catch (error) {
            failureResponse('Error', error, res);
        }
    }

    userActivityDelete = async (req:Request, res:Response)=>{
         const filter =  { _id: req.params.id };
         const update = { is_deleted: true };
         activityData.findOneAndUpdate(filter, update, (error: any, data: any) => {
            let blank = '';
            if(data == undefined){
                failureResponse('Something is going wrong, Please check again', blank, res);
            }else{
                successResponse('Activity staus successfull changed', data, res);
            }
           
         });
    }
    activitySearch = async (req:Request, res:Response)=>{
        try {
            const id = req.body.user_id;
            const date = req.body.activity_date;
            console.log(id, date, 'new');
            let query=[{ user_id: id}, { activity_date: date }, { is_deleted: false }];
            const data = await activityData.aggregate(
                [ {
                    $match: { $and: query }
                  },
                  { 
                    $project: {  _id:0, user_id:0, is_deleted:0, createdDate:0, __v:0   } 
                  }
                ]
            );
            successResponse('Successfully Get User Activity by Search with Date', data, res);
        } catch (error) {
            insufficientParameters(error)
        }
    };

}