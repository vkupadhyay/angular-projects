import { Request, Response } from 'express';
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';
import { bmrCalculation, ageCalculation } from '../modules/common/calculation-service';
const mongoose = require("mongoose");
import {IUser} from '../modules/users/model'
import activityData from '../modules/activity/schema'
import foodData from '../modules/food/schema'
const TOKEN_KEY = "@QEGTUI";
import users from '../modules/users/schema';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
const ObjectId = mongoose.Types.ObjectId;
export class UserController {
    
    //----------------------------For User List----------------------------

        userList = async (req: Request, res: Response) => {
            
            try {
                const studentData = await users.aggregate([
                    { $project: {  __v: 0, password: 0, modification_notes: 0, create_date: 0, createdAt: 0, updatedAt: 0, is_deleted: 0 } }
                ]);
                successResponse('Student list got successfully', studentData, res);
            } catch (error) {
                failureResponse('error', res, error);
            }
        };

//----------------------------For User Sign up----------------------------
   
        userSignUp = async(req: Request, res: Response) =>{
        try {
            console.log(req.body.age);
                const age = ageCalculation(req.body.age);
                const bmr = bmrCalculation(age,  req.body.weight, req.body.height, req.body.gender);
                if( req.body.name && req.body.weight && req.body.height && req.body.gender && req.body.age && req.body.email && req.body.password){
                    const salt = await bcrypt.genSalt(10);
                    const newPassword = await bcrypt.hash(req.body.password, salt);
                    const user = new users({
                        "name" : req.body.name,
                        "weight" : req.body.weight,
                        "height" : req.body.height,
                        "password" : newPassword,
                        "bmr" : bmr,
                        "email" : req.body.email,
                        "birth_date": req.body.age,
                        "gender" : "male",
                        "age" : age,
                    });
                    await user.save().then((Date: any) => { 
                    successResponse('create user successfull', Date, res)
                    });
                 }else{
                    failureResponse('Something is going wrong, Please check again', Date, res);
                }
        } catch (error) {
            failureResponse('Error', error, res);
        }
        };

   //----------------------------For User Sign in----------------------------  
    
        userSignIn =async (req: Request, res: Response) => {
                try {
                    const {email, password} = req.body; 
                    const user = await users.findOne({ email });
                    if(!user){
                        failureResponse('Email not match', Date, res);
                    }else{
                       bcrypt.compare(password, user.password, function (error, result) {
                           if (!result) {
                               failureResponse('Password not match', error, res);
                           } else {
                               const auth_token = jwt.sign(
                                   {
                                       user_id: user._id,
                                       name: user.name,
                                       user_email: user.email,
                                       weight: user.weight,
                                       height: user.height,
                                       BMR: user.bmr,
                                       gender: user.gender,
                                       birth_date: user.birth_date,
                                       age: user.age
                                   },
                                   TOKEN_KEY,
                                   {
                                       expiresIn: "12h",
                                   }
                               );
                               successResponse('Sign In successfully', auth_token, res);
                           }
                       });
                    }
                } catch (error) {
                    insufficientParameters(error)
                }
        }
    //----------------------------For User Sign in----------------------------  

    userAllActivityData = async (req: Request, res: Response)=>{
        try {
            const id = req.params.id;
            let query=[{ user_id: id}, { is_deleted: false }];
            const data = await activityData.aggregate(
                [ {
                    $match: { $and: query }
                  },
                  { 
                    $project: {  activity_name:1, activity_date:1, activity_des:1, calorie_out:1, activity_duration:1,  met_value:1, user_id:1   } 
                  }
                ]
            );
            successResponse('Get user activity list successfully', data, res);
        } catch (error) {
            insufficientParameters(error)
        }
    }


    userAllFoodData = async (req: Request, res: Response)=>{
        try {
            const id = req.params.id;
            let query=[{ user_id: id}, { is_deleted: false }];
            const data = await foodData.aggregate(
                [ {
                    $match: { $and: query }
                  },
                  { 
                    $project: {   food_name:1, food_group:1, serving:1, calorie_in:1, add_food_date:1, user_id:1 } 
                  }
                ]
            );
            successResponse('Get user food list successfully', data, res);
        } catch (error) {
            insufficientParameters(error)
        }
    }


    userProfile = async (req: Request, res: Response)=>{
        try {
            const id = ObjectId(req.params.id);
            let query=[{ _id: id}, { is_deleted: false }];
            const data = await users.aggregate(
                [ {
                    $match: { $and: query }
                  },
                  { 
                    $project: { _id:0, password:0, is_deleted:0, modification_notes:0, __v:0 } 
                  }
                ]
            );
            successResponse('User profile data got successfully', data, res);
        } catch (error) {
            insufficientParameters(error)
        }
    }
}