import { Application, Request, Response } from 'express';
import { UserController } from '../controllers/userController';
import { verifyToken } from '../middleware/verifyToken';

export class UserRoutes {

   private user_controller: UserController = new UserController();

    public route(app: Application) {
       
        // ----------------------Api for user get list-------------------------------
            app.get('/api/user-list', (req: Request, res: Response) => {
                this.user_controller.userList(req, res);
            });
       
        
        // ----------------------Api for Sign up-------------------------------
            app.post('/api/sign-up', (req: Request, res: Response) =>{
               
                this.user_controller.userSignUp(req, res);
            });


        //--------------------------Api for Sign in-------------------------------------------
             app.post('/api/sign-in', (req: Request, res: Response)=>{
                this.user_controller.userSignIn(req, res);
             });

        //--------------------------Api for Get Data By ID and date-------------------------------------------
             app.get('/api/user-activity-data/:id', verifyToken, (req: Request, res: Response)=>{
               this.user_controller.userAllActivityData(req, res)
             });

        //--------------------------Api for Get Data By ID and date-------------------------------------------
             app.get('/api/user-food-data/:id', verifyToken, (req: Request, res: Response)=>{
                this.user_controller.userAllFoodData(req, res)
              });

        //--------------------------Api for Get Data By ID and date-------------------------------------------
        app.get('/api/profile/:id', verifyToken, (req: Request, res: Response)=>{
            this.user_controller.userProfile(req, res)
        });

    
    }
}