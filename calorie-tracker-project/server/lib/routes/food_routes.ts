import { Application, Request, Response } from 'express';
import { verifyToken } from '../middleware/verifyToken';
import { FoodController } from '../controllers/foodController';

export class FoodRoutes {
    private food_controller: FoodController = new FoodController();

    public route(app: Application) {
       
        // ----------------------Api for Get food list-------------------------------
            app.get('/api/food-list', (req: Request, res: Response) => {
                this.food_controller.food(req, res);
            });

            app.post('/api/user-food', (req: Request, res: Response) => {
                this.food_controller.addFood(req, res)
            });

             // ----------------------Api for activity delete-------------------------------
             app.put('/api/user-food-delete/:id', verifyToken, (req: Request, res: Response) => {
                this.food_controller.userFoodDelete(req, res);
             });

            // ----------------------Api for Search-------------------------------
            app.post('/api/food-by-date', (req: Request, res: Response) => {
                this.food_controller.foodSearch(req, res);
            });
       
   
    
    }
}