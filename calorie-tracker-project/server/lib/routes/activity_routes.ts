import { Application, Request, Response } from 'express';
import { verifyToken } from '../middleware/verifyToken';
import { ActivityController } from '../controllers/activityController';

export class ActivityRoutes {
    private activity_controller: ActivityController = new ActivityController();
    public route(app: Application) {
       
        // ----------------------Api for user get list-------------------------------
            app.get('/api/activity-list', (req: Request, res: Response) => {
                this.activity_controller.activity(req, res);
            });
       
        // ----------------------Api for user get list-------------------------------
            app.post('/api/user-activity', (req: Request, res: Response) => {
                this.activity_controller.userActivity(req, res);
            });
        // ----------------------Api for activity delete-------------------------------
            app.put('/api/user-activity-delete/:id', verifyToken, (req: Request, res: Response) => {
                this.activity_controller.userActivityDelete(req, res);
            });
         // ----------------------Api for Search-------------------------------
         app.post('/api/activity-by-date', (req: Request, res: Response) => {
            this.activity_controller.activitySearch(req, res);
        });
    }
}