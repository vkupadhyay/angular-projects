"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const helmet_1 = require("helmet");
const environment_1 = require("../environment");
const user_routes_1 = require("../routes/user_routes");
const activity_routes_1 = require("../routes/activity_routes");
const food_routes_1 = require("../routes/food_routes");
const common_routes_1 = require("../routes/common_routes");
class App {
    constructor() {
        this.mongoUrl = 'mongodb://localhost/' + environment_1.default.getDBName();
        this.user_routes = new user_routes_1.UserRoutes();
        this.activity_routes = new activity_routes_1.ActivityRoutes();
        this.food_routes = new food_routes_1.FoodRoutes();
        this.common_routes = new common_routes_1.CommonRoutes();
        this.app = express();
        this.app.use(cors());
        this.app.use((0, helmet_1.default)());
        this.config();
        this.mongoSetup();
        this.user_routes.route(this.app);
        this.activity_routes.route(this.app);
        this.food_routes.route(this.app);
        this.common_routes.route(this.app);
    }
    config() {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use((req, res, next) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Headers', 'Origin, x-access-token, token, Content-Type, Accept, Authorization');
            res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
            res.setHeader('Content-Type', 'text/plain');
            if (req.method == 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
                return res.status(200).json({});
            }
            next();
        });
    }
    mongoSetup() {
        mongoose.connect(this.mongoUrl, {});
    }
}
exports.default = new App().app;
