"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivityRoutes = void 0;
const activityController_1 = require("../controllers/activityController");
class ActivityRoutes {
    constructor() {
        this.activity_controller = new activityController_1.ActivityController();
    }
    route(app) {
        // ----------------------Api for user get list-------------------------------
        app.get('/api/activity-list', (req, res) => {
            this.activity_controller.activity(req, res);
        });
        // ----------------------Api for user get list-------------------------------
        app.post('/api/user-activity', (req, res) => {
            this.activity_controller.userActivity(req, res);
        });
        // ----------------------Api for activity delete-------------------------------
        app.put('/api/user-activity-delete/:id', (req, res) => {
            this.activity_controller.userActivityDelete(req, res);
        });
        // ----------------------Api for Search-------------------------------
        app.post('/api/activity-by-date', (req, res) => {
            this.activity_controller.activitySearch(req, res);
        });
    }
}
exports.ActivityRoutes = ActivityRoutes;
