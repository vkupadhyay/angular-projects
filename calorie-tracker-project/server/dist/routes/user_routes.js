"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoutes = void 0;
const userController_1 = require("../controllers/userController");
class UserRoutes {
    constructor() {
        this.user_controller = new userController_1.UserController();
    }
    route(app) {
        // ----------------------Api for user get list-------------------------------
        app.get('/api/user-list', (req, res) => {
            this.user_controller.userList(req, res);
        });
        // ----------------------Api for Sign up-------------------------------
        app.post('/api/sign-up', (req, res) => {
            this.user_controller.userSignUp(req, res);
        });
        //--------------------------Api for Sign in-------------------------------------------
        app.post('/api/sign-in', (req, res) => {
            this.user_controller.userSignIn(req, res);
        });
        //--------------------------Api for Get Data By ID and date-------------------------------------------
        app.get('/api/user-activity-data/:id', (req, res) => {
            this.user_controller.userAllActivityData(req, res);
        });
        //--------------------------Api for Get Data By ID and date-------------------------------------------
        app.get('/api/user-food-data/:id', (req, res) => {
            this.user_controller.userAllFoodData(req, res);
        });
        //--------------------------Api for Get Data By ID and date-------------------------------------------
        app.get('/api/profile/:id', (req, res) => {
            this.user_controller.userProfile(req, res);
        });
    }
}
exports.UserRoutes = UserRoutes;
