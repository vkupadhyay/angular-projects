"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodRoutes = void 0;
const foodController_1 = require("../controllers/foodController");
class FoodRoutes {
    constructor() {
        this.food_controller = new foodController_1.FoodController();
    }
    route(app) {
        // ----------------------Api for Get food list-------------------------------
        app.get('/api/food-list', (req, res) => {
            this.food_controller.food(req, res);
        });
        app.post('/api/user-food', (req, res) => {
            this.food_controller.addFood(req, res);
        });
        // ----------------------Api for activity delete-------------------------------
        app.put('/api/user-food-delete/:id', (req, res) => {
            this.food_controller.userFoodDelete(req, res);
        });
        // ----------------------Api for Search-------------------------------
        app.post('/api/food-by-date', (req, res) => {
            this.food_controller.foodSearch(req, res);
        });
    }
}
exports.FoodRoutes = FoodRoutes;
