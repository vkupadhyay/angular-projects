"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const service_1 = require("../modules/common/service");
const calculation_service_1 = require("../modules/common/calculation-service");
const mongoose = require("mongoose");
const schema_1 = require("../modules/activity/schema");
const schema_2 = require("../modules/food/schema");
const TOKEN_KEY = "@QEGTUI";
const schema_3 = require("../modules/users/schema");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const ObjectId = mongoose.Types.ObjectId;
class UserController {
    constructor() {
        //----------------------------For User List----------------------------
        this.userList = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const studentData = yield schema_3.default.aggregate([
                    { $project: { __v: 0, password: 0, modification_notes: 0, create_date: 0, createdAt: 0, updatedAt: 0, is_deleted: 0 } }
                ]);
                (0, service_1.successResponse)('Student list got successfully', studentData, res);
            }
            catch (error) {
                (0, service_1.failureResponse)('error', res, error);
            }
        });
        //----------------------------For User Sign up----------------------------
        this.userSignUp = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const age = (0, calculation_service_1.ageCalculation)(req.body.age);
                const bmr = (0, calculation_service_1.bmrCalculation)(age, req.body.weight, req.body.height, req.body.gender);
                if (req.body.name && req.body.weight && req.body.height && req.body.gender && req.body.age && req.body.email && req.body.password) {
                    const salt = yield bcrypt.genSalt(10);
                    const newPassword = yield bcrypt.hash(req.body.password, salt);
                    const user = new schema_3.default({
                        "name": req.body.name,
                        "weight": req.body.weight,
                        "height": req.body.height,
                        "password": newPassword,
                        "bmr": bmr,
                        "email": req.body.email,
                        "gender": "male",
                        "age": age,
                    });
                    yield user.save().then((Date) => {
                        (0, service_1.successResponse)('create user successfull', Date, res);
                    });
                }
                else {
                    (0, service_1.failureResponse)('Something is going wrong, Please check again', Date, res);
                }
            }
            catch (error) {
                (0, service_1.failureResponse)('Error', error, res);
            }
        });
        //----------------------------For User Sign in----------------------------  
        this.userSignIn = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { email, password } = req.body;
                const user = yield schema_3.default.findOne({ email });
                if (!user) {
                    (0, service_1.failureResponse)('Email not match', Date, res);
                }
                else {
                    bcrypt.compare(password, user.password, function (error, result) {
                        if (!result) {
                            (0, service_1.failureResponse)('Password not match', error, res);
                        }
                        else {
                            const auth_token = jwt.sign({
                                user_id: user._id,
                                name: user.name,
                                user_email: user.email,
                                weight: user.weight,
                                height: user.height,
                                BMR: user.bmr,
                                gender: user.gender,
                                age: user.age
                            }, TOKEN_KEY, {
                                expiresIn: "12h",
                            });
                            (0, service_1.successResponse)('Sign In successfully', auth_token, res);
                        }
                    });
                }
            }
            catch (error) {
                (0, service_1.insufficientParameters)(error);
            }
        });
        //----------------------------For User Sign in----------------------------  
        this.userAllActivityData = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                let query = [{ user_id: id }, { is_deleted: false }];
                const data = yield schema_1.default.aggregate([{
                        $match: { $and: query }
                    },
                    {
                        $project: { activity_name: 1, activity_date: 1, activity_des: 1, calorie_out: 1, activity_duration: 1, met_value: 1, user_id: 1 }
                    }
                ]);
                (0, service_1.successResponse)('Get user activity list successfully', data, res);
            }
            catch (error) {
                (0, service_1.insufficientParameters)(error);
            }
        });
        this.userAllFoodData = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                let query = [{ user_id: id }, { is_deleted: false }];
                const data = yield schema_2.default.aggregate([{
                        $match: { $and: query }
                    },
                    {
                        $project: { food_name: 1, food_group: 1, serving: 1, calorie_in: 1, add_food_date: 1, user_id: 1 }
                    }
                ]);
                (0, service_1.successResponse)('Get user food list successfully', data, res);
            }
            catch (error) {
                (0, service_1.insufficientParameters)(error);
            }
        });
        this.userProfile = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const id = ObjectId(req.params.id);
                let query = [{ _id: id }, { is_deleted: false }];
                const data = yield schema_3.default.aggregate([{
                        $match: { $and: query }
                    },
                    {
                        $project: { _id: 0, password: 0, is_deleted: 0, modification_notes: 0, __v: 0 }
                    }
                ]);
                (0, service_1.successResponse)('User profile data got successfully', data, res);
            }
            catch (error) {
                (0, service_1.insufficientParameters)(error);
            }
        });
    }
}
exports.UserController = UserController;
