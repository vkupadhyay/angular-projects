"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivityController = void 0;
const service_1 = require("../modules/common/service");
const schema_1 = require("../modules/activity/schema");
const activity_model_1 = require("../modules/common/activity_model");
const schema_2 = require("../modules/activity/schema");
class ActivityController {
    constructor() {
        this.activity = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const activityData = yield activity_model_1.default.aggregate([
                    { $project: { __v: 0, modification_notes: 0, create_date: 0, createdAt: 0, updatedAt: 0, is_deleted: 0 } }
                ]);
                (0, service_1.successResponse)('Activity list got successfully', activityData, res);
            }
            catch (error) {
                (0, service_1.failureResponse)('error', res, error);
            }
        });
        this.userActivity = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.body.user_id && req.body.activity_name && req.body.activity_date && req.body.activity_des && req.body.calorie_out && req.body.activity_duration && req.body.met_value) {
                    const activityData = new schema_1.default({
                        "user_id": req.body.user_id,
                        "activity_name": req.body.activity_name,
                        "activity_date": req.body.activity_date,
                        "activity_des": req.body.activity_des,
                        "calorie_out": req.body.calorie_out,
                        "activity_duration": req.body.activity_duration,
                        "met_value": req.body.met_value
                    });
                    yield activityData.save().then((Date) => {
                        (0, service_1.successResponse)('Activity successfull Added', Date, res);
                    });
                }
                else {
                    (0, service_1.failureResponse)('Something is going wrong, Please check again', Date, res);
                }
            }
            catch (error) {
                (0, service_1.failureResponse)('Error', error, res);
            }
        });
        this.userActivityDelete = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const filter = { _id: req.params.id };
            const update = { is_deleted: true };
            schema_2.default.findOneAndUpdate(filter, update, (error, data) => {
                let blank = '';
                if (data == undefined) {
                    (0, service_1.failureResponse)('Something is going wrong, Please check again', blank, res);
                }
                else {
                    (0, service_1.successResponse)('Activity staus successfull changed', data, res);
                }
            });
        });
        this.activitySearch = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.body.user_id;
                const date = req.body.activity_date;
                console.log(id, date, 'new');
                let query = [{ user_id: id }, { activity_date: date }, { is_deleted: false }];
                const data = yield schema_2.default.aggregate([{
                        $match: { $and: query }
                    },
                    {
                        $project: { _id: 0, user_id: 0, is_deleted: 0, createdDate: 0, __v: 0 }
                    }
                ]);
                (0, service_1.successResponse)('Successfully Get User Activity by Search with Date', data, res);
            }
            catch (error) {
                (0, service_1.insufficientParameters)(error);
            }
        });
    }
}
exports.ActivityController = ActivityController;
