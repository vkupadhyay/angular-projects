"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodController = void 0;
const service_1 = require("../modules/common/service");
const schema_1 = require("../modules/food/schema");
const food_model_1 = require("../modules/common/food_model");
const schema_2 = require("../modules/food/schema");
class FoodController {
    constructor() {
        this.food = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const activityData = yield food_model_1.default.aggregate([
                    { $project: { _id: 0, __v: 0, modification_notes: 0, create_date: 0, createdAt: 0, updatedAt: 0, is_deleted: 0 } }
                ]);
                (0, service_1.successResponse)('Activity list got successfully', activityData, res);
            }
            catch (error) {
                (0, service_1.failureResponse)('error', res, error);
            }
        });
        this.addFood = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.body.add_food_date && req.body.user_id && req.body.food_name && req.body.food_group && req.body.serving && req.body.calorie_in) {
                    const foodData = new schema_1.default({
                        "add_food_date": req.body.add_food_date,
                        "user_id": req.body.user_id,
                        "food_group": req.body.food_group,
                        "food_name": req.body.food_name,
                        "serving": req.body.serving,
                        "calorie_in": req.body.calorie_in
                    });
                    yield foodData.save().then((Date) => {
                        (0, service_1.successResponse)('Food successfull Added', Date, res);
                    });
                }
                else {
                    (0, service_1.failureResponse)('Something is going wrong, Please check again', Date, res);
                }
            }
            catch (error) {
                (0, service_1.failureResponse)('Error', error, res);
            }
        });
        this.userFoodDelete = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const filter = { _id: req.params.id };
            const update = { is_deleted: true };
            schema_2.default.findOneAndUpdate(filter, update, (error, data) => {
                if (data == undefined) {
                    (0, service_1.failureResponse)('Something is going wrong, Please check again', service_1.blank, res);
                }
                else {
                    (0, service_1.successResponse)('Activity staus successfull changed', data, res);
                }
            });
        });
        this.foodSearch = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.body.user_id;
                const date = req.body.add_food_date;
                let query = [{ user_id: id }, { add_food_date: date }, { is_deleted: false }];
                const data = yield schema_2.default.aggregate([{
                        $match: { $and: query }
                    },
                    {
                        $project: { _id: 0, user_id: 0, is_deleted: 0, createdDate: 0, __v: 0 }
                    }
                ]);
                (0, service_1.successResponse)('Successfully Get User Food by Search with Date', data, res);
            }
            catch (error) {
                (0, service_1.insufficientParameters)(error);
            }
        });
    }
}
exports.FoodController = FoodController;
