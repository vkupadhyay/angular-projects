"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
    user_id: {
        type: String,
        require: "ID is require"
    },
    activity_name: {
        type: String,
        require: "Activity name is require"
    },
    activity_date: {
        type: String,
        require: "Activity name is require"
    },
    activity_des: {
        type: String,
        require: "Activity description is require"
    },
    calorie_out: {
        type: Number,
        require: "Calorie Out is require"
    },
    activity_duration: {
        type: Number,
        require: "Time is require"
    },
    met_value: {
        type: Number,
        require: "METs is require"
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    createdDate: {
        type: Date,
        default: new Date()
    }
});
exports.default = mongoose.model('activityData', schema);
