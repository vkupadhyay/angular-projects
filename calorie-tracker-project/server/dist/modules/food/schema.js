"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
    add_food_date: {
        type: String,
        require: "Date is require"
    },
    user_id: {
        type: String,
        require: "Weight is require"
    },
    food_name: {
        type: String,
        require: "Name is require"
    },
    food_group: {
        type: String,
        require: "Height is require"
    },
    serving: {
        type: String
    },
    calorie_in: {
        type: Number,
        require: "Height is require"
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    createdDate: {
        type: Date,
        default: new Date()
    }
});
exports.default = mongoose.model('foodData', schema);
