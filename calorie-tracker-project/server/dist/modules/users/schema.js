"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const model_1 = require("../common/model");
const Schema = mongoose.Schema;
const schema = new Schema({
    name: {
        type: String,
        require: "Name is require"
    },
    weight: {
        type: Number,
        require: "Weight is require"
    },
    height: {
        type: Number,
        require: "Height is require"
    },
    bmr: {
        type: Number
    },
    email: {
        type: String,
        require: [true, 'Enter an email address.'],
        index: { unique: true, sparse: true },
        lowercase: true,
    },
    password: {
        type: String,
        required: [true, 'Name is required'],
        minlength: 6,
    },
    gender: {
        type: String,
        require: "Gender is require"
    },
    age: {
        type: Number,
        require: "Age is require"
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    modification_notes: [model_1.ModificationNote]
});
exports.default = mongoose.model('users', schema);
