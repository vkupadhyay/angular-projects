import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from 'mongoose';
import helmet from "helmet";
import environment from "../environment";
import { UserRoutes } from "../routes/user_routes";
import { BlogRoutes } from "../routes/blog_routes";
import { CommentRoutes } from "../routes/comment_routes";
import { UserImageRoutes } from "../routes/image_routes";
import { CommonRoutes } from "../routes/common_routes";

class App {

   public app: express.Application;
   public mongoUrl: string = 'mongodb://localhost/' + environment.getDBName();

   private user_routes: UserRoutes = new UserRoutes();
   private blog_routes: BlogRoutes = new BlogRoutes();
   private comment_routes: CommentRoutes = new CommentRoutes();
   private image_routes: UserImageRoutes = new UserImageRoutes();
   private common_routes: CommonRoutes = new CommonRoutes();

   constructor() {
      this.app = express();
      this.config();
      this.app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));
      this.app.use('/upload', express.static(process.cwd() + '/upload'))
      this.mongoSetup();
      this.user_routes.route(this.app);
      this.blog_routes.route(this.app);
      this.comment_routes.route(this.app);
      this.image_routes.route(this.app);
      this.common_routes.route(this.app);
   }

   private config(): void {
      // support application/json type post data
      this.app.use(bodyParser.json());
      //support application/x-www-form-urlencoded post data
      this.app.use(bodyParser.urlencoded({ extended: false }));
      this.app.use((req, res, next) => {
         res.setHeader('Access-Control-Allow-Origin', '*');
         res.setHeader('Access-Control-Allow-Headers', 'Origin, x-access-token, token, Content-Type, Accept, Authorization');
         res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
         res.setHeader('Content-Type', 'text/plain');
         if (req.method == 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
            return res.status(200).json({});
         }

         next();
      });
   }

   private mongoSetup(): void {
      mongoose.set('strictQuery', true);
      mongoose.connect(this.mongoUrl, {});
   }

}
export default new App().app;