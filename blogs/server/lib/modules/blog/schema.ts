import * as mongoose from 'mongoose';
import { ModificationNote  } from '../common/model';
import {ICreateBlog} from './model';
import { isEmail } from 'validator';
const Schema = mongoose.Schema;

const schema = new Schema<ICreateBlog>({
    name:{ type: String},
    email:<any> { type: String,
        required: 'Email address is required',
        match: /.+@.+\..+/
    },
    phone_number: {type: Number},
    image: [],
    description:{ type: String},
    author:{ type: String},
    status: {
        type: String,
        default: 'inactive'
    }
}, { timestamps: true });

export default mongoose.model('blog', schema);