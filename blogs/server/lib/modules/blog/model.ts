

export interface ICreateBlog {
    _id?: string;
     name: string;
    email: string;
    phone_number: number;
    description: string;
    image: [];
    author: string;
    status?: string;
}