

export interface ICreateComment {
    _id?: string;
    name: string;
    email: string;
    phone_number: number;
    description: string;
    status?: string;
    blog_id?: string;
}