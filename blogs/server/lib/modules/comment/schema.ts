import * as mongoose from 'mongoose';
import {ICreateComment} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<ICreateComment>({
    name:{ type: String},
    blog_id:{ type: String},
    email:<any> { type: String,
        required: 'Email address is required',
        match: /.+@.+\..+/
    },
    phone_number: {type: Number},
    description:{ type: String},
    status: {
        type: String,
        default: 'inactive'
    }
}, { timestamps: true });

export default mongoose.model('comment', schema);