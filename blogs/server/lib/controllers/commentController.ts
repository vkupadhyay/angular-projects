import { Request, Response } from "express";
import {
  insufficientParameters,
  mongoError,
  successResponse,
  failureResponse,
} from "../modules/common/service";
const mongoose = require("mongoose");
import { ICreateComment } from "../modules/comment/model";
import comment from "../modules/comment/schema";
const TOKEN_KEY = "@QEGTUI";
const ObjectId = mongoose.Types.ObjectId;

export class CommentController {

  comment = async (req: Request, res: Response) => {
    if (req.body.name && req.body.email && req.body.phone_number && req.body.description && req.body.blog_id) {
      const comment_params: ICreateComment = {
        name: req.body.name,
        blog_id: req.body.blog_id,
        email: req.body.email,
        phone_number: req.body.phone_number,
        description: req.body.description,
      };
      let _session = new comment(comment_params);
      await _session.save().then(
        (blog_data: ICreateComment) => {
          successResponse("Comment successfull post", blog_data, res);
        },
        (error) => {
          mongoError(error, res);
        }
      );
    } else {
      // error response if some fields are missing in request body
      insufficientParameters(res);
    }
  };

  adminCommentList = async (req: Request, res: Response) => {
    try {
      let statusTxt = req.query.status;
      let query = [{ status: statusTxt }];
      const data = await comment.aggregate([
        {
          $match: { $and: query },
        },
        {
          $project: { updatedAt: 0, __v: 0, status: 0 },
        },
        {
          $lookup: {
            from: 'blogs',
            "let": { "id": { "$toObjectId": "$blog_id" } },
            "pipeline": [
              {
                $match:
                {
                  $expr:
                  {
                    $and:
                      [
                        { $eq: ["$_id", "$$id"] },
                      ]
                  }
                }
              },

              {
                $project: { _id: 0, name: 1 },
              }
            ], as: "blog_name"
          }
        }, { $unwind: '$blog_name' },
      ]);
      successResponse("Comments list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

  commentStatus = async (req: Request, res: Response) =>{
    const { _id, status } = req.body.params;
     try {
        const data = await comment.updateOne(
          { _id: _id, }, 
          [ { $set: { "status": status} } ] 
          );
        successResponse('Comment status change successfully', data, res);
      } catch (error) {
        insufficientParameters(error)
      }
  }

  adminCommentCount = async (req: Request, res: Response) =>{
    try {
      const data = await comment.aggregate([
          {
            $facet: {
              first: [
                {
                  $match: {
                   
                    status: "active",
                    user_type: req.query.user_type,
                  },
                },
              ],
              second: [
                {
                  $match: {
                   
                    status: "inactive",
                    user_type: req.query.user_type,
                  },
                },
              ],
            },
          },
          {
            $project: {
              Active_count: {
                $size: "$first",
              },
              Inactive_count: {
                $size: "$second",
              },
            },
          },
        ]);
        successResponse("Comment count get successfully", data, res);
      } catch (error) {
        insufficientParameters(error);
      }
    }


}
