import { Request, Response } from "express";
import {
  insufficientParameters,
  mongoError,
  successResponse,
  failureResponse,
} from "../modules/common/service";
const mongoose = require("mongoose");
import { ICreateBlog } from "../modules/blog/model";
import blog from "../modules/blog/schema";
const TOKEN_KEY = "@QEGTUI";
const ObjectId = mongoose.Types.ObjectId;

export class BlogController {
 
  create = async (req: Request, res: Response) => {
   
    if (req.body.name && req.body.email && req.body.phone_number && req.body.description && req.body.image && req.body.author) {
        const blog_params: ICreateBlog = {
            name: req.body.name,
            email: req.body.email,
            phone_number: req.body.phone_number,
            description: req.body.description,
            image: req.body.image,
            author: req.body.author
          };
        let _session = new blog(blog_params);
        
        await _session.save().then(
          (blog_data: ICreateBlog) => {
            successResponse("create user successfull", blog_data, res);},
          (error) => {
           
            mongoError(error, res);
          }
        );
  } else {
      // error response if some fields are missing in request body
        insufficientParameters(res);
  }
  };

  blogList = async (req: Request, res: Response) => {
    let query = [{ status: "active" }];
    try {
     
      const data = await blog.aggregate([
        {
            $match: { $and: query },
        },
        {
            $project: { updatedAt: 0, __v: 0, status: 0 },
        },
        {
            $lookup:
            {
                from: "comments",
                "let": { "id": { "$toString": "$_id" } },
                "pipeline": [
                    {
                        $match:
                        {
                            $expr:
                            {
                                $and:
                                    [   { $eq: ["$status", "active"]},
                                        { $eq: ["$blog_id", "$$id"] }
                                        
                                    ]
                            }
                        }
                    },
                  {
                    "$limit": 2
                  }
                  ],as: "comment_list"
            }
        }
    ])
      successResponse("User list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

  blogDetails = async (req: Request, res: Response) => {
    let query = [{ status: "active", _id: ObjectId(req.params)}];
    try {
      const data = await blog.aggregate([
        {
            $match: { $and: query },
        },
        {
            $project: { updatedAt: 0, __v: 0, status: 0 },
        },
        
            {
              $lookup:
              {
                  from: "comments",
                  "let": { "id": { "$toString": "$_id" } },
                  "pipeline": [
                      {
                          $match:
                          {
                              $expr:
                              {
                                  $and:
                                      [
                                          { $eq: ["$blog_id", "$$id"] },
                                          { $eq: ["$status", "active"] }
                                      ]
                              }
                          }
                      }],as: "comment_count"
              }
            }
          
        
    ]);
      successResponse("Blog Details get successfully", data, res);
    }catch (error) {
      insufficientParameters(error);
    }
    
   
  }

  adminBlogList = async (req: Request, res: Response) =>{
    try {
      let skipNumber = Number(req.query.skip);
      let limitNumber = Number(req.query.limit);
      let statusTxt = req.query.status;
      let query = [{ status:  statusTxt}];
      const data = await blog.aggregate([
        {
            $match: { $and: query },
        },
        {
         $project: { updatedAt: 0, __v: 0, status: 0 },
        },
        {
          "$skip": skipNumber
        },
        {
         "$limit":limitNumber 
        },
       
        {
              $lookup:
              {
                  from: "comments",
                  "let": { "id": { "$toString": "$_id" } },
                  "pipeline": [
                      {
                          $match:
                          {
                              $expr:
                              {
                                  $and:
                                      [
                                          { $eq: ["$blog_id", "$$id"] },
                                          { $eq: ["$status", "active"] }
                                      ]
                              }
                          }
                      },
                      { $group: { _id: null, count: { $sum: 1 } } },
                      {
                        $project: { _id: 0 },
                    }
                    ],as: "comment_count"
              }
        }
            
        
    ]);
      successResponse("User list got successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

  blogStatus = async (req: Request, res: Response) =>{
    const { _id, status } = req.body.params;
     try {
        const data = await blog.updateOne(
          { _id: _id, }, 
          [ { $set: { "status": status} } ] 
          );
        successResponse('Blog status change successfully', data, res);
      } catch (error) {
        insufficientParameters(error)
      }
  }

  adminBlogCount = async (req: Request, res: Response) =>{
  try {
    
      const data = await blog.aggregate([
        {
          $facet: {
            first: [
              {
                $match: {
                 
                  status: "active",
                  user_type: req.query.user_type,
                },
              },
            ],
            second: [
              {
                $match: {
                 
                  status: "inactive",
                  user_type: req.query.user_type,
                },
              },
            ],
          },
        },
        {
          $project: {
            Active_count: {
              $size: "$first",
            },
            Inactive_count: {
              $size: "$second",
            },
          },
        },
      ]);
      successResponse("Blog count get successfully", data, res);
    } catch (error) {
      insufficientParameters(error);
    }
  }

}
