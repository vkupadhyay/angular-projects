import { Application, Request, Response, NextFunction } from 'express';
import { uploadImages, userImages, productFullImages } from '../controllers/imageController';

export class UserImageRoutes {

   public route(app: Application) {
      app.post('/api/blog-image', uploadImages, productFullImages, (req: Request, res: Response) => { });
      app.post('/api/user-image', uploadImages, userImages, (req: Request, res: Response) => { });
   }
}