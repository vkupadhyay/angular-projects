import { Application, Request, Response } from 'express';
import {BlogController} from '../controllers/BlogController';

export class BlogRoutes {
    private blog_controller: BlogController = new BlogController();
    public route(app: Application) {
        app.post('/api/create', (req: Request, res: Response) => {
            this.blog_controller.create(req, res);
        });

        app.get('/api/blog', (req: Request, res: Response) => {
            this.blog_controller.blogList(req, res);
        });

        app.get('/api/blog-details/:id', (req: Request, res: Response) => {
            this.blog_controller.blogDetails(req, res);
        });

        app.get('/api/admin-blog-list', (req: Request, res: Response) => {
            this.blog_controller.adminBlogList(req, res);
        });

        app.get('/api/admin-blog-count', (req: Request, res: Response) => {
            this.blog_controller.adminBlogCount(req, res);
        });

        app.put('/api/blog-status', (req: Request, res: Response) => {
            this.blog_controller.blogStatus(req, res);
        });

    }
}