import { Application, Request, Response } from 'express';
import {CommentController} from '../controllers/commentController';

export class CommentRoutes {
    private comment_controller: CommentController = new CommentController();
    public route(app: Application) {
        app.post('/api/comment', (req: Request, res: Response) => {
            this.comment_controller.comment(req, res);
        });
        
        app.get('/api/admin-comment-list', (req: Request, res: Response) => {
            this.comment_controller.adminCommentList(req, res);
        });

        app.get('/api/admin-comment-count', (req: Request, res: Response) => {
            this.comment_controller.adminCommentCount(req, res);
        });

        app.put('/api/comment-status', (req: Request, res: Response) => {
            this.comment_controller.commentStatus(req, res);
        });
        

    }
}