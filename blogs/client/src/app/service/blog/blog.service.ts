import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { baseUrlApi } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
 
  constructor( private http: HttpClient, public router: Router ) {
    
  }

  

  addBlog(addBlog: any): Observable<any> {
    let api = `${baseUrlApi}/create`;
    return this.http.post(api, addBlog).pipe(catchError(this.handleError));
  }

  blogList(): Observable<any> {
    let api = `${baseUrlApi}/blog`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  getBlogDetails(id: string): Observable<any> {
    let api = `${baseUrlApi}/blog-details/${id}`;
    return this.http.get(api).pipe(catchError(this.handleError));
  }

  adminBlogList(queryObj: any): Observable<any> {
    let api = `${baseUrlApi}/admin-blog-list`;
    return this.http.get(api, {params: queryObj}).pipe(catchError(this.handleError));
  }

  adminBlogCount(queryObj: any): Observable<any> {
    let api = `${baseUrlApi}/admin-blog-count`;
    return this.http.get(api, {params: queryObj}).pipe(catchError(this.handleError));
  }

  blogStatus(queryObj: any): Observable<any> {
    let api = `${baseUrlApi}/blog-status`;
    return this.http.put(api, {params: queryObj}).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.status === 0) {
      errorMessage = error.error

    } else {
      errorMessage = error.error
    }
    return throwError(errorMessage);
  }
}
