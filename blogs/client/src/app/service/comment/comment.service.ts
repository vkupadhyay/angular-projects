import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { baseUrlApi } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor( private http: HttpClient, public router: Router ) {}

  addComment(addComment: any): Observable<any> {
    let api = `${baseUrlApi}/comment`;
    return this.http.post(api, addComment).pipe(catchError(this.handleError));
  }

  adminCommentsList(queryObj: any): Observable<any> {
    let api = `${baseUrlApi}/admin-comment-list`;
    return this.http.get(api, {params: queryObj}).pipe(catchError(this.handleError));
  }

  commentStatus(queryObj: any): Observable<any> {
    let api = `${baseUrlApi}/comment-status`;
    return this.http.put(api, {params: queryObj}).pipe(catchError(this.handleError));
  }

  adminCommentCount(queryObj: any): Observable<any> {
    let api = `${baseUrlApi}/admin-comment-count`;
    return this.http.get(api, {params: queryObj}).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.status === 0) {
      errorMessage = error.error

    } else {
      errorMessage = error.error
    }
    return throwError(errorMessage);
  }
}
