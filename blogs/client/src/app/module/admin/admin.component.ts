import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  buttoncomponentStatus: string = 'blogs';
  constructor() { }

  ngOnInit(): void {
  }
  
  checkButton(status: string){
    this.buttoncomponentStatus = status;
    console.log(status);
  }

}
