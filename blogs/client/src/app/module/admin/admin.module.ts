import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { BlogsComponent } from './components/blogs/blogs.component';
import { CommentsComponent } from './components/comments/comments.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';


@NgModule({
  declarations: [AdminComponent, BlogsComponent, CommentsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    PaginationModule.forRoot(),
  ]
})
export class AdminModule { }
