import { Component, OnInit } from '@angular/core';
import { IAdminCommentList } from 'src/app/interface/i-comments';
import {CommentService } from 'src/app/service/comment/comment.service';
import { forkJoin } from 'rxjs';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  buttonStatus: string = 'active';
  commentsItems: IAdminCommentList[];
  limit: number = 10;
  skip: number = 0;
  currentPage = 1;
  countActive: number = 0;
  countInActive: number = 0;
  count: number = 0;
  constructor( private commentService: CommentService) { }

  ngOnInit(): void {
    this.getCommentListAndCounts();
  }

  checkButton(status: string){
    this.buttonStatus = status;
    this.getCommentListAndCounts();
  }

  setUserStatusQuery(queryObj: any): void {
    if (this.buttonStatus) {
      queryObj.status = this.buttonStatus;
    } else {
      queryObj.status = 'active';
    }
  }

  commentsList(){
    const queryObj: Partial<object> = { limit: this.limit, skip: this.skip };
    this.setUserStatusQuery(queryObj);
    this.commentService.adminCommentsList(queryObj).subscribe((result)=>{
      if(result.STATUS === 'SUCCESS'){
        this.commentsItems = result.DATA;
      }
    }),(error)=>{
      console.log(error);
    }
  }

  changeStatus(id: string, status: string ){
    const queryObj: Partial<object> = { _id: id, status: status};
    this.commentService.commentStatus(queryObj).subscribe((result)=>{
     if(result.STATUS === 'SUCCESS'){
        this.commentsList();
      }
    }),(error)=>{
      console.log(error);
    }
  }

  countComment() {
    const queryObj: Partial<object> = {};
    this.setUserStatusQuery(queryObj);
    this.commentService.adminCommentCount(queryObj).subscribe((result) => {
      if (result.STATUS === 'SUCCESS') {
        this.countActive = result.DATA[0].Active_count;
        this.countInActive = result.DATA[0].Inactive_count;
        this.sendCountToPagination();
      }
    }), (error) => {
      console.log(error);
    };
  }

  sendCountToPagination(): void {
    if (this.buttonStatus === 'active') {
      this.count = this.countActive
    } else {
      this.count = this.countInActive
    }
  }

  pageChanged(event: PageChangedEvent): void {
    this.skip = (event.page - 1) * event.itemsPerPage;
    this.commentsList();
  }

  getCommentListAndCounts(): void {
    forkJoin([
      this.countComment(),
      this.commentsList(),
    ]);
  }


}
