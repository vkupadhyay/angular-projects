import { Component, OnInit } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { IAdminBlogList } from 'src/app/interface/i-blog';
import { BlogService } from 'src/app/service/blog/blog.service'
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit {
  sAttributes: any = {};
  buttonStatus: string = 'active';
  blogItems: IAdminBlogList[];
  limit: number = 10;
  skip: number = 0;
  currentPage = 1;
  countActive: number = 0;
  countInActive: number = 0;
  count: number = 0;
  constructor(private blogService: BlogService) { }

  ngOnInit(): void {
    this.getBlogListAndCounts();
  }

  checkButton(status: string): void {
    this.buttonStatus = status;
    this.currentPage = 0;
    this.getBlogListAndCounts();
  }

  setUserStatusQuery(queryObj: any): void {
    if (this.buttonStatus) {
      queryObj.status = this.buttonStatus;
    } else {
      queryObj.status = 'active';
    }
  }

  blogList() {
    const queryObj: Partial<object> = { limit: this.limit, skip: this.skip };
    this.setUserStatusQuery(queryObj);
    this.blogService.adminBlogList(queryObj).subscribe((result) => {
      if (result.STATUS === 'SUCCESS') {
        this.blogItems = result.DATA;
      }
    }), (error) => {
      console.log(error);
    };
  }

  countBlog() {
    const queryObj: Partial<object> = {};
    this.setUserStatusQuery(queryObj);
    this.blogService.adminBlogCount(queryObj).subscribe((result) => {
      if (result.STATUS === 'SUCCESS') {
        this.countActive = result.DATA[0].Active_count;
        this.countInActive = result.DATA[0].Inactive_count;
        this.sendCountToPagination();
      }
    }), (error) => {
      console.log(error);
    };
  }

  sendCountToPagination(): void {
    if (this.buttonStatus === 'active') {
      this.count = this.countActive
    } else {
      this.count = this.countInActive
    }
  }

  changeStatus(id: string, status: string) {
    const queryObj: Partial<object> = { _id: id, status: status };
    this.blogService.blogStatus(queryObj).subscribe((result) => {
      this.getBlogListAndCounts();
    }), (error) => {
      console.log(error);
    }
  }

  pageChanged(event: PageChangedEvent): void {
    this.skip = (event.page - 1) * event.itemsPerPage;
    this.blogList();
  }

  getBlogListAndCounts(): void {
    forkJoin([
      this.countBlog(),
      this.blogList()
    ]);
  }
}
