import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBlogDetails } from 'src/app/interface/i-blog';
import {BlogService} from 'src/app/service/blog/blog.service';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss']
})
export class BlogDetailsComponent implements OnInit {
  blogDetails: IBlogDetails[];
  constructor(private blogService:BlogService, private route: ActivatedRoute) {}

  ngOnInit(): void {
  this.getBlogData();
  }

  getBlogData(){
    this.blogService.getBlogDetails(this.route.snapshot.params.id).subscribe((result)=>{
      if(result.STATUS === 'SUCCESS'){
       this.blogDetails = result.DATA;
      }
    }),(error)=>{
      console.log(error);
    }
  }



}
