import { Component, OnInit } from '@angular/core';
import { IBlogList } from 'src/app/interface/i-blog';
import { BlogService } from 'src/app/service/blog/blog.service'

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {
  blogList:IBlogList[];
  isShow: boolean = false;
  index: number = -1;
  idBlog: string;
  constructor( private blogService:BlogService ) { }

  ngOnInit(): void {
   this.getBlogList();
  }
  
  getBlogList(){
   this.blogService.blogList().subscribe((result)=>{
    if(result.STATUS === 'SUCCESS'){
      this.blogList=result.DATA;
    }
  },(error)=>{
     console.log(error);
   })
  }

  addItem(newItem: boolean) {
    this.isShow = newItem;
    this.getBlogList();
  }

  getCommentStatus(status: number){
    this.index = status;
    this.getBlogList();
  }

  showCommentForm(itemIndex: number, itemId: string){
    this.index = itemIndex;
    this.idBlog = itemId;
  }

  readMore(itemId: string){
    
  }

}
