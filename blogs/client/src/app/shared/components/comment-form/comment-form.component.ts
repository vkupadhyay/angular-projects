import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { CommentService } from 'src/app/service/comment/comment.service';


@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  commentForm: FormGroup;
  @Input() blogId = '';
  submitted: boolean = false;
  @Output() popCloseStatus = new EventEmitter<number>();
  constructor(private formBuilder: FormBuilder, private commentService:CommentService) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  get f(): any {
    return this.commentForm.controls;
  }

  initializeForm(){
     this.commentForm = this.formBuilder.group({
      name: ['', Validators.required],
      blog_id: this.blogId,
      email: ['', Validators.required],
      description: ['', Validators.required],
      phone_number: ['', Validators.required],
     })
  }

  submit(){
   if (this.commentForm.invalid) {
      this.submitted = true;
      return;
    }else{
      this.commentService.addComment(this.commentForm.value).subscribe((result)=>{
        if(result.STATUS === 'SUCCESS'){
          this.commentForm.reset();
          this.popCloseStatus.emit(-1);
        }
      },(error)=>{
        console.log(error);
      })
    }
  }

  closeCommentForm(){
    this.popCloseStatus.emit(-1);
  }

}
