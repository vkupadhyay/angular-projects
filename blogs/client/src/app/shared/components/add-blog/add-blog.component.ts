import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import {BlogService} from 'src/app/service/blog/blog.service'
@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.scss']
})
export class AddBlogComponent implements OnInit {
  addBlogForm: FormGroup;
  submitted: boolean = false;
  URL = environment.productImageApiUrl;
  public uploader: FileUploader = new FileUploader({ url: this.URL, itemAlias: 'image' });
  @Output() popCloseStatus = new EventEmitter<boolean>();

  constructor( private formBuilder: FormBuilder, private blogService:BlogService ) { }

  ngOnInit(): void {
    this.initializeForm();
    this.setAttributesForFileUploader();
  }

  get f(): any {
    return this.addBlogForm.controls;
  }

  setAttributesForFileUploader() {
    this.uploader.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      const res = JSON.parse(response);
      this.selectedImage(res.result.DATA);
    };
  }

  onImageSelect(event) {
    if (event && event.target && event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.uploader.uploadAll();
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  
  selectedImage(img: string) {
    this.addBlogForm.patchValue({
      image: img
    });
  }

  initializeForm(){
    this.addBlogForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      description: ['', Validators.required],
      image: ['', Validators.required],
      phone_number: ['', Validators.required],
      author: ['', Validators.required],
    })
  }

  submit(){
    if (this.addBlogForm.invalid) {
      this.submitted = true;
      return;
    }
    this.blogService.addBlog(this.addBlogForm.value).subscribe((result)=>{
      this.addBlogForm.reset();
      this.popCloseStatus.emit(false);
    },(error)=>{
      console.log(error);
    })
  }

  cancel(popStaus: boolean){
    this.popCloseStatus.emit(popStaus);
  }

}
