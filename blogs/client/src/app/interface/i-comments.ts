export interface IAdminCommentListBlogName {
    name: string;
}

export interface IAdminCommentList {
    _id: string;
    name: string;
    blog_id: string;
    email: string;
    phone_number: number;
    description: string;
    createdAt: Date;
    blog_name: IAdminCommentListBlogName;
}
