

export interface CommentList {
    _id: string;
    name: string;
    description: string;
    createdAt: Date;
}

export interface IBlogList {
    _id: string;
    name: string;
    email: string;
    phone_number: number;
    image: string[];
    description: string;
    author: string;
    createdAt: Date;
    comment_list: CommentList[];
}





export interface CommentCount {
    count: number;
}

export interface IAdminBlogList {
    _id: string;
    name: string;
    email: string;
    phone_number: number;
    image: string[];
    description: string;
    author: string;
    createdAt: Date;
    comment_count: CommentCount[];
}



export interface IBlogCommentCount {
    _id: string;
    name: string;
    blog_id: string;
    email: string;
    phone_number: any;
    description: string;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    __v: number;
}

export interface IBlogDetails {
    _id: string;
    name: string;
    email: string;
    phone_number: number;
    image: string[];
    description: string;
    author: string;
    createdAt: Date;
    comment_count: IBlogCommentCount[];
}