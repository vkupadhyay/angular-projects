import { Application, Request, Response, NextFunction } from 'express';
import * as multer from 'multer';
import * as sharp from 'sharp';
const multerStorage = multer.memoryStorage();
const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb("Please upload only images.", false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
});

const uploadFiles = upload.array("image", 5);

export const uploadImages = (req: Request, res: Response, next: NextFunction) => {
  uploadFiles(req, res, err => {
    if (err instanceof multer.MulterError) {
      if (err.code === "LIMIT_UNEXPECTED_FILE") {
        return res.send("Too many files to upload.");
      }
    } else if (err) {
      return res.send(err);
    }
    next();
  });
};
export const userImages = async (req: any, res: Response, next: NextFunction) => {
  if (!req.files) return next();
  req.body.images = [];
  await Promise.all(
    req.files.map(async file => {
      const filename = file.originalname.replace(/\..+$/, "");
      const newFilename = `users-${filename}-${Date.now()}.jpeg`;
      await sharp(file.buffer)
        .resize(640, 640)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`upload/${newFilename}`);
      req.body.images.push(newFilename);
    })
  );
  next();
};


export const productFullImages = async (req: any, res: Response) => {
  if (!req.files) 
  req.body.images = [];
  await Promise.all(
    req.files.map(async file => {
      const filename = file.originalname.replace(/\..+$/, "");
      const newFilename = `blog-full-${filename}-${Date.now()}-640-440.jpeg`;
      const newFilenamea = `blog-thumb-${filename}-${Date.now()}-440-440.jpeg`;
      
      await sharp(file.buffer)
        .resize(640, 440)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`upload/${newFilename}`);
        
      await sharp(file.buffer)
        .resize(440, 440)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`upload/${newFilenamea}`);
 
      
        var responsearray = {
          result: {
            DATA: [
              newFilenamea, newFilename
            ],}
        };
        return res.send(responsearray);
    })
  );
 
};
