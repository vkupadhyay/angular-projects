import { Request, Response } from "express";
const mongoose = require("mongoose");
import { IUser } from "../modules/user/model";
const TOKEN_KEY = "@QEGTUI";
import users from "../modules/user/schema";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
const ObjectId = mongoose.Types.ObjectId;
export class UserController {
 

  userData = async (req: Request, res: Response) => {
    try {
      if(req.body.name.first_name && req.body.email && req.body.password){
        const salt = await bcrypt.genSalt(10);
        const newPassword = await bcrypt.hash(req.body.password, salt);
        const user_params: IUser = {
          name: {
            first_name: req.body.name.first_name,
            last_name: req.body.name.last_name,
          },
          email: req.body.email,
          phone_number: req.body.phone_number,
          image: req.body.image,
          gender: req.body.gender,
          password: newPassword,
          address: req.body.address,
        };
        console.log(user_params);
      }else{
        res.send('sadsad')
      }
      
    } catch (error) {
      res.send(error)
    }
  };


}
