import { Application, Request, Response } from 'express';
import {UserController} from '../controllers/userController';

export class UserRoutes {
    private user_controller: UserController = new UserController();
    public route(app: Application) {
        app.post('/auth/sign-up', (req: Request, res: Response) => {
            this.user_controller.userData(req, res);
        });

    }
}