import * as mongoose from 'mongoose';
import { ModificationNote  } from '../common/model';
import {IUser} from './model'
const Schema = mongoose.Schema;

const schema = new Schema<IUser>({
   
    name: {
        first_name: { type: String},
        last_name: { type: String }
    },
    email:<any> { type: String,
        required: 'This email address has already been taken.',
        match: /.+@.+\..+/,
        unique: true
    },
    phone_number: <any> { type: Number, required: 'This phone number has already been taken.',  unique: true },
    gender: String,
    image: String,
    password: String,
    user_type: {
        type: String,
        default: 'user'
    },
    address: String,
    status: {
        type: String,
        default: 'active'
    }
    
}, { timestamps: true });

export default mongoose.model('users', schema);