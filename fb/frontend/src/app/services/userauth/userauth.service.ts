import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {baseUrlApi  } from 'src/environments/environment';
import { Router } from '@angular/router';
import{ ILogin} from '../../models/login-interface'

@Injectable({
  providedIn: 'root'
})
export class UserauthService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor( private http: HttpClient, public router: Router ) { }
 
  signUp(user:any): Observable<any> {
    let api = `${baseUrlApi}/signup`;
    return this.http.post(api, user).pipe(catchError(this.handleError));
  }

  signIn(date: ILogin): Observable<any> {
    let api = `${baseUrlApi}/signin`;
    return this.http.post(api, date).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: any ;
        if (error.status === 0) {
          errorMessage = error.error
          
        } else {
          errorMessage = error.error
        }
      return throwError(errorMessage);
  }
}
