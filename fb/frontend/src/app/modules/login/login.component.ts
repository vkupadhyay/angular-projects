import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserauthService } from '../../services/userauth/userauth.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isPopShow: boolean = false;
  signInForm!: FormGroup;
  signUpForm!: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private userAuthService:UserauthService) { }

  ngOnInit(): void {
    this.initializeSignInForm();
  }

  get f(): any {
    return this.signInForm.controls;
  }

  initializeSignInForm(){
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.signUpForm = this.formBuilder.group({
      username:[ '', [Validators.required, Validators.minLength(4)]],
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  signInSubmit(){
      if (this.signInForm.invalid) {
        this.submitted = true;
        return;
      }else{
          this.userAuthService.signIn(this.signInForm.value).subscribe((res)=>{
            if(res && res.DATA){
              localStorage.setItem("isLoggedInSuccessfully", JSON.stringify(true));
              localStorage.setItem("loginToken", res.DATA);
            }
          }, error => {
            console.log(error);
          })
      };
  }

  signUpSubmit(){
    if (this.signUpForm.invalid) {
      this.submitted = true;
      return;
    }else{
        console.log(this.signUpForm.value);
    };
}


  showPop(){
    this.isPopShow = true;
  }

  popClose(){
    this.isPopShow = false;
  }

}
